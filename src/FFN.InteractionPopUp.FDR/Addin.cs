﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;
using InteractionPopUp.FDR.Forms;
using InteractionPopUp.FDR.MiddlewareService;
using System.Collections.Specialized;

namespace InteractionPopUp.FDR
{
	public class DotNetClientAddin : QueueMonitor
	{
		#region const
		private const string FdrLeadIdAttributeName = "FF_FdrLeadId";
		private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzzzz";

		private const string QB_BASE_URL = @"https://ffn.quickbase.com/db/";

		private const string QB_QUERYSTRING_PARAM_RECORD_TYPE = "a";
		private const string QB_QUERYSTRING_PARAM_RECORD_TYPE_VALUE_NEW_RECORD = "nwr";
		private const string QB_QUERYSTRING_PARAM_RECORD_TYPE_VALUE_EXISTING_RECORD = "er";

		private const string QB_FIELD_LEAD_TYPE = "_fid_133";
		private const string QB_FIELD_DAY_PHONE = "_fid_7";
		private const string QB_LEAD_TYPE_VALUE_BALBOA_FPLUS_DECLINED = "Decline Balboa";
		private const string QB_LEAD_TYPE_VALUE_NONE = "";

		private const string DNIS_BALBOA_FPLUS_DECLINED = "8885203266";
		#endregion



		#region Variables
		private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.InteractionPopUpFdr.AddIn");

		// Interaction Management
		private readonly object _interactionListLock = new object();
		private readonly List<IInteraction> _interactionsAlreadyProcessed = new List<IInteraction>();
		private NameValueCollection _disconnectMonitorInteractionIds = new NameValueCollection();

		private MiddlewareClient _middlewareClient;
		private SynchronizationContext _synchronizationContext;
		private IServiceProvider _serviceProvider;
		private Session _session;
		private InteractionsManager _interactionsManager;

		private string _qbTableId = "bg5dbsbd2";
		private string UserId { get; set; }
		private DateTime _startTime;
		#endregion Variables



		/// <summary>
		///     Insert attribute to watch list so this class will receive change
		///     notifications for only those attributes.
		/// </summary>
		protected override IEnumerable<string> Attributes {
			get {
				return new[] {
										InteractionAttributes.State,
										InteractionAttributes.Direction,
										InteractionAttributes.CallType,
										InteractionAttributeName.LocalId,
										InteractionAttributeName.RemoteId,
										"FF_FdrLeadId",
										"FF_FpApplicationId",
										"Eic_SipNumberLocal"
								};
			}
		}

		/// <summary>
		///     This method is called when QueueMonitorAddin is loaded from .net client.
		/// </summary>
		/// <param name="serviceProvider">This object will give you session.</param>
		protected override void OnLoad(IServiceProvider serviceProvider) {
			using (Topic.Scope()) {
				try {
					_serviceProvider = serviceProvider;
					_synchronizationContext = SynchronizationContext.Current;
					_session = _serviceProvider.GetService(typeof(Session)) as Session;
					_interactionsManager = InteractionsManager.GetInstance(_session);

					UserId = Environment.UserName;

					CreateAdminSession();
					WatchServerParameter();
					DisconnectAdminSession();

					CreateMiddlewareClient();
					GetQbTableId();
				}
				catch (Exception ex) {
					Topic.Exception(ex);
					throw;
				}
			}
		}

		#region InteractionsEvents


		/// <summary>
		///     This method is called when the interaction changes its state.
		/// </summary>
		/// <param name="interaction">interaction in queue</param>
		protected override void InteractionChanged(IInteraction interaction) {
			using (Topic.Scope()) {
				try {
					var interactionType = interaction.GetAttribute(InteractionAttributes.InteractionType);

					if (interactionType == InteractionType.Call.ToString()) {
						var callDirection = interaction.GetAttribute(InteractionAttributes.Direction);
						var callType = interaction.GetAttribute(InteractionAttributes.CallType);
						var callState = interaction.GetAttribute(InteractionAttributes.State);


						Topic.Verbose("Interaction {} (state change) Call Direction: {} | Call Type: {} | Call State: {}",
								interaction.InteractionId, callDirection, callType, callState);

						if (_disconnectMonitorInteractionIds[interaction.InteractionId] == null) {
							_startTime = DateTime.Now;
							_disconnectMonitorInteractionIds[interaction.InteractionId] = interaction.InteractionId;
							interaction.SetAttribute("FF_Addin_UserId", UserId);
							MonitorInteractionForDisconnect(interaction);
						}

						if (!_interactionsAlreadyProcessed.Contains(interaction)) {
							Topic.Note("The interation {} has not been processed yet", interaction.InteractionId);

							if (callType.Equals(InteractionAttributeValues.CallType.External) &&
									callState.Equals(InteractionAttributeValues.State.Connected) &&
									callDirection.Equals(InteractionAttributeValues.Direction.Incoming)) {
								// Answered External Inbound
								Topic.Note("Interaction {} is answered external inbound");
								AnsweredExternalInbound(interaction);
								AddInteraction(interaction);
							}
							else if (callType.Equals(InteractionAttributeValues.CallType.External) &&
											 callState.Equals(InteractionAttributeValues.State.Proceeding) &&
											 callDirection.Equals(InteractionAttributeValues.Direction.Outgoing)) {
								// Ringing External Outbound
								Topic.Note("Interaction {} is ringing external outbound");
								RingingExternalOutbound(interaction);
								AddInteraction(interaction);
							}
							else if (callType.Equals(InteractionAttributeValues.CallType.Intercom) &&
											 callState.Equals(InteractionAttributeValues.State.Connected) &&
											 callDirection.Equals(InteractionAttributeValues.Direction.Incoming)) {
								// Answered Internal Inbound
								Topic.Note("Interaction {} is answered internal inbound");
								AnsweredInternalInbound(interaction);
								AddInteraction(interaction);
							}
						}
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		/// <summary>
		///     This method is called when the interaction is removed from queue.
		/// </summary>
		/// <param name="interaction">interaction in queue</param>
		protected override void InteractionRemoved(IInteraction interaction) {
			var interactionType = interaction.GetAttribute(InteractionAttributes.InteractionType);

			if (interactionType == InteractionType.Call.ToString()) {
				// Remove the interaction from the process list
				RemoveInteraction(interaction);
				_disconnectMonitorInteractionIds.Remove(interaction.InteractionId);
			}

			base.InteractionRemoved(interaction);
		}

		#endregion InteractionsEvents

		#region Interaction Triggers

		private void RingingExternalOutbound(IInteraction ringingInteraction) {
			using (Topic.Scope()) {
				try {
					var interaction = ConvertIInteractionToInteraction(ringingInteraction);
					var ani = TrimPhoneNumber(GetInteractionAttributeValue(interaction, InteractionAttributeName.RemoteId));
					var leadId = GetInteractionAttributeValue(interaction, FdrLeadIdAttributeName);

					SetInteractionAttributeValue(interaction, "FF_StartTime", DateTime.Now.ToString(DateTimeFormat));

					Topic.Note("The length of the Lead Id is {}", leadId.Length);
					if (leadId.Length >= 7) {
						var url = QB_BASE_URL + _qbTableId + "?a=er&rid=" + leadId;
						Topic.Note("Created the URL '{}' to screen pop", url);

						//_client.QbAssignWcbLeadToAgent(leadId, UserId);
						ScreenPopUrl(url);

						SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");
						SetInteractionAttributeValue(interaction, "FF_IsWcbCall", "true");
					}
					else {
						var leadByAni = _middlewareClient.QbGetLeadByAni(ani);

						Topic.Note("Found {} leads for the ANI {}", leadByAni.Records.Count(), ani);

						if (!leadByAni.Records.Count().Equals(1)) {
							leadId = string.Empty;
							SetInteractionAttributeValue(interaction, FdrLeadIdAttributeName, leadId);
							SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");
						}
						else {
							if (leadByAni.Records.Any()) {
								Topic.Note("Setting {} to be {}", FdrLeadIdAttributeName, leadByAni.Records[0].RecordId);
								SetInteractionAttributeValue(interaction, FdrLeadIdAttributeName, leadByAni.Records[0].RecordId);
								SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");
							}
							else {
								Topic.Note("Unable to set {} because no records were found", FdrLeadIdAttributeName);
								SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
							}
						}

						SetInteractionAttributeValue(interaction, "FF_IsWcbCall", "false");
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		private void AnsweredExternalInbound(IInteraction answeredInteraction) {
			using (Topic.Scope()) {
				try {
					Stopwatch stopWatch = new Stopwatch();
					Interaction interaction = ConvertIInteractionToInteraction(answeredInteraction);
					string ani = TrimPhoneNumber(GetInteractionAttributeValue(interaction, InteractionAttributeName.RemoteId));
					string dnis = TrimPhoneNumber(GetInteractionAttributeValue(interaction, "Eic_SipNumberLocal"));
					string qbLeadId = "";
					string screenPopUrl = QB_BASE_URL + this._qbTableId;
					string agentUserName = "";
					bool shouldBeAssignedToAgent = false;

					stopWatch.Reset();
					stopWatch.Start();
					QbGetLeadAndAssignedAgentByAniResponse leadByAni = _middlewareClient.QbGetLeadAndAssignedAgentByAni(ani);
					stopWatch.Stop();

					Topic.Note("QB REQUEST TIME _client.QbGetLeadAndAssignedAgentByAni({}) = {} ms", ani, stopWatch.ElapsedMilliseconds);
					Topic.Note("Found {} leads for the ANI {}", leadByAni.Records.Count(), ani);

					SetInteractionAttributeValue(interaction, "FF_StartTime", DateTime.Now.ToString(DateTimeFormat));

					//no lead record(s) found, pop add new lead page
					if (leadByAni.Records == null || !leadByAni.Records.Any()) {
						SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");

						Topic.Note("Unable to set {} because no records were found", FdrLeadIdAttributeName);

						//var tollFreeByDnis = _client.QbGetTollFreeByDnis(dnis);
						string tollFreeByDnis = _middlewareClient.QbGetCachedTollFreeByDnis(dnis);
						var validLeadTypes = GetValidLeadTypes();

						//this sucks, all QB querystring constructions should be refactored to a new wrapper class for QB querystrings
						screenPopUrl = screenPopUrl + $"?{QB_QUERYSTRING_PARAM_RECORD_TYPE}={QB_QUERYSTRING_PARAM_RECORD_TYPE_VALUE_NEW_RECORD}&{QB_FIELD_DAY_PHONE}={ani}&{QB_FIELD_LEAD_TYPE}={QB_LEAD_TYPE_VALUE_NONE}";


						if (tollFreeByDnis != null
								 && validLeadTypes.Any(choice => choice.Equals(tollFreeByDnis, StringComparison.CurrentCultureIgnoreCase))) {
							Topic.Note("Phone display returned by middleware:", tollFreeByDnis);
							screenPopUrl += tollFreeByDnis;
							Topic.Note("The phone display {} was found as a valid lead type", tollFreeByDnis);
						}
						else {
							Topic.Note("Phone display returned by middleware:", tollFreeByDnis);
							screenPopUrl = screenPopUrl + "Phone Lead";
						}
					}
					else {
						//unique lead record found, pop lead record page and assign it to the current agent if unassigned
						if (leadByAni.Records.Count().Equals(1)) {
							qbLeadId = leadByAni.Records[0].RecordId;
							agentUserName = leadByAni.Records[0].SalesAgent;
							screenPopUrl = screenPopUrl + "?a=er&rid=" + qbLeadId;
							shouldBeAssignedToAgent = (string.IsNullOrEmpty(agentUserName) || agentUserName.Equals("unassigned", StringComparison.CurrentCultureIgnoreCase) || agentUserName.Equals("balboa", StringComparison.CurrentCultureIgnoreCase));

							SetInteractionAttributeValue(interaction, FdrLeadIdAttributeName, qbLeadId);
							SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");

							Topic.Note("Setting {} to be {}", FdrLeadIdAttributeName, qbLeadId);
						}
						else
						//multiple records found for ANI, pop lead search page with ANI as search criteria
						{
							screenPopUrl = screenPopUrl + "?a=q&qid=151&nv=1&v0=" + ani;

							SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
						}
					}

					Topic.Note("Created the URL '{}' to screen pop", screenPopUrl);
					SetInteractionAttributeValue(interaction, "FF_IsWcbCall", "false");

					ScreenPopUrl(screenPopUrl);

					if (shouldBeAssignedToAgent) {
						stopWatch.Reset();
						stopWatch.Start();
						_middlewareClient.QbAssignWcbLeadToAgent(qbLeadId, UserId);
						stopWatch.Stop();

						Topic.Note("QB REQUEST TIME _client.QbGetLeadAndAssignedAgentByAni({}) = {} ms", ani, stopWatch.ElapsedMilliseconds);
						Topic.Note("No sales agents were assigned to the lead {}", qbLeadId);
						Topic.Note("Assigned {} to the lead {}", UserId, qbLeadId);
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		private void AnsweredInternalInbound(IInteraction answeredInteraction) {
			using (Topic.Scope()) {
				try {

					var interaction = ConvertIInteractionToInteraction(answeredInteraction);
					var qbLeadId = interaction.GetStringAttribute("FF_FdrLeadId");

					SetInteractionAttributeValue(interaction, "FF_StartTime", DateTime.Now.ToString(DateTimeFormat));

					Topic.Note("The lead Id for interaction {} is {}", interaction.InteractionId.Id, qbLeadId);
					if (qbLeadId.Length >= 7) {
						SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");

						Topic.Verbose("The lead id length is greater than or equal to 7 digits");
						var url = QB_BASE_URL + _qbTableId + "?a=er&rid=" + qbLeadId;
						Topic.Note("Created the URL '{}' to screen pop", url);

						var assignedAgent = _middlewareClient.QbGetAssignedAgentFromLead(qbLeadId);
						if (assignedAgent.Records != null && (string.IsNullOrEmpty(assignedAgent.Records[0].SalesAgent) ||
								 assignedAgent.Records[0].SalesAgent.Equals("unassigned", StringComparison.CurrentCultureIgnoreCase))) {
							_middlewareClient.QbAssignLeadToAgent(qbLeadId, UserId);
							Topic.Note("No sales agents were assigned to the lead {}", qbLeadId);
							Topic.Note("Assigned {} to the lead {}", UserId, qbLeadId);
						}

						ScreenPopUrl(url);

					}
					else {
						SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
						Topic.Verbose("The lead id is shorter than 7 digits");
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}



		#endregion

		#region InteractionProcessList

		private void AddInteraction(IInteraction interaction) {
			using (Topic.Scope()) {
				try {
					lock (_interactionListLock) {
						Topic.Verbose("Checking processed list.");

						if (!_interactionsAlreadyProcessed.Contains(interaction)) {
							Topic.Verbose("Adding Interaction:{} to the list.", interaction.InteractionId);
							_interactionsAlreadyProcessed.Add(interaction);
						}
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		private void RemoveInteraction(IInteraction interaction) {
			using (Topic.Scope()) {
				try {
					lock (_interactionListLock) {
						if (_interactionsAlreadyProcessed.Contains(interaction)) {
							Topic.Verbose("Remove Interaction:{} from the list.", interaction.InteractionId);
							_interactionsAlreadyProcessed.Remove(interaction);
						}

					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		#endregion InteractionProcessList

		#region Helper Methods

		private void MonitorInteractionForDisconnect(IInteraction interaction) {
			using (Topic.Scope()) {
				Topic.Note("Start of method MonitorInteractionForDisconnectv");
				try {
					var callDirection = interaction.GetAttribute(InteractionAttributes.Direction);
					var callType = interaction.GetAttribute(InteractionAttributes.CallType);
					var callState = interaction.GetAttribute(InteractionAttributes.State);
					var transferred = interaction.GetAttribute("FF_TransferredCall");

					Topic.Verbose("Interaction {} (dequeued) Call Direction: {} | Call Type: {} | Call State: {}",
							interaction.InteractionId, callDirection, callType, callState);

					/*
					 * terrible awful no good workaround for undocumented CIC change that results in call state being 
					 * unreliable when agent disconnects.
					*/
					if (!transferred.Equals("true")) {
						string interactionId = interaction.InteractionId;
						CrmCdrLogger crmCdrLogIt = new CrmCdrLogger(interactionId, _serviceProvider, _synchronizationContext, _startTime, MiddlewareServiceEndpoint, UserId, callDirection);
						Thread threadCrmCdrLogger = new Thread(new ThreadStart(crmCdrLogIt.CrmCdrLog));
						threadCrmCdrLogger.Start();
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
				Topic.Note("End of method MonitorInteractionForDisconnect");
			}
		}

		private void CreateMiddlewareClient() {
			try {
				var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };
				var clientEndpoint = new EndpointAddress(MiddlewareServiceEndpoint);

				clientBinding.MaxReceivedMessageSize = 20000000;
				clientBinding.MaxBufferPoolSize = 20000000;
				clientBinding.MaxBufferSize = 20000000;

				_middlewareClient = new MiddlewareClient(clientBinding, clientEndpoint);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}

		private void GetQbTableId() {
			try {
				_qbTableId = _middlewareClient.GetQbTableId();
				Topic.Status("The Quickbase table ID is {}", _qbTableId);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}

		private Interaction ConvertIInteractionToInteraction(IInteraction iinteraction) {
			Interaction ret = null;

			using (Topic.Scope()) {
				try {
					ret = _interactionsManager.CreateInteraction(new InteractionId(iinteraction.InteractionId));
					Topic.Verbose("Successfully created Interaction {}", ret.InteractionId.Id);
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}

			return ret;
		}

		private static string GetInteractionAttributeValue(Interaction interaction, string attributeName) {
			var ret = "";

			using (Topic.Scope()) {
				try {
					ret = interaction.GetStringAttribute(attributeName);
					Topic.Note("The value of the attribute {} is {}", attributeName, ret);
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}

			return ret;
		}

		private static void SetInteractionAttributeValue(Interaction interaction, string attributeName, string attributeValue) {
			using (Topic.Scope()) {
				try {
					interaction.SetStringAttribute(attributeName, attributeValue);
					Topic.Note("Set the interaction attribute {} to be {}", attributeName, attributeValue);
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		private IEnumerable<string> GetValidLeadTypes() {
			var ret = new List<string>();
			try {
				var record = _middlewareClient.QbGetValidLeadTypeValues();
				var table = record.Tables.FirstOrDefault();
				if (table != null) {
					var fields = table.fields.Select(x => x);
					ret.AddRange(fields.ToList().SelectMany(field => field.Choices));
					Topic.Note("Found {} choices!", ret.Count);
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
			return ret;
		}

		private static string TrimPhoneNumber(string ani) {
			var ret = "";

			using (Topic.Scope()) {
				try {
					ret = ani.Replace("(", "");
					ret = ret.Replace(")", "");
					ret = ret.Replace(" ", "");
					ret = ret.Replace("-", "");
					ret = ret.Replace("+", "");
					ret = ret.Replace("sip:", "");
					ret = ret.Replace("ip:", "");

					if (ret.Length > 10) {
						ret = ret.Substring(1, 10);
					}

					Topic.Verbose("Trimmed the phone number: {}", ret);
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}

			return ret;
		}
		#endregion

		#region Screen Pop Methods

		private void ScreenPopUrl(string url) {
			using (Topic.Scope()) {
				try {
					Process.Start(url);
					Topic.Note("Popped the url {}", url);
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		#endregion

		#region Server Parameter
		private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
		public string MiddlewareServiceEndpoint { get; private set; }

		private void WatchServerParameter() {
			using (Topic.Scope()) {
				try {
					var serverParameters = new ServerParameters(_adminSession);
					serverParameters.StartWatching(_serverParameterName);
					var parameterList = serverParameters.GetServerParameters(_serverParameterName);
					Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

					if (parameterList.Count.Equals(1)) {
						MiddlewareServiceEndpoint = parameterList[0].Value;
						Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
					}
					else {
						Topic.Error("Could not find an endpoint for the middleware!");
					}

					serverParameters.StopWatching();
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}
		#endregion

		#region Admin Session
		private Session _adminSession;
		private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

		private void CreateAdminSession() {
			try {
				Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host, AdminUsername);
                _adminSession = new Session
                {
                    AutoReconnectEnabled = true
                };
                _adminSession.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
						new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}

		}

		private void DisconnectAdminSession() {
			try {
				_adminSession.Disconnect();
				Topic.Status("Proxy session has been disconnected");
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}
		#endregion
	}
};



