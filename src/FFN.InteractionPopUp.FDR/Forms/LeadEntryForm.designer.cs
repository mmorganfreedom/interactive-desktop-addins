﻿namespace InteractionPopUp.FDR.Forms
{
    partial class LeadEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LeadEntryForm));
            this.titleLabel = new System.Windows.Forms.Label();
            this.dnisLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.aniLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.leadIdTextbox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(12, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(214, 13);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "No FDR Lead ID # associated with this call.";
            // 
            // dnisLabel
            // 
            this.dnisLabel.AutoSize = true;
            this.dnisLabel.Location = new System.Drawing.Point(108, 61);
            this.dnisLabel.Name = "dnisLabel";
            this.dnisLabel.Size = new System.Drawing.Size(111, 13);
            this.dnisLabel.TabIndex = 8;
            this.dnisLabel.Text = "No inbound line found";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Inbound Line:";
            // 
            // aniLabel
            // 
            this.aniLabel.AutoSize = true;
            this.aniLabel.Location = new System.Drawing.Point(108, 39);
            this.aniLabel.Name = "aniLabel";
            this.aniLabel.Size = new System.Drawing.Size(72, 13);
            this.aniLabel.TabIndex = 6;
            this.aniLabel.Text = "No ANI found";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Outside Phone #:";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(12, 92);
            this.descriptionLabel.MaximumSize = new System.Drawing.Size(270, 100);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(252, 26);
            this.descriptionLabel.TabIndex = 9;
            this.descriptionLabel.Text = "If this call was to/from a lead, please enter the FDR Lead ID # then click OK.";
            // 
            // leadIdTextbox
            // 
            this.leadIdTextbox.Location = new System.Drawing.Point(15, 121);
            this.leadIdTextbox.Name = "leadIdTextbox";
            this.leadIdTextbox.Size = new System.Drawing.Size(266, 20);
            this.leadIdTextbox.TabIndex = 10;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(206, 147);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 11;
            this.submitButton.Text = "OK";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(125, 147);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 12;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // LeadEntryForm
            // 
            this.AcceptButton = this.submitButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(293, 184);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.leadIdTextbox);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.dnisLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.aniLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.titleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LeadEntryForm";
            this.Text = "Input FDR Lead ID #";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label dnisLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label aniLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox leadIdTextbox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
    }
}