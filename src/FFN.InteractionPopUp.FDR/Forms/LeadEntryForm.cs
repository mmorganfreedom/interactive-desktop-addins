﻿using System;
using System.Windows.Forms;

namespace InteractionPopUp.FDR.Forms
{
    public partial class LeadEntryForm : Form
    {
        public string Ani { get; set; }
        public string Dnis { get; set; }
        public string LeadIdValue { get; private set; }

        public LeadEntryForm(string dnis, string ani)
        {
            InitializeComponent();
            aniLabel.Text = ani;
            dnisLabel.Text = dnis;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            LeadIdValue = leadIdTextbox.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
