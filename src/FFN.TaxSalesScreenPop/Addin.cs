﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;
using FFN.TaxSalesScreenPop.MiddlewareService;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.InteractionClient.AddIn;

namespace FFN.TaxSalesScreenPop
{
    public class Addin : QueueMonitor
    {
        private readonly List<IInteraction> _processedInteractions = new List<IInteraction>();
        private readonly object _interactionsLock = new object();

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.TaxSalesScreenPop.AddIn");
        protected override IEnumerable<string> Attributes
        {
            get
            {
                return new[]
                {
                    InteractionAttributes.Direction,
                    InteractionAttributes.CallType,
                    InteractionAttributes.State,
                    InteractionAttributes.RemoteId
                };
            }
        }

        private Session _session;
        private MiddlewareClient _client;

        private SynchronizationContext _context;
        private string _screenPopUrlValue = "https://ffn.quickbase.com/db/bexpsx4re?a=q&qid=71&nv=1&v0=";

        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            using (Topic.Scope())
            {
                try
                {
                    _session = serviceProvider.GetService(typeof (Session)) as Session;
                    _context = SynchronizationContext.Current;

                    CreateAdminSession();
                    WatchServerParameter();
                    DisconnectAdminSession();

                    CreateMiddlewareClient();
                    LoadScreenPopUrl();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                    throw;
                }

                base.OnLoad(serviceProvider);
            }
        }

        protected override void InteractionChanged(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    var callDirection = interaction.GetAttribute(InteractionAttributes.Direction);
                    var callState = interaction.GetAttribute(InteractionAttributes.State);
                    var callType = interaction.GetAttribute(InteractionAttributes.CallType);

                    Topic.Verbose("Interaction {} changed! Direction: {} | State: {} | Type: {}", interaction.InteractionId,
                        callDirection, callState, callType);

                    if (callDirection.Equals(InteractionAttributeValues.Direction.Incoming) &&
                        callState.Equals(InteractionAttributeValues.State.Connected) && 
                        callType.Equals(InteractionAttributeValues.CallType.External))
                    {
                        lock (_interactionsLock)
                        {
                            if (!_processedInteractions.Contains(interaction))
                            {
                                Topic.Verbose("Processing interaction {}", interaction.InteractionId);
                                _processedInteractions.Add(interaction);
                                ScreenPopUrl(interaction.GetAttribute(InteractionAttributes.RemoteId));
                            }
                            else
                            {
                                Topic.Note("Interaction {} has already been processed!", interaction.InteractionId);
                            }
                        }
                    }
                    else
                    {
                        if (callDirection.Equals(InteractionAttributeValues.Direction.Outgoing) &&
                            callState.Equals(InteractionAttributeValues.State.Proceeding) && 
                            callType.Equals(InteractionAttributeValues.CallType.External))
                        {
                            lock (_interactionsLock)
                            {
                                if (!_processedInteractions.Contains(interaction))
                                {
                                    Topic.Verbose("Processing interaction {}", interaction.InteractionId);
                                    _processedInteractions.Add(interaction);

                                    _context.Send(p =>
                                    {
                                        var form = new MessageBoxForm {TopMost = true};
                                        form.Show();

                                    }, null);

                                }
                                else
                                {
                                    Topic.Note("Interaction {} has already been processed!", interaction.InteractionId);
                                }
                            }
                        }
                        else
                        {
                            Topic.Note("Interaction {} will not be processed!");
                            Topic.Note("Interaction {}'s state is {} and direction is {}", interaction.InteractionId, callState,
                                callDirection);
                        }

                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }

                base.InteractionChanged(interaction);
            }
        }

        protected override void InteractionRemoved(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    lock (_interactionsLock)
                    {
                        if (_processedInteractions.Contains(interaction))
                        {
                            Topic.Verbose("Removing interaction {} from the processed list", interaction.InteractionId);
                            _processedInteractions.Remove(interaction);
                        }
                        else
                        {
                            Topic.Verbose("Interaction {} was not found in the process list! It will not be removed",
                                interaction.InteractionId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }

                base.InteractionRemoved(interaction);
            }
        }

        #region Helper Methods

        private void CreateMiddlewareClient()
        {
            using (Topic.Scope())
            {
                try
                {
                    // Create a new Middleware Client
                    var clientBinding = new BasicHttpBinding
                    {
                        Name = "BasicHttpBinding_IMiddleware"
                    };

                    var clientEndpoint = new EndpointAddress(MiddlewareServiceEndpoint);
                    _client = new MiddlewareClient(clientBinding, clientEndpoint);

                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void LoadScreenPopUrl()
        {
            using (Topic.Scope())
            {
                try
                {
                    _screenPopUrlValue = _client.GetTaxSalesScreenPopUrl();
                    Topic.Note("GetTaxSalesScreenPopUrl returned the value {}", _screenPopUrlValue);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

        }

        private void ScreenPopUrl(string ani)
        {
            using (Topic.Scope())
            {
                try
                {
                    var url = _screenPopUrlValue + TrimPhoneNumber(ani);
                    Process.Start(url);
                    Topic.Verbose("Successfully screen popped the URL '{}'", url);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

        }

        private string TrimPhoneNumber(string ani)
        {
            var ret = "";

            using (Topic.Scope())
            {
                try
                {
                    ret = ani.Replace("(", "");
                    ret = ret.Replace(")", "");
                    ret = ret.Replace(" ", "");
                    ret = ret.Replace("-", "");
                    ret = ret.Replace("+", "");

                    if (ret.Length > 10)
                    {
                        ret = ret.Substring(1);
                    }

                    Topic.Verbose("Trimmed the ANI: {}", ret);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        #endregion

        #region Server Parameter
        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope())
            {
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    serverParameters.StartWatching(_serverParameterName);
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
                    }
                    else
                    {
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region Admin Session
        private Session _adminSession;
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

        private void CreateAdminSession()
        {
            try
            {
                Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host, AdminUsername);
                _adminSession = new Session();
                _adminSession.AutoReconnectEnabled = true;
                _adminSession.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                    new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

        }

        private void DisconnectAdminSession()
        {
            try
            {
                _adminSession.Disconnect();
                Topic.Status("Proxy session has been disconnected");
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion
    }
}
