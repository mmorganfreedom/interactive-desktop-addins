﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using FFN.PopPartnerLead.Forms;
using FFN.PopPartnerLead.MiddlewareService;
using ININ.Client.Common.Connection;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;
using IInteraction = ININ.Client.Common.Interactions.IInteraction;

namespace FFN.PopPartnerLead
{
    internal class Button : IInteractionButton
    {
        #region IInteraction Button Attributes

        public Icon Icon
        {
            get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); }
        }

        public string Id
        {
            get { return "PopPartnerLeadAddIn"; }
        }

        public SupportedInteractionTypes SupportedInteractionTypes
        {
            get { return SupportedInteractionTypes.Call; }
        }

        public string Text
        {
            get { return "Pop Partner Lead"; }
        }

        public string ToolTipText
        {
            get { return "Display a screen pop with the partner lead."; }
        }

        #endregion

        #region Tracing

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.PopPartnerLead.Button");

        private static readonly ITopicTracerContextAttribute<string> InteractionIdAttribute =
            TopicTracerContextAttributeFactory.CreateStringContextAttribute("FFN.PopPartnerLead.InteractionId",
                "Interaction ID", "{}");

        #endregion

        private readonly SynchronizationContext _context;
        private MiddlewareClient _client;

        public Button(Addin addin)
        {
            try
            {
                _context = SynchronizationContext.Current;
                CreateMiddlewareClient(addin.MiddlewareServiceEndpoint);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }
        }

        public bool CanExecute(IInteraction selectedInteraction)
        {
            return selectedInteraction.InteractionState.Equals(InteractionState.Connected);
        }

        public void Execute(IInteraction selectedInteraction)
        {
            using (Topic.Scope())
            {
                using (InteractionIdAttribute.Create(selectedInteraction.InteractionId))
                {
                    try
                    {
                        PopPartnerLead(selectedInteraction);
                    }
                    catch (Exception ex)
                    {
                        Topic.Exception(ex);
                    }
                }
            }
        }

        private void PopPartnerLead(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    string emailAddress;

                    do
                    {
                        emailAddress = DisplayEmailEntryForm();
                        if (string.IsNullOrEmpty(emailAddress)) { return; }
                        Topic.Note("The email address is set to {}", emailAddress);
                    } while (string.IsNullOrEmpty(emailAddress));

                    var appByEmail = SfGetApplicationByEmailAddress(emailAddress);
                    if (appByEmail == null || appByEmail.totalSize <= 0)
                    {
                        Topic.Warning("No application found. Try pop again manually");
                        _context.Send(p => MessageBox.Show("No application found. Try pop again manually", "No Application Found"), null);
                        return;
                    }

                    interaction.SetAttribute("FF_FpApplicationId", appByEmail.records[0].Id);
                    Topic.Note("Set FF_FpApplicationId to be {}", appByEmail.records[0].Id);

                    var url = _client.IVRGetSalesforceInstanceUrl() + "/" + appByEmail.records[0].Id;
                    Topic.Note("Created the URL: {}", url);

                    Topic.Note("Searching for an employee with the username {}", Environment.UserName);
                    var employee = SfGetEmployeeByUsername(Environment.UserName);

                    if ((string.IsNullOrEmpty(appByEmail.records[0].Loan_Officer__c) ||
                        !appByEmail.records[0].Referral_Owner__c.Equals("Freedom Plus")) && employee.TotalSize > 0)
                    {
                        var employeeToAssign = string.IsNullOrEmpty(employee.Records[0].Id)
                            ? ""
                            : employee.Records[0].Id;

                        _client.SfAssignAndUnlockApplication(appByEmail.records[0].Id, employeeToAssign);
                        Topic.Note("Called SfAssignAndUnlockApplication with the Id: {} and Employee: {}",
                            appByEmail.records[0].Id, employeeToAssign);
                    }
                    else
                    {
                        Topic.Warning("Either loan officer and referral owner were set or no employee was found!");
                    }

                    Process.Start(url);
                    Topic.Note("Popped the URL: {}", url);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        #region Middleware Methods
        private void CreateMiddlewareClient(string endpoint)
        {
            using (Topic.Scope())
            {
                try
                {
                    var clientBinding = new BasicHttpBinding
                    {
                        Name = "BasicHttpBinding_IMiddleware"
                    };

                    var clientEndpoint = new EndpointAddress(endpoint);
                    _client = new MiddlewareClient(clientBinding, clientEndpoint);

                    Topic.Status("Successfully created the Middleware Client connection");
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private SfGetEmployeeByUsernameResponse SfGetEmployeeByUsername(string username)
        {
            var ret = new SfGetEmployeeByUsernameResponse();
            try
            {
                ret = _client.SfGetEmployeeByUsername(username);

                if (ret.Done && ret.TotalSize > 0)
                {
                    Topic.Note("SfGetEmployeeByUsername Id: {}", ret.Records[0].Id);
                }
                else
                {
                    Topic.Warning("No record found for SfGetEmployeeByUsername with the username {}", username);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return ret;
        }

        private SfQueryResponse SfGetApplicationByEmailAddress(string emailAddress)
        {
            var ret = new SfQueryResponse();
            try
            {
                ret = _client.SfGetApplicationByEmailAddress(emailAddress);

                if (ret.done && ret.totalSize > 0)
                {
                    Topic.Note("SfGetApplicationByEmailAddress Id: {}", ret.records[0].Id);
                    Topic.Note("SfGetApplicationByEmailAddress Loan Officer: {}", ret.records[0].Loan_Officer__c);
                    Topic.Note("SfGetApplicationByEmailAddress Referral Owner: {}", ret.records[0].Referral_Owner__c);
                }
                else
                {
                    Topic.Warning("No record found for SfGetApplicationByEmailAddress with the email {}", emailAddress);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return ret;
        }
        #endregion

        #region Display Method
        private string DisplayEmailEntryForm()
        {
            var ret = "";

            try
            {
                var form = new EmailAddressEntryForm();
                _context.Send(p =>
                {
                    var result = form.ShowDialog();
                    if (result.Equals(DialogResult.OK))
                    {
                        ret = form.EmailAddress;
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }
        #endregion
    }
}