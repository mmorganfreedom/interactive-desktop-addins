﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.PopPartnerLead.Forms
{
    public partial class EmailAddressEntryForm : Form
    {
        public string EmailAddress { get; set; }

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.PopPartnerLead.EmailAddressEntryForm");

        public EmailAddressEntryForm()
        {
            InitializeComponent();
        }

        private void emailAddressTextBox_TextChanged(object sender, EventArgs e)
        {
            if (IsValidEmail(emailAddressTextBox.Text))
            {
                Topic.Verbose("{} is a valid email", emailAddressTextBox.Text);
                submitButton.Enabled = true;
            }
            else
            {
                submitButton.Enabled = false;
            }
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                Topic.Note("Validating {}.", email);
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase);
            }
            catch (Exception ex)
            {
                Topic.Note("{} is an invalid email", email);
                Topic.Exception(ex);
                return false;
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            EmailAddress = emailAddressTextBox.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            EmailAddress = "";
            DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}
