﻿using System;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.InteractionClient;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;

namespace FFN.PopPartnerLead
{
    public class Addin : IAddIn
    {
        private Session _session;

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.PopPartnerLead.AddIn");

        public void Load(IServiceProvider serviceProvider)
        {
            try
            {
                _session = serviceProvider.GetService(typeof (Session)) as Session;

                CreateAdminSession();
                WatchServerParameter();
                DisconnectAdminSession();

                var service = ServiceLocator.Current.GetInstance<IClientInteractionButtonService>();
                service.Add(new Button(this));
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }
        }

        public void Unload()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                //throw;
            }
        }

        #region Server Parameter
        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope())
            {
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    serverParameters.StartWatching(_serverParameterName);
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
                    }
                    else
                    {
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region Admin Session
        private Session _adminSession;
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

        private void CreateAdminSession()
        {
            try
            {
                Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host, AdminUsername);
                _adminSession = new Session();
                _adminSession.AutoReconnectEnabled = true;
                _adminSession.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                    new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

        }

        private void DisconnectAdminSession()
        {
            try
            {
                _adminSession.Disconnect();
                Topic.Status("Proxy session has been disconnected");
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion
    }
}