﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using ScreenPopManager.FDR;
using ININ.Diagnostics;
using System.Threading;
//using mshtml;
//using System.Text.RegularExpressions;

namespace ScreenPopManager.FDR.Forms
{
    public partial class BillboardVerificationSecurityQuestion : Form
    {
        #region Constants
        public const string RESOURCE_TYPE_URI = "uri";
        public const string RESOURCE_TYPE_HTML = "html";

        private const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS = "<span class='verifiedPass'>&#10004;</span>";
        private const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL = "<span class='verifiedFail'>X</span>";
        private const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL = "<span class='verifiedNull'>&nbsp;</span>";

        private const string SALESFORCE_CLIENT_HIGHLIGHTS_CSS_LINK = @"https://cs61.salesforce.com/resource/1479988015000/SLDS/assets/styles/salesforce-lightning-design-system-ltng.css";
        #endregion

        #region Variables
        private string _salesforceClientProfileUrl = "";
        private string _salesforceClientHighlightsUrl = "";
        private bool _salesforceLoginSubmitted = false;
        private bool _debug = false;
        #endregion
        #region Force Form Always on Top
        //see http://stackoverflow.com/a/34703664/90400
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("ScreenPopManager.FDR.Forms");

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        #endregion

        public BillboardVerificationSecurityQuestion(string windowTitle, string resource, string resourceType, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion(..)"))
            {

                try
                {
                    Topic.Note(
                        "Entering BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion:\nParamters:\n\twindowTitle: " +
                        windowTitle.ToString() + "\n\tresource: " + resource.ToString() + "\n\tresourceType: " +
                        resourceType.ToString() + "\n\tcustomText: {}", customText);
                    InitializeComponent();
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted InitializeComponent()");
                    SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS); //force form to always be on top
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion:\nExecuted SetWindowPos()\nParameters:\n\tthis.Handle: {}\n\tHWND_TOPMOST: " +
                        HWND_TOPMOST.ToString() + "\n\t0: 0\n\t0: 0\n\t0: 0\n\t0: 0\n\tTOPMOST_FLAGS: " +
                        TOPMOST_FLAGS.ToString(), this.Handle);

                    this.FormClosing += new FormClosingEventHandler(Billboard_Exit);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion:\nExecuted the following statement:\n\tthis.FormClosing += new FormClosingEventHandler(billboard_Exit);");

                    #region Init

                    this._salesforceClientProfileUrl = customText.clientSalesforceUrl;
                    this._salesforceClientHighlightsUrl = customText.clientSalesforceSummaryUrl;
                    string htmlBodyText = "ERROR: NO RESOURCE LOADED";
                    bool verificationResult = (customText.clientSsnLastFourDigitsActive
                                                  ? customText.clientSsnLastFourDigitsVerified
                                                  : true)
                                              && (customText.clientDateOfBirthAsStringActive
                                                  ? customText.clientDateOfBirthAsStringVerified
                                                  : true)
                                              && (customText.clientMonthlyDraftAmountActive
                                                  ? customText.clientMonthlyDraftAmountVerified
                                                  : true)
                        ;

                    this.Text = windowTitle;
                    this.webBrowserPanel.BackColor = verificationResult ? Color.ForestGreen : Color.DarkRed;
                    this.billboardDismissButton.Text = "Continue >>";
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion:\nVariable Assignment:\n\tthis._salesforceClientProfileUrl: " +
                        this._salesforceClientProfileUrl.ToString() + "\n\tthis._salesforceClientHighlightsUrl: " +
                        this._salesforceClientHighlightsUrl.ToString() + "\n\thtmlBodyText: " +
                        htmlBodyText.ToString() + "\n\tverificationResult: " + verificationResult.ToString() +
                        "\n\tthis.Text: " + this.Text.ToString() +
                        "\n\tthis.webBrowserPanel.BackColor: {}\n\tthis.billboardDismissButton.Text: " +
                        this.billboardDismissButton.Text.ToString(), this.webBrowserPanel.BackColor);


                    if (this._debug)
                    {
                        Topic.Note(
                            "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\n\tthis._debug is true:");
                        this.MaximizeBox = false;
                        this.TopMost = false;
                        this.billboardWebBrowser.IsWebBrowserContextMenuEnabled = true;
                        this.verificationResultsWebBrowser.IsWebBrowserContextMenuEnabled = true;
                        Topic.Note(
                            "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nthis._debug is true:\n\tVariable Assginment:\n\t\tthis.MaximizeBox: " +
                            this.MaximizeBox.ToString() + "\n\t\tthis.TopMost: " + this.TopMost.ToString() +
                            "\n\t\tthis.billboardWebBrowser.IsWebBrowserContextMenuEnabled: " +
                            this.billboardWebBrowser.IsWebBrowserContextMenuEnabled.ToString() +
                            "\n\t\tthis.verificationResultsWebBrowser.IsWebBrowserContextMenuEnabled: {}",
                            this.verificationResultsWebBrowser.IsWebBrowserContextMenuEnabled);
                    }

                    //this.billboardWebBrowser.Navigating                  += new WebBrowserNavigatingEventHandler(webBrowser_HijackLinkClicks);
                    //this.verificationResultsWebBrowser.Navigating        += new WebBrowserNavigatingEventHandler(webBrowser_HijackLinkClicks);

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.DocumentText = "0";
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\tthis.billboardWebBrowser: " +
                        this.billboardWebBrowser.ToString());

                    this.billboardWebBrowser.Document.OpenNew(true);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted this.billboardWebBrowser.Document.OpenNew(true)");

                    switch (resourceType)
                    {
                        case RESOURCE_TYPE_HTML:
                            Topic.Note(
                                "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nIn Switch:\nrosourceType == RESOURCE_TYPE_HTML\n\tresourceType: " +
                                resourceType.ToString() + "\n\tRESOURCE_TYPE_HTML: {}", RESOURCE_TYPE_HTML);
                            htmlBodyText = resource;
                            Topic.Note(
                                "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nIn Switch:\nrosourceType == RESOURCE_TYPE_HTML:\nVariable Assignment:\n\thtmlBodyText: {}",
                                htmlBodyText);
                            break;
                        default:
                            Topic.Note(
                                "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nIn Switch:\nrosourceType == default:\n\tresourceType: {}",
                                resourceType);
                            Topic.Note(
                                "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nIn Switch:\nrosourceType == default:\nException Occurred:\n\tSCREEN POP CONFIGURATION ERROR!Unhandled resource type\n\t\tresourceType: {}",
                                resourceType);
                            /*throw new Exception("SCREEN POP CONFIGURATION ERROR! Unhandled resource type `" +
                                                resourceType + "`.");*/
                            break;
                    }

                    #endregion

                    #region Variable Data Insertion

                    //show/hide VSQs
                    htmlBodyText = htmlBodyText.Replace("(rowVsqMonthlyDepositClass)",
                        (customText.clientMonthlyDraftAmountActive ? "visible" : "hidden"));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(rowVsqClientSsnClass)",
                        (customText.clientSsnLastFourDigitsActive ? "visible" : "hidden"));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(rowVsqDobClass)",
                        (customText.clientDateOfBirthAsStringActive ? "visible" : "hidden"));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    //perform variable data replacement
                    htmlBodyText = htmlBodyText.Replace("(verified?:pass_fail)",
                        (verificationResult
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText = htmlBodyText.Replace("(response:ssn_last_four_digits)",
                        customText.clientSsnLastFourDigits);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:ssn_last_four_digits)",
                        (customText.clientSsnLastFourDigitsVerified
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText =
                        htmlBodyText.Replace("(response:date_of_birth)", customText.clientDateOfBirthAsString);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:date_of_birth)",
                        (customText.clientDateOfBirthAsStringVerified
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText =
                        htmlBodyText.Replace("(response:monthly_deposit)", customText.clientMonthlyDraftAmount);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:monthly_deposit)",
                        (customText.clientMonthlyDraftAmountVerified
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    //currently unhandled
                    htmlBodyText = htmlBodyText.Replace("(verified?:clientinfo:fullname)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    htmlBodyText = htmlBodyText.Replace("(clientinfo:fullname)", "");
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText = htmlBodyText.Replace("(verified?:clientinfo:fullname)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(clientinfo:fullname)", "");
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText = htmlBodyText.Replace("(response:phone)", "");
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:phone)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText = htmlBodyText.Replace("(verified?:clientinfo:coclient_fullname)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(clientinfo:coclient_fullname)", "");

                    htmlBodyText = htmlBodyText.Replace("(response:email)", "");
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(verified?:email)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    htmlBodyText = htmlBodyText.Replace("(response:address_full)", "");
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(verified?:address_full)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nVariable Assignment:\n\n\thtmlBodyText: {}",
                        htmlBodyText);

                    #endregion

                    #region Set Up Webbrowsers & Load Pages

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.Document.Write(htmlBodyText);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted Method this.billboardWebBrowser.Document.Write:\nParameters\n\thtmlBodyText: {}",
                        htmlBodyText);
                    this.billboardWebBrowser.Refresh();
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted Method this.billboardWebBrowser.Refresh");

                    this.verificationResultsWebBrowser.DocumentCompleted +=
                        new WebBrowserDocumentCompletedEventHandler(WebBrowser_CheckForSfLogin);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted the following code:\n\tthis.verificationResultsWebBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser_CheckForSfLogin)");
                    this.verificationResultsWebBrowser.Navigate(this._salesforceClientHighlightsUrl);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted Method this.verificationResultsWebBrowser.Navigate\nParameter\n\tthis._salesforceClientHighlightsUrl: {}",
                        this._salesforceClientHighlightsUrl);
                    //this.verificationResultsWebBrowser.Navigated += this.webBrowser_injectSfCss;

                    Process.Start(this._salesforceClientProfileUrl);
                    Topic.Note(
                        "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nExecuted Method Process.Start\nParameter\n\tthis._salesforceClientProfileUrl: {}",
                        this._salesforceClientProfileUrl);

                    #endregion
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nException occurred:\n\tScreen Pop Manager Billboard has encountered an error:\n\tException: {}",
                            ex);
                    Topic.Exception(newEx);
                }
            }
        }

        void BillboardVerificationSecurityQuestion_FormClosing(object sender, FormClosedEventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion_FormClosing(..)"))
            {
                Topic.Note("Entering and exiting method BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion_FormClosing:\nParameters\n\tsender: " + sender.ToString() + "\n\te: {}", e);
            }
        }

        private void WebBrowser_injectSfCss(object sender, WebBrowserNavigatedEventArgs e)
        {
            //can be deleted, left in for personal future reference
            using (Topic.Scope("BillboardVerificationSecurityQuestion.webBrowser_injectSfCss(..)"))
            {
                Topic.Note("Entering and exiting method BillboardVerificationSecurityQuestion.webBrowser_injectSfCss:\nParameters\n\tsender: " + sender.ToString() + "\n\te: {}", e);
            }
        }

        private void WebBrowser_CheckForSfLogin(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin(..)"))
            {
                try
                {
                    Topic.Note("Entering Method BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin:\nParameters\n\tsender: " + sender.ToString() + "/n/te: {}", e);
                    string urlAbsolutePath = e.Url.AbsolutePath;
                    Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin:\nVariable Assignment:\n\turlAbsolutePath: {}", urlAbsolutePath);
                    //string url = e.Url.ToString().Split('?')[0];
                    //string[] urlDomainElements = e.Url.GetComponents(UriComponents.Host, UriFormat.Unescaped).Split('.');
                    //string urlSubdomain = (urlDomainElements.Count() == 3) ? urlDomainElements[0] : "";

                    //this is terrible awful no good and MUST be refactored when there is more time - far, far too fragile!
                    //https://test.salesforce.com/s.gif
                    //https://test.salesforce.com/?ec=302&startURL=/apex/ClientHighlightsPanel_CS?id=0017000000v3c4iAAA
                    //https://c.salesforce.com/login-messages/promos.html?r=https://test.salesforce.com/apex/ClientHighlightsPanel_CS?id=0017000000v3c4iAAA
                    if (this.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete)
                    {
                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n");

                        if (!this._salesforceLoginSubmitted && urlAbsolutePath.Contains("login"))
                        {
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true");
                            HtmlElement elUsername =
                                this.verificationResultsWebBrowser.Document.GetElementById("username");
                            HtmlElement elUn =
                                this.verificationResultsWebBrowser.Document.All.GetElementsByName("un")[0];
                            HtmlElement elPassword =
                                this.verificationResultsWebBrowser.Document.GetElementById("password");
                            HtmlElement elLoginForm =
                                this.verificationResultsWebBrowser.Document.GetElementById("login_form");

                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true\nVariable Assignment:\n\telUsername: " + elUsername.ToString() + "\n\telUn: " + elUn.ToString() + "\n\telPassword: " + elPassword.ToString() + "\n\telLoginForm: {}", elLoginForm);

                            elUsername.SetAttribute("Value",
                                ScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony);
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true\nExecuted Method elUsername.SetAttribute\nParameters\n\tValue: \"Value\"\n\tScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony: {}", ScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony);

                            elUn.SetAttribute("Value",
                                ScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony);
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true\nExecuted Method elUn.SetAttribut\nParameters\n\tValue: \"Value\"\n\tScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony: {}", ScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony);
                            elPassword.SetAttribute("Value",
                                ScreenPopManager.FDR.DotNetClientAddin._salesforcePasswordInintelephony);
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true:\nExecuted Method elPassword.SetAttribute\nParameters\n\tValue: \"Value\"\n\tScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony: {}", ScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony);
                            elLoginForm.InvokeMember("submit");
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true\nExecuted Method elLoginForm.InvokeMember(\"submit\")");
                            this._salesforceLoginSubmitted = true;
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true:\n!this._salesforceLoginSubmitted && urlAbsolutePath.Contains(\"login\") is true\nVariable Assignment\n\tthis._salesforceLoginSubmitted: {}", this._salesforceLoginSubmitted);
                        }
                        else if (urlAbsolutePath.Contains("ClientHighlightsPanel_CS"))
                        {
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\n");
                            HtmlDocument document = this.verificationResultsWebBrowser.Document;
                            HtmlElement sfClientDataDiv = null;
                            HtmlElementCollection sfAllDivs = document.GetElementsByTagName("div");

                            int[] countsColumns = new int[] { 0, 0, 0 };
                            int indexCurrentColumn = -1;
                            string innerHtml = null;

                            HtmlElement table = null;
                            HtmlElement thead = null;
                            HtmlElement row = null;
                            HtmlElement th = null;
                            HtmlElement td = null;

                            table = document.CreateElement("TABLE");
                            thead = document.CreateElement("THEAD");
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nVariable Assignment:\n\tdocument: " + document.ToString() + "\n\tsfClientDataDiv: null\n\tsfAllDivs: " + sfAllDivs.ToString() + "\n\tcountsColumns: " + countsColumns.ToString() + "\n\tindexCurrentColumn: " + indexCurrentColumn.ToString() + "\n\tinnerHtml: null\n\ttable: null\n\tthead: null\n\trow: null\n\tth: null\n\ttd: null\n\ttable: " + table.ToString() + "\n\tthead: {}", thead);
                            table.SetAttribute("style", " width: 800px; ");
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nExecuted Method table.SetAttribute(\"style\", \" width: 800px; \":");
                            document.Body.AppendChild(table);
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nExecuted Method document.Body.AppendChild:\nParameter\n\ttable: {}", table);
                            table.AppendChild(thead);
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nExecuted Method table.AppendChild:\nParameter\n\tthead: {}", thead);

                            List<HtmlElement> rows = new List<HtmlElement>();
                            int indexCurrentRow = 0;
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nVariable Assignment\n\trows: " + rows.ToString() + "\n\tindexCurrentRow: {}", indexCurrentRow);
                            foreach (HtmlElement div in sfAllDivs)
                            {
                                Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop:\n\tCurrent value of foreach array member:\n\tdiv: {}", div);
                                switch (div.GetAttribute("className").ToString())
                                {
                                    case "slds-theme--default": //timeline
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\nIn Case div.GetAttribute(\"className\").ToString() == \"slds - theme--default\"");
                                        div.FirstChild.OuterHtml = "";

                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\nIn Case div.GetAttribute(\"className\").ToString() == \"slds - theme--default\"\nVariable Assignment:\n\tdiv.FirstChild.OuterHtml: {}", div.FirstChild.OuterHtml);
                                        break;
                                    case "slds-grid slds-m-top--x-small": //cient info
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-grid slds-m-top--x-small\"");
                                        sfClientDataDiv = div;
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-grid slds-m-top--x-small\"\nVariable Assignment:\n\tsfClientDataDiv: " + sfClientDataDiv.ToString());
                                        break;
                                    case "slds-col slds-text-heading--label": //section heading
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"", div);
                                        indexCurrentColumn++;
                                        indexCurrentRow = 0;

                                        row = rows.ElementAtOrDefault(indexCurrentRow);
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nVariable Assignment:\n\tindexCurrentColumn: " + indexCurrentColumn.ToString() + "\n\tindexCurrentRow: " + indexCurrentRow.ToString() + "\n\trow: " + row.ToString(), div);

                                        if (row == null)
                                        {
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nrow == null is true", div);
                                            row = document.CreateElement("TR");
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nrow == null is true\nVariable Assignment\n\trow: " + row.ToString(), div);
                                            rows.Insert(indexCurrentRow, row);
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nrow == null is true\nExecute Method rows.Insert\nParameters\n\tindexCurrentRow: " + indexCurrentRow.ToString() + "\n\trow " + row.ToString(), div);
                                            thead.AppendChild(row);
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nrow == null is true\nExecute Method thead.AppendChild\nParameters\n\trow " + row.ToString(), div);
                                        }

                                        indexCurrentRow++;
                                        th = document.CreateElement("TH");
                                        th.InnerHtml = div.InnerHtml;


                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nVariable Assignment:\n\tindexCurrentRow: " + indexCurrentRow.ToString() + "\n\tth: " + th.ToString() + "\n\tth.InnerHtml: " + th.InnerHtml.ToString(), div);
                                        th.SetAttribute("colspan", "2");
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nExecuted Mehtod th.SetAttribute(\"colspan\", \"2\")", div);
                                        row.AppendChild(th);
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col slds-text-heading--label\"\nExecuted Mehtod row.AppendChild:\nParameters:\n\tth: " + th.ToString(), div);
                                        break;
                                    case "slds-col": //name/value pairs
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"", div);
                                        countsColumns[indexCurrentColumn]++;
                                        row = rows.ElementAtOrDefault(indexCurrentRow);

                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nVariable Assignments:\n\tcountsColumns[indexCurrentColumn]: " + countsColumns[indexCurrentColumn].ToString() + "\n\trow: " + row.ToString(), div);
                                        if (row == null)
                                        {
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nrow == null is true", div);
                                            row = document.CreateElement("TR");
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nrow == null is true\nVariable Assignment\n\trow: {1}", div, row);
                                            rows.Insert(indexCurrentRow, row);
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nrow == null is true\nExecuted Method rows.Insert\nParameters:\n\tindexCurrentRow: " + indexCurrentRow.ToString() + "\n\trow: " + row.ToString(), div);
                                            thead.AppendChild(row);
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nrow == null is true\nExecuted Method thead.AppendChild\nParameters:\n\trow: " + row.ToString(), div);
                                        }

                                        indexCurrentRow++;
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nVariable Assignments:\n\tindexCurrentRow: " + indexCurrentRow.ToString(), div);

                                        //ridiculously overcomplicated work around to different column counts needing blank cells inserted, refactor
                                        for (int i = 0; i < indexCurrentColumn; i++)
                                        {
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nIn For Loop\nStart of For Loop:\nFor loop index value:\n\ti: " + i.ToString(), div);
                                            if (countsColumns[i] < countsColumns[indexCurrentColumn])
                                            {
                                                Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nIn For Loop\n(countsColumns[i] < countsColumns[indexCurrentColumn]) is true", div);
                                                td = document.CreateElement("TD");
                                                td.InnerHtml = "&nbsp;";
                                                Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nIn For Loop\n(countsColumns[i] < countsColumns[indexCurrentColumn]) is true\nVariable Assignment\n\ttd: " + td.ToString() + "\n\ttd.InnerHtml: " + td.InnerHtml.ToString(), div);
                                                row.AppendChild(td);
                                                Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nIn For Loop\n(countsColumns[i] < countsColumns[indexCurrentColumn]) is true\nExecuted Method row.AppendChild:\nParameters\n\ttd: {2}", div, i, td);
                                                td = document.CreateElement("TD");
                                                td.InnerHtml = "&nbsp;";
                                                Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nIn For Loop\n(countsColumns[i] < countsColumns[indexCurrentColumn]) is true\nVariable Assignment\n\ttd: {2}\n\ttd.InnerHtml {3}", div, i, td, td.InnerHtml);
                                                row.AppendChild(td);
                                                Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nIn For Loop\n(countsColumns[i] < countsColumns[indexCurrentColumn]) is true\nExecuted Method row.AppendChild\nParameters:\n\ttd: {2}", div, i, td);
                                            }
                                        }

                                        innerHtml = div.Children[0].InnerHtml;
                                        td = document.CreateElement("TD");
                                        td.InnerHtml = (!string.IsNullOrEmpty(innerHtml)) ? innerHtml : "&nbsp;";
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nVariable Assignments:\n\tinnerHtml: " + innerHtml.ToString() + "\n\ttd: " + td.ToString() + "\n\ttd.InnerHtml: " + td.InnerHtml.ToString(), div);
                                        row.AppendChild(td);
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nExecuted Method row.AppendChild:\nParamters\n\ttd: " + td.ToString(), div);
                                        innerHtml = div.Children[1].InnerHtml;
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nVariable Assignments:\n\tinnerHtml: " + innerHtml.ToString(), div);
                                        if (string.IsNullOrEmpty(innerHtml))
                                        {
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nstring.IsNullOrEmpty(innerHtml) is true", div);
                                            innerHtml = "&nbsp;";
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nstring.IsNullOrEmpty(innerHtml) is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true:\nIn Start of Forloop:\n\tCurrent value of foreach array member:\n\tdiv: {}:\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nstring.IsNullOrEmpty(innerHtml) is true\nVariable Assignment\n\tinnerHtml: " + innerHtml.ToString(), div);
                                        }
                                        else if (innerHtml.Contains("Not Checked"))
                                        {
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\n(innerHtml.Contains(\"Not Checked\")) is true", div);
                                            innerHtml = "<span style='color:red; '>X</span>";
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\n(innerHtml.Contains(\"Not Checked\")) is true\nVariable Assignment\n\tinnerHtml: " + innerHtml.ToString(), div);
                                        }
                                        else if (innerHtml.Contains("Checked"))
                                        {
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\n(innerHtml.Contains(\"Checked\")) is true", div);
                                            innerHtml = "<span style='color:green; '>&#10004;</span>";
                                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\n(innerHtml.Contains(\"Checked\")) is true\nVariable Assignment\n\tinnerHtml: " + innerHtml.ToString(), div);
                                        }

                                        td = document.CreateElement("TD");
                                        td.InnerHtml = innerHtml;
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nVariable Assignments:\n\ttd: " + td.ToString() + "\n\ttd.InnerHtml: " + td.InnerHtml.ToString(), div);
                                        row.AppendChild(td);
                                        Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nIn Start of For loop\n\tCurrent value of foreach array member:\n\tdiv: {}\nIn Case div.GetAttribute(\"className\").ToString() == \"slds-col\"\nExecuted Method row.AppendChild:\nParameters:\n\ttd: " + td.ToString(), div);
                                        break;
                                }
                            }

                            sfClientDataDiv.OuterHtml = "";
                            Topic.Note("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nIn webBrowser_CheckForSfLogin:\nthis.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete is true\nurlAbsolutePath.Contains(\"ClientHighlightsPanel_CS\") is true\nVariable Assignment\n\tsfClientDataDiv.OuterHtml: {}", sfClientDataDiv.OuterHtml);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nException Occurred in Method webBrowser_CheckForSfLogin:\nVerified Security Questions billboard error trying to log into Salesforce:]\n\tEx: {}", ex);
                    Topic.Exception(newEx);
                    this.TopMost = false;
                }
            }
        }

        private void WebBrowser_HijackLinkClicks(object sender, WebBrowserNavigatingEventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.webBrowser_HijackLinkClicks(..)"))

            {
                /*
                                string url = e.Url.ToString();
                                string urlHost = e.Url.GetComponents(UriComponents.Host, UriFormat.Unescaped);
                
                                if (url != "about:blank" && !urlHost.EndsWith("salesforce.com"))
                                {
                                    e.Cancel = true;
                                    Process.Start(e.Url.ToString());
                                }*/


                Topic.Note(
                    "Entering and exiting method BillboardVerificationSecurityQuestion.webBrowser_HijackLinkClicks:\nParameters\n\tsender: {}\n\te: " + e.ToString(),
                    sender);
            }
        }

        private void VerificationSecurityQuestion_Load(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load(..)"))
            {
                Topic.Note("Entering and exiting method BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load:\nParameters\n\tsender: {}\n\te: " + e.ToString(), sender);
            }

        }

        private void BillboardDismissButton_Click(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load(..)"))
            {
                Topic.Note("Entering Method BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load:\nParameters:\n\tsender: {}\n\te: " + e.ToString(), sender);
                this.billboardDismissButton.Enabled = false;
                Topic.Note("In BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load\nVariable Assignment:\n\tthis.billboardDismissButton.Enabled: {}", this.billboardDismissButton.Enabled);
                this.Close();
                Topic.Note("In BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load\n\nExecuted Method this.Close()");
            }
        }

        private void Billboard_Exit(object sender, System.ComponentModel.CancelEventArgs e)
        {

            using (Topic.Scope("BillboardVerificationSecurityQuestion.billboard_Exit(..)"))
            {
                try
                {
                    Topic.Note(
                        "Entering method BillboardVerificationSecurityQuestion.billboard_Exit:\nParameters\n\tsender: {}\n\te: " + e.ToString(),
                        sender);
                    // Commenting out for now.  This method was set as the FormClosingEventHandler.  The this.close() seems to do nothing more than call this event.  
                    //this.Close();
                    Topic.Note(
                        "Exiting method BillboardVerificationSecurityQuestion.billboard_Exit:\nParameters\n\tsender: {}\n\te: " + e.ToString(),
                        sender);
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardVerificationSecurityQuestion.billboard_Exit\nException occurred in Billboard_exit\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void BillboardWebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}