﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using ScreenPopManager.FDR;
using ININ.Diagnostics;

namespace ScreenPopManager.FDR.Forms
{
    public partial class BillboardFromUri : Form
    {
        #region Constants
        public const string RESOURCE_TYPE_URI = "uri";
        public const string RESOURCE_TYPE_HTML = "html";
        #endregion

        #region Force Form Always on Top
        //see http://stackoverflow.com/a/34703664/90400
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("ScreenPopManager.FDR.Forms");

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        #endregion

        public BillboardFromUri(string windowTitle, string resource, string resourceType, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("BillboardFromUri.BillboardFromUri(..)"))
            {

                Topic.Note("Entering BillboardFromUri.BillboardFromUri\nParameters\n\twindowTitle: " + windowTitle.ToString() + "\n\tresource: " + resource.ToString() + "\n\tresourceType: " + resourceType.ToString() + "\n\tcustomText: {}", customText);
                InitializeComponent();
                SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS); //force form to always be on top

                try
                {
                    string htmlBodyText = "ERROR: NO RESOURCE LOADED";
                    int browserLoadTime = 0;

                    this.Text = windowTitle;
                    this.dismissBillboardButton.Text = "Close";

                    this.billboardWebBrowser.Navigating +=
                        new WebBrowserNavigatingEventHandler(BillboardWebBrowser_HijackLinkClicks);

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.DocumentText = "0";
                    Topic.Note("In BillboardFromUri.BillboardFromUri:\nVariable Assignment:\n\thtmlBodyText: " + htmlBodyText.ToString() + "\n\tbrowserLoadTime: " + browserLoadTime.ToString() + "\n\tthis.Text: " + this.Text.ToString() + "\n\tthis.dismissBillboardButton.Text: " + this.dismissBillboardButton.Text.ToString() + "\n\tthis.billboardWebBrowser.DocumentText: {}", this.billboardWebBrowser.DocumentText);
                    this.billboardWebBrowser.Document.OpenNew(true);
                    Topic.Note("In BillboardFromUri.BillboardFromUri: Executed this.billboardWebBrowser.Document.OpenNew(true)");
                    Topic.Note("In BillboardFromUri.BillboardFromUri: Before Switch");
                    switch (resourceType)
                    {
                        case RESOURCE_TYPE_URI:
                            Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"{}\")", RESOURCE_TYPE_URI);
                            this.billboardWebBrowser.Navigate(new Uri(resource));
                            Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nExecuted method this.billboardWebBrowser.Navigate:\nParameter\n\tresource: {}", resource);
                            while (this.billboardWebBrowser.ReadyState != WebBrowserReadyState.Complete &&
                                   browserLoadTime < ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax)
                            {
                                Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nIn While loop:\nCurrent Condition Values:\n\tthis.billboardWebBrowser.ReadyState: " + this.billboardWebBrowser.ReadyState.ToString() + "\n\tWebBrowserReadyState.Complete: " + WebBrowserReadyState.Complete.ToString() + "\n\tbrowserLoadTime: " + browserLoadTime.ToString() + "\n\tScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax: {}", ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax);
                                Application.DoEvents();
                                Topic.Note(
                                    "In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nIn While loop:\nExecuted Method Aplication.DoEvents()");
                                System.Threading.Thread.Sleep(1000);
                                Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nIn While loop:\nExecuted method System.Threading.Thread.Sleep(1000)");
                                browserLoadTime++;

                            }

                            if (browserLoadTime == ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax)
                            {
                                Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nbrowserLoadTime == ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax is true");
                                htmlBodyText = "<h1>TIME OUT TRYING TO LOAD BILLBOARD HTML SCRIPT `" + resource +
                                               "`</h1>";
                                Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nbrowserLoadTime == ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax is true\nVariable Assignment:\n\thtmlBodyText: {}", htmlBodyText);
                            }
                            else
                            {
                                Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nbrowserLoadTime == ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax is false");
                                htmlBodyText = billboardWebBrowser.DocumentText;
                                Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_URI.ToString() + "\")\nbrowserLoadTime == ScreenPopManager.FDR.DotNetClientAddin.billboardBrowserLoadTimeMax is false\nVariable Assignment:\n\thtmlBodyText: {}", htmlBodyText);
                            }

                            break;
                        case RESOURCE_TYPE_HTML:
                            Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_HTML.ToString() + "\"");
                            htmlBodyText = resource;
                            Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch:\n\tresourceType (\"" + resourceType.ToString() + "\") == RESOURCE_TYPE_URI (\"" + RESOURCE_TYPE_HTML.ToString() + "\"\nVariable Assignment:\n\t\thtmlBodyText: " + htmlBodyText.ToString());
                            break;
                        default:
                            Topic.Note("In BillboardFromUri.BillboardFromUri\nIn Switch: Default:\nSCREEN POP CONFIGURATION ERROR! Unhandled resource type `" +
                                       resourceType + "`.");
                            //throw new Exception("SCREEN POP CONFIGURATION ERROR! Unhandled resource type `" +
                            //                    resourceType + "`.");
                            break;
                    }

                    //perform variable data replacement
                    htmlBodyText = htmlBodyText.Replace("(AE)", customText.agentName);
                    Topic.Note("In BillboardFromUri.BillboardFromUri\nVariable\n\thtmlBodyText {}", htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(prospect)", customText.prospectName);
                    Topic.Note("In BillboardFromUri.BillboardFromUri\nVariable\n\thtmlBodyText {}", htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(ani)", customText.clientPhone);
                    Topic.Note("In BillboardFromUri.BillboardFromUri\nVariable\n\thtmlBodyText {}", htmlBodyText);
                    htmlBodyText = htmlBodyText.Replace("(ani pretty)", customText.clientPhonePretty);
                    Topic.Note("In BillboardFromUri.BillboardFromUri\nVariable\n\thtmlBodyText {}", htmlBodyText);

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.Document.Write(htmlBodyText);
                    Topic.Note("In BillboardFromUri.BillboardFromUri\nExecuted Method this.billboardWebBrowser.Document.Write\nParameter\n\thtmlBodyText: {}", htmlBodyText);
                    //this.billboardWebBrowser.DocumentText = htmlBodyText;
                    this.billboardWebBrowser.Refresh();
                    Topic.Note("In BillboardFromUri.BillboardFromUri\nExecuted Method this.billboardWebBrowser.Refresh()");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardFromUri.BillboardFromUri:\nException occurred:\nScreen Pop Manager Billboard has encountered an error:\nError Message: {}", ex);
                    Topic.Exception(newEx);
                }
            }
        }

        private void DismissBillboardButton_Click(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.dismissBillboardButton_Click(..)"))
            {
                try
                {
                    Topic.Note("Entering dismissBillboardButton_Click:\nParameters:\n\tsender: " + sender.ToString() + "\n\te: {}", e);
                    this.dismissBillboardButton.Enabled = false;
                    Topic.Note("In BillboardFromUri.dismissBillboardButton_Click\n\tVariable Assignment:\n\tthis.dismissBillboardButton.Enabled: " + this.dismissBillboardButton.Enabled.ToString());
                    this.Close();
                    Topic.Note("In BillboardFromUri.dismissBillboardButton_Click:\n\tExecuting this.close()");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardFromUri.dismissBillboardButton_Click\nException occured in dismissBillboardButton_Click. Method Parameters:\n\tsender: " + sender.ToString() + "\n\te: " + e.ToString() + ". Failed to execute command this.close().\n Exception: {0}", ex);
                    Topic.Exception(newEx);
                }
            }
        }

        private void BillboardWebBrowser_HijackLinkClicks(object sender, WebBrowserNavigatingEventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.billboardWebBrowser_HijackLinkClicks(..)"))
            {
                try
                {
                    Topic.Note(
                        "Entering billboardWebBrowser_HijackLinkClicks:\nParameters:\n\tsender: " + sender.ToString() +
                        "\n\te: {}", e);
                    string url = e.Url.ToString();
                    Topic.Note("In billboardWebBrowser_HijackLinkClicks:Varialbe Assignment:\n\turl: {}", url);
                    if (url != "about:blank")
                    {
                        Topic.Note("In BillboardFromUri.billboardWebBrowser_HijackLinkClicks:\n\t(url != \"about: blank\" is true:");
                        e.Cancel = true;
                        Topic.Note(
                            "In BillboardFromUri.billboardWebBrowser_HijackLinkClicks:\n(url != \"about: blank\" is true:\nVarialbe Assignment\n\te.Cancel: {}",
                            e.Cancel);
                        Process.Start(e.Url.ToString());
                        Topic.Note(
                            "In BillboardFromUri.billboardWebBrowser_HijackLinkClicks:\n(url != \"about: blank\" is true:\nExecuted Method Process.Start\nParameters\n\te.Url.ToString(): {}",
                            e.Url.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Exception newex = new Exception("BillboardFromUri.billboardWebBrowser_HijackLinkClicks\nException occurred in billboardWebBrowser_HijackLinkClicks:\nException: " + ex.Message);
                    Topic.Exception(newex);
                }
            }
        }

        private void BillboardWebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.billboardWebBrowser_DocumentCompleted(..)"))
            {
                Topic.Note("Entering and Exiting billboardWebBrowser_DocumentCompleted:\nParameters\n\tsender " + sender.ToString() + "\n\te: {}", e);
            }

        }

        private void BillboardFromUri_Load(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.BillboardFromUri_Load(..)"))
            {
                Topic.Note("Entering and Exiting BillboardFromUri_Load:\nParameters\n\tsender " + sender.ToString() + "\n\te: {}", e);
            }
        }
    }
}

