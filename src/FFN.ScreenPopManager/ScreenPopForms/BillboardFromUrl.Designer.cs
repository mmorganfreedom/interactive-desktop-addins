﻿using System;
using ININ.Diagnostics;

namespace ScreenPopManager.FDR.Forms
{

    partial class BillboardFromUri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            using (Topic.Scope("BillboardFromUri.Dispose(..)"))
            {
                try
                {
                    Topic.Note("Entering method BillboardFromUri.Dispose:\nParamter\n\tdisposing: " + disposing.ToString());
                    if (disposing && (components != null))
                    {
                        Topic.Note("In method BillboardFromUri.Dispose:\ndisposing && (components != null) is true");
                        components.Dispose();
                        Topic.Note("In method BillboardFromUri.Dispose:\ndisposing && (components != null) is true:\nExectuted Mehtod components.Dispose()");
                    }

                    base.Dispose(disposing);
                    Topic.Note("In method BillboardFromUri.Dispose:\nExecuted Method base.Dispose:\nParamert\n\tdisposing: " + disposing.ToString());

                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);

                }
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.billboardWebBrowser = new System.Windows.Forms.WebBrowser();
            this.dismissBillboardButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // billboardWebBrowser
            // 
            this.billboardWebBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.billboardWebBrowser.Location = new System.Drawing.Point(12, 33);
            this.billboardWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.billboardWebBrowser.Name = "billboardWebBrowser";
            this.billboardWebBrowser.Size = new System.Drawing.Size(760, 497);
            this.billboardWebBrowser.TabIndex = 2;
            this.billboardWebBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.BillboardWebBrowser_DocumentCompleted);
            // 
            // dismissBillboardButton
            // 
            this.dismissBillboardButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dismissBillboardButton.Location = new System.Drawing.Point(12, 536);
            this.dismissBillboardButton.Name = "dismissBillboardButton";
            this.dismissBillboardButton.Size = new System.Drawing.Size(760, 23);
            this.dismissBillboardButton.TabIndex = 3;
            this.dismissBillboardButton.Text = "dismissBillboardButton";
            this.dismissBillboardButton.UseVisualStyleBackColor = true;
            this.dismissBillboardButton.Click += new System.EventHandler(this.DismissBillboardButton_Click);
            // 
            // BillboardFromUri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.dismissBillboardButton);
            this.Controls.Add(this.billboardWebBrowser);
            this.Name = "BillboardFromUri";
            this.Text = "BillboardFromUrl";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BillboardFromUri_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser billboardWebBrowser;
        private System.Windows.Forms.Button dismissBillboardButton;
    }
}