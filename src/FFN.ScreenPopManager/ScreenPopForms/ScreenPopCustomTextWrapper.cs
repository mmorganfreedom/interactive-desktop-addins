﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace ScreenPopManager.FDR
{
    public class ScreenPopCustomTextWrapper
    {
        private string _clientSsn;
        private string _clientSsnLastFourDigits;
        private string _clientPhone;


        public string clientSsn { get { return this._clientSsn; } set { if (value.Length == 9) { this._clientSsn = value;  this.clientSsnLastFourDigits = value.Substring(5, 8); } else { throw new Exception("Invalid SSN `" + value + "`; must by exactly 9 characters."); } } }
        public string clientName { get; set; }
        public string clientAddress { get; set; }
        public string clientSsnPretty => (clientSsn.Insert(2, "-")).Insert(7,"-");
        public string clientSsnLastFourDigits { get { return this._clientSsnLastFourDigits; } set { if (value.Length == 4) { this._clientSsnLastFourDigits = value; } else { throw new Exception("Invalid SSN last 4 digits - `" + value + "`; must by exactly 4 characters."); } } }
        public string clientDateOfBirthAsString { get; set; }
        public string clientMonthlyDraftAmount { get; set; }
        public string clientPhone { get { return this._clientPhone; } set { this._clientPhone = System.Text.RegularExpressions.Regex.Replace(value, "[^0-9]", ""); if (this._clientPhone.Length == 11 && (this._clientPhone.Substring(0,1) == "1")) { this._clientPhone = this._clientPhone.Substring(1, 10); } } }
        public string clientPhonePretty => (clientPhone.Insert(0, "(").Insert(4, ") ").Insert(9, "-"));
        public string coClientName { get; set; }
        public string clientEmail { get; set; }
        public string prospectName { get; set; }
        public string agentName { get; set; }
        public bool clientSsnLastFourDigitsVerified  { get; set; }
        public bool clientDateOfBirthAsStringVerified { get; set; }
        public bool clientMonthlyDraftAmountVerified { get; set; }
        public bool clientSsnLastFourDigitsActive { get; set; }
        public bool clientDateOfBirthAsStringActive { get; set; }
        public bool clientMonthlyDraftAmountActive { get; set; }

        public string clientSalesforceUrl { get; set; }
        public string clientSalesforceSummaryUrl { get; set; }



        public ScreenPopCustomTextWrapper()
        {
            this._clientSsn                        = "000000000";
            this._clientSsnLastFourDigits          = "0000";
            this.clientName                        = "NO CLIENT NAME";
            this.clientAddress                     = "NO CLIENT ADDRESS";
            this.clientDateOfBirthAsString         = "1900-01-01";
            this.clientMonthlyDraftAmount          = "0.00";
            this.clientPhone                       = "0000000000";
            this.coClientName                      = "NO CO-CLIENT NAME";
            this.clientEmail                       = "NO CLIENT EMAIL";
            this.prospectName                      = "NO PROSPECT NAME";
            this.agentName                         = "NO AGENT NAME";
            this.clientSsnLastFourDigitsVerified   = false;
            this.clientDateOfBirthAsStringVerified = false;
            this.clientMonthlyDraftAmountVerified  = false;
            this.clientSsnLastFourDigitsActive = false;
            this.clientDateOfBirthAsStringActive = false;
            this.clientMonthlyDraftAmountActive = false;
            this.clientSalesforceUrl               = "";
            this.clientSalesforceSummaryUrl        = "";
        }

    }
}
 