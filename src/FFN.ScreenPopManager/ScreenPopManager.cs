﻿///<summary>
///	Manage screen pop across addins.  Other modules contain business logic to determine whether to call a pop up; this module actually pops the requested screen.
///	<para>
///	Screen pop types:
///		- text in Windows dialog box
///		- URL in Windows dialog box browser widget
///		- URL in browser
///		- Windows form
///	</para>
///</summary>
///<jira><see cref="https://ffnitops.atlassian.net/browse/TEL-44"/></jira>
///<author>Tyler Style &lt;tstyle@bills.com&gt;</author>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.Threading;
using System.Windows.Forms;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;
using ScreenPopManager.FDR.Forms;
using ScreenPopManager.FDR.MiddlewareService;



namespace ScreenPopManager.FDR
{
    public class DotNetClientAddin : QueueMonitor
    {
        #region Constants
        public const string DATETIME_FORMAT = "yyyy-MM-ddTHH:mm:ss.fffzzzz";
        public const string SCREEN_POP_SRC_DIR = @"\\ffn-shared\phx-dc1\ININ_SCREEN_POPS\";
        public const string SCREEN_POP_CONFIG_FILENAME = "config_scripts.csv";
        public const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_FILENAME = "config_CS_Security_ID_Verification_Results.htm";
        public const string SCREEN_POP_BASE_URL = @"https://ffn.quickbase.com/db/";

        private const string CONFIG_COLNAME_CAMPAIGN = "campaign";
        private const string CONFIG_COLNAME_WORKGROUP_Q_NAME = "workgroup";
        private const string CONFIG_COLNAME_DNIS = "dnis";
        private const string CONFIG_COLNAME_SCRIPT_FILENAME = "script_filename";
        private const string CONFIG_COLNAME_SCRIPT_HTML = "script_text";

        public const string WORKGROUP_QUEUE_CLIENT_SERVICES = "cs-primary";
        public const string WORKGROUP_QUEUE_WCB = "wcb";
        public const string WORKGROUP_QUEUE_FDR_SALES = "fdrsales";
        public const string WORKGROUP_QUEUE_FDR_SALES_QA = "fdrsales-qa";
        public const string DNIS_LEAR = "8445779443";
        public const string DNIS_INFOMERCIAL = "8004601483";
        public const string ANI_TESTING = "7806804585";

        public const string HANDLER_STATUS_SUCCESS = "success";

        //ININ custom attributes
        public const string ATTRIBUTE_FF_SecurityQuestionVerification_Status = "FF_SecurityQuestionVerification_Status";
        public const string ATTRIBUTE_FF_SecurityQuestionVerification_Return = "FF_SecurityQuestionVerification_Return";
        public const string ATTRIBUTENAME_FF_ScreensPopped = "FF_ScreensPopped";

        //Screen Pop friendly names
        public const string SCREEN_POP_NAME_VSQ = "vsq";

        //billboard magic numbers
        public const int billboardBrowserLoadTimeMax = 3;  //seconds
        #endregion



        #region Variables
        private readonly string[] _serverParameterNames = { "FFN_MiddlewareServer", "FFN_IninHelpersServer", "FFN_SalesforceProductionUsernameInintelephony", "FFN_SalesforceSandboxUsernameInintelephony", "FFN_SalesforceProductionPasswordInintelephony", "FFN_SalesforceSandboxPasswordInintelephony" };
        private string _ininHelpersServerUri = "";
        private readonly object _interactionListLock = new object();
        private readonly List<IInteraction> _interactionsAlreadyProcessed = new List<IInteraction>();
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("ScreenPopManager.FDR");
        private Exception newEx;
        private InteractionsManager _interactionsManager;
        private string _htmlWindowTitle;
        private string _myhtml;
        private ScreenPopCustomTextWrapper _htmlCustomText;
        private string _verifyWindowTitle;
        private string _verifyHtml;
        private ScreenPopCustomTextWrapper _verifyCustomText;
        private string _urlWindowTitle;
        private string _urlHtml;
        private ScreenPopCustomTextWrapper _urlCustomText;

        #region  middleware overhead gobbledygook
        private SynchronizationContext _context;
        private string _qbTableId = "bg5dbsbd2";
        private DateTime _startTime;
        private Session _session;
        private string UserId { get; set; }
        private MiddlewareClient _client;
        #endregion middleware overhead gobbledygook

        #region  billboard config file entries & caches
        private Dictionary<string, Dictionary<string, string>> _configBillboardByDnisAndWorkgroupQName = new Dictionary<string, Dictionary<string, string>>();
        private Dictionary<string, Dictionary<string, string>> _configBillboardByDnis = new Dictionary<string, Dictionary<string, string>>();
        private Dictionary<string, Dictionary<string, string>> _configBillboardByWorkgroupQName = new Dictionary<string, Dictionary<string, string>>();
        private Dictionary<string, string> _configBillboard = null;

        private string _cacheSecurityQuestionVerificationPassHtml = "";

        static public string _salesforceUsernameInintelephony = "";
        static public string _salesforcePasswordInintelephony = "";
        static public string _salesforceProductionUsernameInintelephony = "";
        static public string _salesforceProductionPasswordInintelephony = "";
        static public string _salesforceSandboxUsernameInintelephony = "";
        static public string _salesforceSandboxPasswordInintelephony = "";

        static public string _salesForceUrlDomain = "";
        static public string _salesforceClientHighlightsCssUrl = "";
        static public string _salesforceClientHighlightsCssText = "";
        #endregion  billboard config file entries & caches


        #region interaction metadata
        private string _ani = null;
        private string _dnis = null;
        private string _agentName = "";
        private string _interactionWorkgroupQName = null;
        private string _clientSalesForceId = "";
        #endregion interaction metadata
        #endregion Variables




        /// <summary>
        ///     Insert attribute to watch list so this class will receive change
        ///     notifications for only those attributes.
        /// </summary>
        protected override IEnumerable<string> Attributes
        {
            get
            {
                return new[] {
                                        InteractionAttributes.State,
                                        InteractionAttributes.Direction,
                                        InteractionAttributes.CallType,
                                        InteractionAttributeName.LocalId,
                                        InteractionAttributeName.RemoteId
                                };
            }
        }

        /// <summary>
        ///     This method is called when QueueMonitorAddin is loaded from .net client.
        /// </summary>
        /// <param name="serviceProvider">This object will give you session.</param>
        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            using (Topic.Scope("DotNetClientAddin.OnLoad(...)"))
            {
                try
                {
                    _session = serviceProvider.GetService(typeof(Session)) as Session;
                    _interactionsManager = InteractionsManager.GetInstance(_session);
                    UserId = Environment.UserName;
                    _context = SynchronizationContext.Current;
                    try
                    {
                        LoadConfigFiles();
                    }
                    catch (Exception ex)
                    {
                        newEx = new Exception("In DotNetClientAddin.OnLoad\nAddin `Screen Pop Manager` has encountered an error trying to load config files." + System.Environment.NewLine + @"(most likely someone is editing a file in `\\phx-dc1\ININ_SCREEN_POPS\`)" + System.Environment.NewLine + System.Environment.NewLine + "Interaction Client / Desktop will need to be restarted in order to enable screen pops." + System.Environment.NewLine + System.Environment.NewLine + "Error message:  " + System.Environment.NewLine + ex.Message);
                        Topic.Exception(newEx);
                    }
                    CreateAdminSession();
                    WatchServerParameter();
                    DisconnectAdminSession();
                    CreateMiddlewareClient();
                    GetQbTableId();

                    ScreenPopManager.FDR.DotNetClientAddin._salesforceClientHighlightsCssUrl = "https://" + ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain + "/resource/1479988015000/SLDS/assets/styles/salesforce-lightning-design-system-ltng.css";
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.OnLoad\nScreen Pop Manager::onLoad() has encountered an error:" + System.Environment.NewLine + ex.Message);
                    Topic.Exception(newEx);
                    //throw;
                }
            }
        }



        #region InteractionsEventListeners

        /// <summary>
        ///     Listen for interaction state changes and if appropriate call related processing for screen pops.
        /// </summary>
        /// <param name="iInteraction">interaction in queue</param>
        protected override void InteractionChanged(IInteraction iInteraction)
        {
            using (Topic.Scope("DotNetClientAddin.InteractionChanged(...)"))
            {
                try
                {
                    #region Variables
                    var interactionType = iInteraction.GetAttribute(InteractionAttributes.InteractionType);
                    var callState = iInteraction.GetAttribute(InteractionAttributes.State);
                    var callDirection = iInteraction.GetAttribute(InteractionAttributes.Direction);
                    var callType = iInteraction.GetAttribute(InteractionAttributes.CallType);

                    var isCallDirectionIncoming = callDirection.Equals(InteractionAttributeValues.Direction.Incoming);
                    var isCallDirectionOutgoing = !isCallDirectionIncoming;
                    var isCallTypeExternal = callType.Equals(InteractionAttributeValues.CallType.External);
                    var isCallTypeInternal = callType.Equals(InteractionAttributeValues.CallType.Intercom);
                    var isCallStateConnected = callState.Equals(InteractionAttributeValues.State.Connected);
                    var isCallStateProceeding = callState.Equals(InteractionAttributeValues.State.Proceeding);

                    var isCallExternalInboundAnswered = isCallTypeExternal && isCallStateConnected && isCallDirectionIncoming;
                    var isCallExternalOutboundProceeding = isCallTypeExternal && isCallStateProceeding && isCallDirectionOutgoing;
                    var isCallInternalInboundAnswered = isCallTypeInternal && isCallStateConnected && isCallDirectionIncoming;

                    var isCallDisconnectedExternally = callState.Equals(InteractionAttributeValues.State.ExternalDisconnect);
                    var isCallDisconnectedInternally = callState.Equals(InteractionAttributeValues.State.InternalDisconnect);

                    var userLastConnected = iInteraction.GetAttribute("Eic_LastConnectedUser");

                    var interaction = ConvertIInteractionToInteraction(iInteraction);
                    #endregion
                    #region InteractionRemoved
                    Topic.Note("In DotNetClientAddin.InteractionChanged\nInteractionChanged hack for InteractionRemoved");

                    if (interaction.IsDisconnected)  //Disconnect
                    {
                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is true");
                        #region Calls Only
                        if (interactionType == InteractionType.Call.ToString())
                        {
                            Topic.Note("In DotNetClientAddin.InteractionChanged\ninteractionType == InteractionType.Call.ToString is true");
                            RemoveInteraction(iInteraction); // Remove the interaction from the process list
                            if(iInteraction != null)
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteractionType == InteractionType.Call.ToString is true\nExecuted RemoveInteraction\nParameter\n\tiInteraction: {}", iInteraction);
                            }
                            else
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteractionType == InteractionType.Call.ToString is true\nExecuted RemoveInteraction\nParameter\n\tiInteraction: null");
                            }
                            
                            this.unsetCallData();   //clear call specific persistent data 
                            Topic.Note("In DotNetClientAddin.InteractionChanged\ninteractionType == InteractionType.Call.ToString is true\nExecuted unsetCallData");
                        }
                        #endregion
                    }
                    #endregion
                    else
                    {
                        #region Calls Only
                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false");
                        if (interactionType == InteractionType.Call.ToString())
                        {
                            Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\ninteractionType == InteractionType.Call.ToString() is true");
                            if(iInteraction.InteractionId != null)
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\niInteraction.InteractionId: " + iInteraction.InteractionId.ToString());

                            }
                            else
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\niInteraction.InteractionId: null");
                            }
                            if (callDirection != null)
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\nCall Direction:  " + callDirection.ToString());
                            }
                            else
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\nCall Direction:  null");
                            }
                            if (callState != null)
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\nCall State:  " + callState.ToString());
                            }
                            else
                            {
                                Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\nCall State:  null");
                            }

                            if (!_interactionsAlreadyProcessed.Contains(iInteraction))
                            {
                                if (iInteraction != null)
                                {
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nParameter\n\tiInteraction: {}", iInteraction);
                                }
                                else
                                {
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nParameter\n\tiInteraction: null");
                                }

                                this.setCallData(interaction);
                                if (interaction != null)
                                {
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nExecuted unsetCallData\nParameter\n\tinteraction: {}", interaction);
                                }
                                else
                                {
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nExecuted unsetCallData\nParameter\n\tinteraction: null");
                                }
                                if (isCallExternalInboundAnswered)
                                {
                                    // Answered External Inbound
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalInboundAnswered is true");
                                    AnsweredExternalInbound(iInteraction);
                                    if (interaction != null)
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalInboundAnswered is true\nExecuted AnsweredExternalInbound\nParameter\n\tiInteraction: {}", iInteraction);
                                    }
                                    else
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalInboundAnswered is true\nExecuted AnsweredExternalInbound\nParameter\n\tiInteraction: null");
                                    }
                                    AddInteraction(iInteraction);
                                    if (iInteraction != null)
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalInboundAnswered is true\nExecuted AddInteraction\nParameter:\n\tiInteraction: {}", iInteraction);
                                    }
                                    else
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalInboundAnswered is true\nExecuted AddInteraction\nParameter:\n\tiInteraction: null");
                                    }
                                    }

                                else if (isCallExternalOutboundProceeding)
                                {
                                    // Ringing External Outbound
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalOutboundProceeding is true");
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalOutboundProceeding is true\nInteraction is ringing external outbound");
                                    RingingExternalOutbound(iInteraction);
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalOutboundProceeding is true\nExecuted RingingExternalOutbound\nParameter\n\tiInteraction: {}", iInteraction);
                                    AddInteraction(iInteraction);
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallExternalOutboundProceeding is true\nExecuted AddInteraction\nParameter\n\tiInteraction: {}", iInteraction);
                                }
                                else if (isCallInternalInboundAnswered)
                                {
                                    // Answered Internal Inbound
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallInternalInboundAnswered is true");
                                    Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallInternalInboundAnswered is true\nInteraction is answered internal inbound");
                                    AnsweredInternalInbound(iInteraction);
                                    if (iInteraction != null)
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallInternalInboundAnswered is true\nExecuted AnsweredInternalInbound\nParameter\n\tiInteraction: {}", iInteraction);
                                    }
                                    else
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallInternalInboundAnswered is true\nExecuted AnsweredInternalInbound\nParameter\n\tiInteraction: null");
                                    }
                                    
                                    AddInteraction(iInteraction);
                                    if (iInteraction != null)
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallInternalInboundAnswered is true\nExecuted AddInteraction\nParameter\n\tiInteraction: {}", iInteraction);
                                    }
                                    else
                                    {
                                        Topic.Note("In DotNetClientAddin.InteractionChanged\ninteraction.IsDisconnected is false\n!_interactionsAlreadyProcessed.Contains(iInteraction) is true\nisCallInternalInboundAnswered is true\nExecuted AddInteraction\nParameter\n\tiInteraction: null");
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.InteractionChanged\nException occured in InteractionChanged method\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        /// <summary>
        ///     This method is called when the interaction is removed from queue.
        /// </summary>
        /// <param name="interaction">interaction in queue</param>
        protected override void InteractionRemoved(IInteraction interaction)
        {
            using (Topic.Scope("DotNetClientAddin.InteractionRemoved(...)"))
            {
                try
                {
                    base.InteractionRemoved(interaction);

                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.InteractionRemoved\nException occured in InteractionRemoved\n" + ex.Message);
                    Topic.Exception(newEx);
                }
                
            }
        }

        #endregion InteractionsEventHandlers



        #region InteractionEventHandlers

        private void RingingExternalOutbound(IInteraction ringingInteraction)
        {
            using (Topic.Scope("DotNetClientAddin.RingingExternalOutbound(...)"))
            {
                try
                {
                    var interaction = ConvertIInteractionToInteraction(ringingInteraction);
                    var ani = TrimPhoneNumber(GetInteractionAttributeValue(interaction, InteractionAttributeName.RemoteId));
                    _startTime = DateTime.Now;
                    SetInteractionAttributeValue(interaction, "FF_StartTime", DateTime.Now.ToString(DATETIME_FORMAT));
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.RingingExternalOutbound\nException occured in RingingExternalOutbound\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void AnsweredInternalInbound(IInteraction answeredInteraction)
        {
            using (Topic.Scope("DotNetClientAddin.AnsweredInteralInbound(...)"))
            {
                try
                {
                    var interaction = ConvertIInteractionToInteraction(answeredInteraction);
                    _startTime = DateTime.Now;
                    SetInteractionAttributeValue(interaction, "FF_StartTime", DateTime.Now.ToString(DATETIME_FORMAT));
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.AnsweredInteralInbound\nException occured in AnsweredInternalInbound\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void AnsweredExternalInbound(IInteraction answeredInteraction)
        {
            using (Topic.Scope("DotNetClientAddin.AnsweredExternalInbound(...)"))
            {
                try
                { 
                    string billboardHtml = "";
                    string attributeValue_FF_SecurityQuestionVerification_Status = answeredInteraction.GetAttribute(ATTRIBUTE_FF_SecurityQuestionVerification_Status);
                    string attributeValue_FF_ScreensPopped = answeredInteraction.GetAttribute(ATTRIBUTENAME_FF_ScreensPopped);
                    ScreenPopCustomTextWrapper customText = new ScreenPopCustomTextWrapper();
                    customText.clientPhone = this._ani;
                    #region Security Verification Question Billboards

                    if (!attributeValue_FF_ScreensPopped.Contains(SCREEN_POP_NAME_VSQ))
                    {
                        attributeValue_FF_ScreensPopped += "---" + SCREEN_POP_NAME_VSQ;
                        answeredInteraction.SetAttribute(ATTRIBUTENAME_FF_ScreensPopped, attributeValue_FF_ScreensPopped);
                       

                        switch (attributeValue_FF_SecurityQuestionVerification_Status)
                        {
                            case "":
                                break;
                            case "abort":
                                break;
                            case HANDLER_STATUS_SUCCESS:
                                billboardHtml = this._cacheSecurityQuestionVerificationPassHtml;
                                string[] securityQuestionResponsesText = answeredInteraction.GetAttribute(ATTRIBUTE_FF_SecurityQuestionVerification_Return).Split('~');
                                int securityQuestionResponseCount = securityQuestionResponsesText.Count();
                                string[][] securityQuestionResponses = new string[securityQuestionResponseCount][];
                                string[] securityQuestionResponse;
                                bool currentSecurityQuestionVerified = false;
                                foreach (string securityQuestionResponseText in securityQuestionResponsesText)
                                {
                                    securityQuestionResponse = securityQuestionResponseText.Split('^');
                                    currentSecurityQuestionVerified = (securityQuestionResponse[1] == "1");
                                    switch (securityQuestionResponse[0])
                                    {
                                        case "client_ssn":
                                            customText.clientSsnLastFourDigits = securityQuestionResponse[3];
                                            customText.clientSsnLastFourDigitsVerified =
                                                currentSecurityQuestionVerified;
                                            customText.clientSsnLastFourDigitsActive = true;
                                            break;
                                        case "client_dob":
                                            customText.clientDateOfBirthAsString = securityQuestionResponse[3];
                                            customText.clientDateOfBirthAsStringVerified =
                                                currentSecurityQuestionVerified;
                                            customText.clientDateOfBirthAsStringActive = true;
                                            break;
                                        case "fdr_monthly_draft_amount":
                                            customText.clientMonthlyDraftAmount = securityQuestionResponse[3];
                                            customText.clientMonthlyDraftAmountVerified =
                                                currentSecurityQuestionVerified;
                                            customText.clientMonthlyDraftAmountActive = true;
                                            break;
                                    }
                                }
                                break;
                            default:
                                billboardHtml = "<h1 style='color:red; '>Security Verification ERROR</h1>";
                                billboardHtml += "<h2 style='color:red; '>Error message:</h2>";
                                billboardHtml += "<pre>" + attributeValue_FF_SecurityQuestionVerification_Status + "</pre>";
                                break;
                        }

                        if (String.IsNullOrWhiteSpace(this._clientSalesForceId) || this._clientSalesForceId == "undefined")
                        {
                            customText.clientSalesforceUrl = "https://" + ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain + "/console#/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=001&str=" + this._ani;
                            customText.clientSalesforceSummaryUrl = "";
                            //Commenting out billboard from being popped. This is a fix for the Red state pop ups.  This will prevent warning for on verified calls, but
                            //since we are planning on removing this functionality it should not matter.

                            /*The code below was added in order block the Salesforce billboard from popping. Wholesalers will not have a salesforce account so no need in popping. */
                            Interaction interaction = ConvertIInteractionToInteraction(answeredInteraction);
                            var dnis = TrimPhoneNumber(GetInteractionAttributeValue(interaction, "Eic_SipNumberLocal"));
                            var wholesaler = GetMatchingWholesalerRecord(dnis);
                            if (wholesaler == null)
                            {
                                PopBillBoardFromHtml("NO CLIENT ASSOCIATED WITH THIS PHONE NUMBER", "<h1>No Salesforce client found with phone number (ani pretty)</h1>", customText);

                            }
                        }
                        else
                        {
                            customText.clientSalesforceUrl = "https://" + ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain + "/console#/" + this._clientSalesForceId + "? nooverride=1";
                            customText.clientSalesforceSummaryUrl = "https://" + ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain + "/apex/ClientHighlightsPanel_CS?id=" + this._clientSalesForceId;

                            PopBillBoardVerificationSecurityQuestionsFromHtml("Security Verification Questions Status", billboardHtml, customText);
                        }
                    }
                    #endregion

                    #region Script Billboards
                    this._configBillboard = null;

                    //ugh, need to refactor this...
                    //determine which billboard to pop
                    //order of precedence:  
                    //  DNIS ### + Workgroup @@@ billboards 
                    //  DNIS  *  + Workgroup @@@ billboards
                    //  DNIS ### + Workgroup  *  billboards 
                    this._configBillboardByDnisAndWorkgroupQName.TryGetValue((this._dnis + this._interactionWorkgroupQName), out this._configBillboard);
                    if (this._configBillboard == null)
                    {
                        this._configBillboardByWorkgroupQName.TryGetValue(this._interactionWorkgroupQName, out this._configBillboard);
                    }

                    if (this._configBillboard == null)
                    {
                        this._configBillboardByDnis.TryGetValue(this._dnis, out this._configBillboard);
                    }
                    //populate billboard
                    if (this._configBillboard != null)
                    {
                        if (string.IsNullOrWhiteSpace(this._configBillboard[CONFIG_COLNAME_SCRIPT_HTML]))
                        {
                            this._configBillboard[CONFIG_COLNAME_SCRIPT_HTML] = System.IO.File.ReadAllText(SCREEN_POP_SRC_DIR + this._configBillboard[CONFIG_COLNAME_SCRIPT_FILENAME]);
                        }
                        customText = this.populateScreenPopCustomTextFromQbByAni();
                        PopBillBoardFromHtml(this._configBillboard[CONFIG_COLNAME_CAMPAIGN], this._configBillboard[CONFIG_COLNAME_SCRIPT_HTML], customText);
                    }
                    else
                    {
                        Topic.Note("In DotNetClientAddin.AnsweredExternalInbound\nthis._configBillboard != null is false\nERROR this._configBillboard is still NULL: Currently in AnsweredExternalInbound. This is Else was missing.  Currently doesn't do anything but print this message.");
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.AnsweredExternalInbound\nException in Method AnsweredExternalInbound: Screen Pop Manager has encountered an error:" + System.Environment.NewLine + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }



        #endregion



        #region InteractionProcessList

        private void AddInteraction(IInteraction interaction)
        {
            using (Topic.Scope("DotNetClientAddin.AddInteraction(..)"))
            {
                try
                {
                    lock (_interactionListLock)
                    {

                        if (_interactionsAlreadyProcessed.Contains(interaction))
                        {
                            Topic.Note("In DotNetClientAddin.AddInteraction\n_interactioListLock Locked.\n_interactionsAlreadyProcessed.Contains(interaction) is true:\nExiting AddIneraction");
                        }
                        else
                        {
                            Topic.Note("In DotNetClientAddin.AddInteraction\n_interactioListLock Locked.\n_interactionsAlreadyProcessed.Contains(interaction) is false");
                            _interactionsAlreadyProcessed.Add(interaction);
                        }
                    }
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.AddInteraction\nException occurred in AddInteraction method: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void RemoveInteraction(IInteraction interaction)
        {
            using (Topic.Scope("DotNetClientAddin.RemoveInteraction(...)"))
            {
                try
                {
                    lock (_interactionListLock)
                    {
                        if (!_interactionsAlreadyProcessed.Contains(interaction))
                        {
                            Topic.Note("In DotNetClientAddin.RemoveInteraction\n_interactioListLock Locked.\n!_interactionsAlreadyProcessed.Contains(interaction) is true");
                        }
                        else
                        {
                            Topic.Note("In DotNetClientAddin.RemoveInteraction\n_interactioListLock Locked.\n!_interactionsAlreadyProcessed.Contains(interaction) is false");
                            _interactionsAlreadyProcessed.Remove(interaction);
                        }
                    }
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.RemoveInteraction\nException occured in method RemoveInteraction: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        #endregion InteractionProcessList



        #region Helper Methods

        public static string populateVariableDataInHtml(string htmlBodyText, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("DotNetClientAddin.populateVariableDataInHtml(..)"))
            {
                try
                {
                    //perform variable data replacement
                    htmlBodyText = htmlBodyText.Replace("(AE)", customText.agentName);
                    htmlBodyText = htmlBodyText.Replace("(prospect)", customText.prospectName);
                    return htmlBodyText;
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.populateVariableDataInHtml\nException occurred in Method populateVariableDataInHtml: " + ex.Message);
                    Topic.Exception(exNew);
                    //throw;
                    //Code below replaced throw.  May need to put it back
                    htmlBodyText = "";
                    return htmlBodyText;
                }
            }
        }

        private void unsetCallData()
        {
            using (Topic.Scope("DotNetClientAddin.unsetCallData()"))
            {
                try
                {
                    this._ani = null;
                    this._dnis = null;
                    this._interactionWorkgroupQName = "";
                    this._configBillboard = null;

                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.unsetCallData\nException occurred in Method unsetCallData: " + ex.Message);
                    Topic.Exception(exNew);
                }

            }
        }

        private void setCallData(Interaction interaction)
        {
            using (Topic.Scope("DotNetClientAddin.setCallData(..)"))
            {
                try
                {
                    string interactionWorkgroupName = interaction.WorkgroupQueueName.ToLower();
                    this._agentName = interaction.GetStringAttribute("Eic_LocalName");
                    this._ani = TrimPhoneNumber(
                    GetInteractionAttributeValue(interaction, InteractionAttributeName.RemoteId));
                    this._dnis = TrimPhoneNumber(GetInteractionAttributeValue(interaction, "Eic_SipNumberLocal"));
                    this._interactionWorkgroupQName = interactionWorkgroupName;
                    this._clientSalesForceId = interaction.GetStringAttribute("FF_CRMid");
                    ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain = interaction.GetStringAttribute("SF_URL_DOMAIN");
                    ScreenPopManager.FDR.DotNetClientAddin._salesforceUsernameInintelephony = (ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain == "test.salesforce.com")
                        ? ScreenPopManager.FDR.DotNetClientAddin._salesforceSandboxUsernameInintelephony
                        : ScreenPopManager.FDR.DotNetClientAddin._salesforceProductionUsernameInintelephony;
                    ScreenPopManager.FDR.DotNetClientAddin._salesforcePasswordInintelephony =
                    (ScreenPopManager.FDR.DotNetClientAddin._salesForceUrlDomain == "test.salesforce.com")
                        ? ScreenPopManager.FDR.DotNetClientAddin._salesforceSandboxPasswordInintelephony
                        : ScreenPopManager.FDR.DotNetClientAddin._salesforceProductionPasswordInintelephony;
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.setCallData\nException occurred in Method setCallData: " + ex.Message);
                    Topic.Exception(exNew);
                }
            }
        }

        private ScreenPopCustomTextWrapper populateScreenPopCustomTextFromQbByAni()
        {
            using (Topic.Scope("DotNetClientAddin.populateScreenPopCustomTextFromQbByAni()"))
            {
                try
                {
                    //var qbRecordSet                       = _client.QbGetLeadByAni(this._ani);
                    ScreenPopCustomTextWrapper customText = new ScreenPopCustomTextWrapper();
                    customText.prospectName = "";
                    customText.agentName = this._agentName;

                    /*if (qbRecordSet.Records.Any())
                    {
                            var qbRecord      = qbRecordSet.Records[0];
                            var qbLeadId      = qbRecord.RecordId;
                            var leadDataFPlus = _client.QbGetLeadDataRequiredByFreedomPlus(qbLeadId);
        
                            if (leadDataFPlus.Records.Any())
                            {
                                    customText.prospectName = leadDataFPlus.Records[0].ClientFirst + " " + leadDataFPlus.Records[0].ClientLast;
                            }
                            else
                            {
                                    customText.prospectName = "-";
                            }
                    }*/
                    return customText;
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("DotNetClientAddin.populateScreenPopCustomTextFromQbByAni\nException occurred in Method populateScreenPopCustomTextFromQbByAni: " + ex.Message);
                    Topic.Exception(exNew);
                    //throw;
                    //Code below replaced throw.  May need to put it back
                    ScreenPopCustomTextWrapper customText = new ScreenPopCustomTextWrapper();
                    return customText;
                }
            }
        }

        private void CreateMiddlewareClient()
        {
            using (Topic.Scope("DotNetClientAddin.CreateMiddlewareClient()"))
            {
                try
                {
                    var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };
                    var clientEndpoint = new EndpointAddress(MiddlewareServiceEndpoint);

                    clientBinding.MaxReceivedMessageSize = 20000000;
                    clientBinding.MaxBufferPoolSize = 20000000;
                    clientBinding.MaxBufferSize = 20000000;
                    _client = new MiddlewareClient(clientBinding, clientEndpoint);
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.CreateMiddlewareClient\nException occurred in Method CreateMiddlewareClient: " + ex.Message);
                    Topic.Exception(exNew);
                }
            }
        }

        private Interaction ConvertIInteractionToInteraction(IInteraction iinteraction)
        {
            Interaction ret = null;

            using (Topic.Scope("DotNetClientAddin.ConvertIInteractionToInteraction(..)"))
            {
                try
                {
                    ret = _interactionsManager.CreateInteraction(new InteractionId(iinteraction.InteractionId));
                    return ret;
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.ConvertIInteractionToInteraction\nException occurred in Method ConvertIInteractionToInteraction: " + ex.Message);
                    Topic.Exception(exNew);
                    //throw;
                    //Code below replaced throw.  May need to put it back
                    ret = null;
                    return ret;
                }
            }
        }

        private static string GetInteractionAttributeValue(Interaction interaction, string attributeName)
        {
            var ret = "";

            using (Topic.Scope("DotNetClientAddin.GetInteractionAttributeValue(...)"))
            {
                try
                {
                    ret = interaction.GetStringAttribute(attributeName);
                    return ret;
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.GetInteractionAttributeValue\nException occurred in Method GetInteractionAttributeValue: " + ex.Message);
                    Topic.Exception(exNew);
                    return exNew.Message;
                }
            }

        }

        private static void SetInteractionAttributeValue(Interaction interaction, string attributeName, string attributeValue)
        {
            using (Topic.Scope("DotNetClientAddin.SetInteractionAttributeValue(..)"))
            {
                try
                {
                    interaction.SetStringAttribute(attributeName, attributeValue);
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.SetInteractionAttributeValue\nException occurred in Method SetInteractionAttributeValue: " + ex.Message);
                    Topic.Exception(exNew);
                }
            }
        }

        private WholesalersResponse.Wholesaler GetMatchingWholesalerRecord(string dnis)
        {
            var ret = new WholesalersResponse.Wholesaler();
            try
            {
                Topic.Note("Searching for a wholesaler with the DNIS {}", dnis);
                CreateMiddlewareClient();
                ret = _client.GetWholesalers().WholesalerList.FirstOrDefault(x => x.Dnis.Equals(dnis));
                if (ret != null)
                {
                    Topic.Note("Wholesaler Name: {}", ret.Name);
                    Topic.Note("Wholesaler DNIS: {}", ret.Dnis);
                    Topic.Note("Wholesaler Transfer Destination: {}", ret.TransferDestination);
                }
                else
                {
                    Topic.Warning("No wholesaler matching the DNIS {} could be found!", dnis);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in wholesale Method: " + ex.Message);
                Topic.Exception(ex);
            }
            return ret;
        }

        private static string TrimPhoneNumber(string ani)
        {
            var ret = "";

            using (Topic.Scope("DotNetClientAddin.TrimPhoneNumber(..)"))
            {
                try
                {
                    ret = ani.Replace("(", "");
                    ret = ret.Replace(")", "");
                    ret = ret.Replace(" ", "");
                    ret = ret.Replace("-", "");
                    ret = ret.Replace("+", "");
                    ret = ret.Replace("sip:", "");
                    ret = ret.Replace("ip:", "");
                    if (ret.Length > 10)
                    {
                        Topic.Note("ret.Length > 10 is true");
                        ret = ret.Substring(1, 10);
                        Topic.Note("Variable Assignment:\n\tret: {}", ret);
                    }
                    return ret;
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.TrimPhoneNumber\nException occurred in Method TrimPhoneNumber: " + ex.Message);
                    Topic.Exception(exNew);
                    //throw;
                    //Code below replaced throw.  May need to put it back
                    return "";
                }
            }

        }

        [STAThread]
        private void PopBillBoardVerificationSecurityQuestionsFromHtmlThread()
        {
            using (Topic.Scope("DotNetClientAddin.PopBillBoardVerificationSecurityQuestionsFromHtmlThread().."))
            {
                try
                {
                    string windowTitle = _verifyWindowTitle;
                    string html = _verifyHtml;
                    ScreenPopCustomTextWrapper customText = _verifyCustomText;
                    BillboardVerificationSecurityQuestion Billboard =
                        new BillboardVerificationSecurityQuestion(windowTitle, html,
                            BillboardFromUri.RESOURCE_TYPE_HTML, customText);
                    Billboard.FormClosing += (s, e) => Application.ExitThread();
                    
                    Billboard.Show();
                    Application.Run();
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In DotNetClientAddin.PopBillBoardVerificationSecurityQuestionsFromHtmlThread:\nExcetption Occurreed:\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }

        }

        [STAThread]
        private void PopBillBoardVerificationSecurityQuestionsFromHtml(string windowTitle, string html, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("DotNetClientAddin.PopBillBoardVerificationSecurityQuestionsFromHtml(..)"))
            {
                try
                {
                    if (customText == null)
                    {
                        customText = new ScreenPopCustomTextWrapper();
                    }

                    //the WebBrowser form control requires single threading because stupid.  Also, this is not my forte, so very likely I'm doing it wrong :/
                    this._verifyWindowTitle = windowTitle;
                    this._verifyHtml = html;
                    this._verifyCustomText = customText;
                    var billboardThread = new Thread(new ThreadStart(PopBillBoardVerificationSecurityQuestionsFromHtmlThread));

                    billboardThread.SetApartmentState(ApartmentState.STA);
                    billboardThread.IsBackground = true;
                    billboardThread.Start();

                    //while (billboardThread.IsAlive) ;

                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.PopBillBoardVerificationSecurityQuestionsFromHtml\nException occurred in Method PopBillBoardVerificationSecurityQuestionsFromHtml: " + ex.Message);
                    Topic.Exception(exNew);

                }
            }
        }

        [STAThread]
        private void PopBillBoardFromHtml(string windowTitle, string html, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("DotNetClientAddin.PopBillBoardFromHtml(..)"))
            {
                try
                {
                    if (customText == null)
                    {
                        customText = new ScreenPopCustomTextWrapper();
                    }

                    //the WebBrowser form control requires single threading because stupid.  Also, this is not my forte, so very likely I'm doing it wrong :/
                    this._htmlWindowTitle = windowTitle;
                    this._myhtml = html;
                    this._htmlCustomText = customText;
                    var billboardThread = new Thread(new ThreadStart(PopBillBoardFromHtmlThread));
                    billboardThread.SetApartmentState(ApartmentState.STA);
                    billboardThread.IsBackground = true;
                    billboardThread.Start();

                    /*while (billboardThread.IsAlive)
		            {
		                Topic.Note("Waiting for PopBillBoardFromHtml Thread to die.");
		                Thread.Sleep(100);
		            } */
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.PopBillBoardFromHtml\nException occurred in Method PopBillBoardFromHtml: " + ex.Message);
                    Topic.Exception(exNew);
                }
            }
        }

        [STAThread]
        private void PopBillBoardFromHtmlThread()

        {
            using (Topic.Scope())
            {
                try
                {
                    string windowTitle = this._htmlWindowTitle;
                    string html = this._myhtml;
                    ScreenPopCustomTextWrapper customText = this._htmlCustomText;
                    BillboardFromUri Billboard = new BillboardFromUri(windowTitle, html,
                        BillboardFromUri.RESOURCE_TYPE_HTML, customText);
                    Billboard.FormClosing += (s, e) => Application.ExitThread();
                    Billboard.Show();
                    Application.Run();
                }
                catch (Exception ex)
                {
                     newEx =
                        new Exception(
                            "In DotNetClientAddin.PopBillBoardFromHtmlThread:\nException occurred.\nException: " +
                            ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        [STAThread]
        private void PopBillBoardFromUri(string windowTitle, string uri, ScreenPopCustomTextWrapper customText)
        {
            //the WebBrowser form control requires single threading because stupid.
            using (Topic.Scope("DotNetClientAddin.PopBillBoardFromUri(..)"))
            {
                try
                {
                    this._urlWindowTitle = windowTitle;
                    this._urlHtml = uri;
                    this._urlCustomText = customText;
                    var billboardThread = new Thread(new ThreadStart(PopBillBoardFromUriThread));

                    billboardThread.SetApartmentState(ApartmentState.STA);
                    billboardThread.IsBackground = true;

                    billboardThread.Start();
                    /*
		            while (billboardThread.IsAlive)
		            {
                        Topic.Note("Waiting for PopBillBoardFromUri Thread to die.");
                        Thread.Sleep(100);
		            }*/
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.PopBillBoardFromUri\nException occurred in Method PopBillBoardFromUri: " + ex.Message);
                    Topic.Exception(exNew);
                }
            }
        }

        [STAThread]
        private void PopBillBoardFromUriThread()
        {
            using (Topic.Scope("DotNetClientAddin.PopBillBoardFromUriThread(..)"))
            {
                try
                {
                    string windowTitle = this._urlWindowTitle;
                    string uri = this._urlHtml;
                    ScreenPopCustomTextWrapper customText = this._urlCustomText;

                    var Billboard = new BillboardFromUri(windowTitle, uri, BillboardFromUri.RESOURCE_TYPE_URI,
                        customText);
                    Billboard.FormClosing += (s, e) => Application.ExitThread();
                    Billboard.Show();
                    Application.Run();
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.PopBillBoardFromUriThread\nException occurred:\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        /**
		 *  <author email="mailto:tstyle@bills.com">
		 *      Tyler Style
		 *  </author>
		 *  <summary>
		 *      Load the screen pop control logic values from a configuration file.
		 *  </summary>
		 *  <remarks>
		 *      The configuration file edit can be edited by end users to add new screen pops without Telecom involvement.
		 *      JIRA TEL-78 (https://ffnitops.atlassian.net/browse/TEL-78)
		 *  </remarks> 
		 **/
        private void LoadConfigFiles()
        {
            using (Topic.Scope("DotNetClientAddin.LoadConfigFiles()"))
            {
                try
                {
                    string[] lines = System.IO.File.ReadLines(SCREEN_POP_SRC_DIR + SCREEN_POP_CONFIG_FILENAME)
                        .ToArray();
                    string[][] config = System.IO.File.ReadLines(SCREEN_POP_SRC_DIR + SCREEN_POP_CONFIG_FILENAME)
                        .Select(x => x.Split(',')).ToArray();
                    bool isFirstRow = true;
                    int rowCampaignColumnIndex = -1;
                    int rowWorkgroupQColumnIndex = -1;
                    int rowDnisColumnIndex = -1;
                    int rowScriptFilenameColumnIndex = -1;
                    int rowCurrentColumnIndex = 0;
                    Dictionary<string, string> currentConfigEntry = null;

                    this._cacheSecurityQuestionVerificationPassHtml =
                        System.IO.File.ReadAllText(SCREEN_POP_SRC_DIR +
                                                   SCREEN_POP_SECURITY_QUESTION_VERIFCATION_FILENAME);
                    #region Load Billboard Pop Timing Rules

                    if (config == null)
                    {
                        /*MessageBox.Show(
                            "Could not load Screen Pop Manager configuration file `" + SCREEN_POP_SRC_DIR +
                            SCREEN_POP_CONFIG_FILENAME + "`.", "Error!");*/
                        Topic.Note("In DotNetClientAddin.LoadConfigFiles\nconfig == null is true\nCould not load Screen Pop Manager configuration file `" + SCREEN_POP_SRC_DIR +
                                   SCREEN_POP_CONFIG_FILENAME + "`.", "Error!");
                    }
                    else
                    {
                        foreach (string[] row in config)
                        {
                            if (isFirstRow)
                            {
                                rowCurrentColumnIndex = 0;
                                foreach (string value in row)
                                {
                                    switch (value.ToLower())
                                    {
                                        case CONFIG_COLNAME_CAMPAIGN:
                                            rowCampaignColumnIndex = rowCurrentColumnIndex;
                                            break;
                                        case CONFIG_COLNAME_WORKGROUP_Q_NAME:
                                            rowWorkgroupQColumnIndex = rowCurrentColumnIndex;
                                            break;
                                        case CONFIG_COLNAME_DNIS:
                                            rowDnisColumnIndex = rowCurrentColumnIndex;
                                            break;
                                        case CONFIG_COLNAME_SCRIPT_FILENAME:
                                            rowScriptFilenameColumnIndex = rowCurrentColumnIndex;
                                            break;
                                        default:
                                            break;
                                    }
                                    rowCurrentColumnIndex++;
                                }

                                isFirstRow = false;
                            }
                            else
                            {
                                currentConfigEntry = new Dictionary<string, string>();
                                currentConfigEntry[CONFIG_COLNAME_DNIS] = row[rowDnisColumnIndex].Trim();
                                currentConfigEntry[CONFIG_COLNAME_CAMPAIGN] = row[rowCampaignColumnIndex].Trim();
                                currentConfigEntry[CONFIG_COLNAME_WORKGROUP_Q_NAME] =
                                    row[rowWorkgroupQColumnIndex].Trim().ToLower();
                                currentConfigEntry[CONFIG_COLNAME_SCRIPT_FILENAME] =
                                    row[rowScriptFilenameColumnIndex].Trim();
                                currentConfigEntry[CONFIG_COLNAME_SCRIPT_HTML] = "";
                                
                                if (currentConfigEntry[CONFIG_COLNAME_DNIS] == "*" &&
                                    currentConfigEntry[CONFIG_COLNAME_WORKGROUP_Q_NAME] == "*")
                                {
                                    //illegal config entry
                                    /* MessageBox.Show(
                                        "Warning: Screen Pop Manager configuration file has a bad wildcard entry: " +
                                        System.Environment.NewLine + System.Environment.NewLine + "`" +
                                        String.Join(",", row) + "`.", "Screen Pop config has a problem..."); */
                                    continue;
                                }
                                else if (currentConfigEntry[CONFIG_COLNAME_WORKGROUP_Q_NAME] == "*")
                                {
                                    this._configBillboardByDnis[currentConfigEntry[CONFIG_COLNAME_DNIS]] =
                                        currentConfigEntry;
                                }
                                else if (currentConfigEntry[CONFIG_COLNAME_DNIS] == "*")
                                {
                                    this._configBillboardByWorkgroupQName[
                                        currentConfigEntry[CONFIG_COLNAME_WORKGROUP_Q_NAME]] = currentConfigEntry;
                                }
                                else
                                {
                                    this._configBillboardByDnisAndWorkgroupQName[
                                        currentConfigEntry[CONFIG_COLNAME_DNIS] +
                                        currentConfigEntry[CONFIG_COLNAME_WORKGROUP_Q_NAME]] = currentConfigEntry;
                                }

                                currentConfigEntry = null;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception exNew = new Exception("In DotNetClientAddin.LoadConfigFiles\nException occurred in Method LoadConfigFiles: " + ex.Message);
                    Topic.Exception(exNew);
                }
            }
            #endregion
        }

        #endregion



        #region Screen Pop Methods

        private void ScreenPopUrl(string url)
        {
            using (Topic.Scope("DotNetClientAddin.ScreenPopUrl(..)"))
            {
                try
                {
                    Process.Start(url);
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.ScreenPopUrl\nException occurred in ScreenPopUrl:\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        private void GetQbTableId()
        {
            using (Topic.Scope("DotNetClientAddin.GetQbTableId()"))
            {
                try
                {
                    _qbTableId = _client.GetQbTableId();
                    Topic.Status("\nThe Quickbase table ID is {}", _qbTableId);
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.GetQbTableId\nException occurred in GetQbTableId:\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        #endregion



        #region Server Parameter
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope("DotNetClientAddin.WatchServerParameter()"))
            {
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    serverParameters.StartWatching(_serverParameterNames);
                    var parameterList = serverParameters.GetServerParameters(_serverParameterNames);
                    if (parameterList.Count.Equals(this._serverParameterNames.Count()))
                    {
                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        this._ininHelpersServerUri = parameterList[1].Value;
                        DotNetClientAddin._salesforceProductionUsernameInintelephony = parameterList[2].Value;
                        DotNetClientAddin._salesforceSandboxUsernameInintelephony = parameterList[3].Value;
                        DotNetClientAddin._salesforceProductionPasswordInintelephony = parameterList[4].Value;
                        DotNetClientAddin._salesforceSandboxPasswordInintelephony = parameterList[5].Value;
                        //MiddlewareServiceEndpoint = "http://i3-pwssr2-px1:8091/MiddlewareService/soap"; //debug, force production server
                    }
                    else
                    {
                        //MessageBox.Show("ERROR: Could not retrieve required server parameters!  Needed `" + this._serverParameterNames.Count() + "`, retrieved `" + parameterList.Count() + "`.");
                        Topic.Error("Could not retrieve required server parameters!");
                    }

                    serverParameters.StopWatching();
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.WatchServerParameter\nException occurred in WatchServerParameter:\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        #endregion



        #region Admin Session
        private Session _adminSession;
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

        private void CreateAdminSession()
        {
            using (Topic.Scope("DotNetClientAddin.CreateAdminSession()"))
            {
                try
                {
                    _adminSession = new Session();
                    _adminSession.AutoReconnectEnabled = true;
                    _adminSession.Connect(new SessionSettings(),
                        new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                        new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.CreateAdminSession\nException occurred in CreateAdminSession:\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void DisconnectAdminSession()
        {
            using (Topic.Scope("DotNetClientAddin.DisconnectAdminSession()"))
            {
                try
                {
                    _adminSession.Disconnect();
                    Topic.Status("Proxy session has been disconnected");
                }
                catch (Exception ex)
                {
                    newEx = new Exception("In DotNetClientAddin.DisconnectAdminSession\nException occurred in DisconnectAdminSession:\n" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        #endregion
    }
}