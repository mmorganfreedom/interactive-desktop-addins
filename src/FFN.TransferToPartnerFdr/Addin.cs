﻿using System;
using ININ.IceLib.Connection;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient;
using ININ.InteractionClient.Interactions;

namespace FFN.TransferToPartnerFdr
{
    public class Addin : IAddIn
    {
        public Session Session { get; private set; }
        
        public void Load(IServiceProvider serviceProvider)
        {
            // Set the session to the client's current session
            Session = serviceProvider.GetService(typeof (Session)) as Session;

            // Create the client button and add it to the service
            var service = ServiceLocator.Current.GetInstance<IClientInteractionButtonService>();
            service.Add(new Button(this));
        }

        public void Unload()
        {

        }
    }
}
