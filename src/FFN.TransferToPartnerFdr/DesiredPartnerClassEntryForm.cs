﻿using System;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.TransferToPartnerFdr
{
    public partial class DesiredPartnerClassEntryForm : Form
    {
        public string DesiredPartnerClass { get; private set; }

        // Topic for client tracing
        private static readonly ITopicTracer Topic =
            TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.DesiredPartnerClassEntryForm");

        public DesiredPartnerClassEntryForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                DesiredPartnerClass = desiredPartnerClassTextBox.Text;
                Topic.Verbose("Desired Partner Class: {}", DesiredPartnerClass);

                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
    }
}
