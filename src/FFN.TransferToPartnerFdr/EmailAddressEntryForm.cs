﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using ININ.Diagnostics;

namespace FFN.TransferToPartnerFdr
{
    public partial class EmailAddressEntryForm : Form
    {
        public string EmailAddress { get; private set; }

        // Topic for client tracing
        private static readonly ITopicTracer Topic =
            TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.EmailAddressEntryForm");

        public EmailAddressEntryForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;
                EmailAddress = emailAddressTextBox.Text;
                Topic.Verbose("Email Address: {}", EmailAddress);

                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void emailAddressTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                submitButton.Enabled = (Regex.IsMatch(emailAddressTextBox.Text,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase));
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
    }
}
