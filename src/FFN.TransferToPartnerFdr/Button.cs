﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using FFN.TransferToPartnerFdr.AccountIdEntryForm;
using FFN.TransferToPartnerFdr.MiddlewareService;
using FFN.TransferToPartnerFp.AccountIdEntryForm;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.InteractionClient;
using ININ.IceLib.Configuration;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.Interactions;
using InteractionState = ININ.Client.Common.Interactions.InteractionState;
using Interaction = ININ.IceLib.Interactions.Interaction;

namespace FFN.TransferToPartnerFdr
{
    public class Button : IInteractionButton
    {
        #region IInteractionButton Properties

        public Icon Icon { get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); } }
        public string ToolTipText { get { return "Transfer to Partner - FDR"; } }
        public string Text { get { return "XFR to Partner FDR"; } }
        public string Id { get { return "Transfer to Partner - FDR"; } }
        public SupportedInteractionTypes SupportedInteractionTypes { get { return SupportedInteractionTypes.Call; } }

        #endregion

        private bool AutoMatched { get; set; }
        private string UserExtension { get; set; }

        private const string DefaultErrorMessage = "There was an error! Please refer to the logs for more details";
        private const string LeadIdAttributeName = "FF_FdrLeadId";

        private Addin Addin { get; set; }
        private InteractionsManager InteractionsManager { get; set; }
        private MiddlewareClient _client = null;
        private readonly SynchronizationContext _context = SynchronizationContext.Current;
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.Button");
        private static readonly ITopicTracerContextAttribute<string> InteractionIdAttribute =
            TopicTracerContextAttributeFactory.CreateStringContextAttribute("FFN.TransferToPartnerFdr.InteractionId",
                "Interaction ID", "{}");

        public Button(Addin addin)
        {
            Addin = addin;
            InteractionsManager = InteractionsManager.GetInstance(Addin.Session);

            // Get the logged in user's extension
            UserExtension = GetUserExtension(Addin.Session.UserId);

            // Creates a client to connect to the middleware
            CreateMiddlewareClient();
        }

        public bool CanExecute(IInteraction selectedInteraction)
        {
            // Only allow this button to work when an interaction has been selected and is connected
            return selectedInteraction != null && selectedInteraction.InteractionState.Equals(InteractionState.Connected);
        }

        public void Execute(IInteraction selectedInteraction)
        {
            try
            {
                var interaction = ConvertIInteractionToInteraction(selectedInteraction);
                var leadId = GetAttributeValue(interaction, LeadIdAttributeName);

                var qualifiedPartnerStats = new QbGetQualifiedPartnerStatsFromLeadResponse();
                if (string.IsNullOrEmpty(leadId))
                {
                    while (string.IsNullOrEmpty(leadId))
                    {
                        DisplayAccountIdEntryForm(interaction, LeadIdAttributeName);
                        leadId = GetAttributeValue(interaction, LeadIdAttributeName);
                        if (leadId.Equals("cancel", StringComparison.CurrentCultureIgnoreCase) || string.IsNullOrEmpty(leadId))
                        {
                            CancelTransfer(interaction.InteractionId.Id.ToString());
                            return;
                        }

                        qualifiedPartnerStats = _client.QbGetQualifiedPartnerStatsFromLead(leadId);
                        if (qualifiedPartnerStats.Records != null && qualifiedPartnerStats.Records.Any()) { break; }
                    }
                }
                else
                {
                    qualifiedPartnerStats = _client.QbGetQualifiedPartnerStatsFromLead(leadId);
                    if (qualifiedPartnerStats.Records != null && !qualifiedPartnerStats.Records.Any())
                    {
                        Topic.Note("Did not find any qualified partner stats");
                        leadId = string.Empty;
                        selectedInteraction.SetAttribute(LeadIdAttributeName, leadId);
                        Execute(selectedInteraction);
                        return;
                    }
                }

                var qualifiedPartnersRelatedToLead = _client.QbGetQualifiedPartnersRelatedToLead(leadId);
                Topic.Note("Found {} qualified partners related to the lead {}", qualifiedPartnersRelatedToLead.Records.Count(), leadId);

                if (qualifiedPartnersRelatedToLead.Records.Any())
                {
                    if (qualifiedPartnerStats.Records != null && Convert.ToInt32(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun) > 15)
                    {
                        Topic.Note("Qualified partners have expired");
                        _context.Send(p => MessageBox.Show("Qualified partners have expired"), null);
                        return;
                    }

                    if (qualifiedPartnerStats.Records != null && Convert.ToInt32(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun) >
                        Convert.ToInt32(qualifiedPartnerStats.Records[0].MinutesSinceLastModification) + 0.1)
                    {
                        Topic.Note("Lead modified since last QP run");
                        _context.Send(p => MessageBox.Show("Lead modified since last QP run"), null);
                        return;
                    }

                    var partnerClass = DisplayDesiredPartnerClassEntryForm();
                    var selectedPartner = qualifiedPartnersRelatedToLead.Records.FirstOrDefault(x => x.PartnerClass.Equals(partnerClass));
                    Topic.Note("The selected partner class is {}", partnerClass);
                    if (partnerClass.Equals("no interest", StringComparison.CurrentCultureIgnoreCase) || selectedPartner == null)
                    {
                        _client.QbCreateTransferRecord(leadId, "Open", Environment.UserName, leadId);
                        _client.QbEditLeadToAddTransferDisposition(leadId, leadId, DateTime.Now.ToString(), "Success", "");
                        return;
                    }

                    var partner = _client.QbGetPartnerById(selectedPartner.PartnerId);
                    if (!partner.Records.Any())
                    {
                        Topic.Note("Did not find any partners for the Id {}", leadId);
                        _context.Send(p => MessageBox.Show("Partner record not found"), null);
                        return;
                    }

                    if (partner.Records[0].PartnerName.Equals("Freedom Plus", StringComparison.CurrentCultureIgnoreCase))
                    {
                        var leadDataForFp = _client.QbGetLeadDataRequiredByFreedomPlus(leadId);
                        if (!leadDataForFp.Records.Any())
                        {
                            Topic.Note("Lead not found");
                            _context.Send(p => MessageBox.Show("Lead not found"), null);

                            Topic.Warning("Transfer Cancelled");
                            _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                            return;
                        }

                        var result = DialogResult.Cancel;
                        _context.Send(p =>
                        {
                            Topic.Note("Lead will be transferred to FreedomPlus");
                            result = MessageBox.Show("Lead will be transferred to FreedomPlus",
                                "Transferring to FreedomPlus", MessageBoxButtons.OKCancel);
                        }, null);

                        if (result.Equals(DialogResult.Cancel))
                        {
                            Topic.Warning("Transfer Cancelled");
                            _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                        }

                        var application = _client.SfGetApplicationSpecifiedInQbLead(leadId);
                        if (application.errorCode.Equals("0"))
                        {
                            Topic.Note("Found {} applications for the Lead Id {}", application.totalSize, leadId);
                            if (application.totalSize <= 0)
                            {
                                var createNewApp = _client.SfCreateNewApplication(leadDataForFp.Records[0].ClientLast,
                                    leadDataForFp.Records[0].ClientFirst, leadDataForFp.Records[0].DayPhone,
                                    leadDataForFp.Records[0].EvePhone, leadDataForFp.Records[0].CellPhone, "Cell",
                                    leadDataForFp.Records[0].OwnOrRent, leadDataForFp.Records[0].EverFiledBk,
                                    leadDataForFp.Records[0].ClientDobText, leadDataForFp.Records[0].EmailAddress,
                                    leadDataForFp.Records[0].HardshipDetailNotes,
                                    leadDataForFp.Records[0].PastDueAccounts, leadDataForFp.Records[0].UnsecuredDebtEstimate,
                                    leadDataForFp.Records[0].IfBkWhatYear, leadDataForFp.Records[0].BestTimeToContact,
                                    leadId, leadDataForFp.Records[0].Address, leadDataForFp.Records[0].City,
                                    leadDataForFp.Records[0].State, leadDataForFp.Records[0].Zip, "FDR", "Internal Transfer",
                                    Environment.UserName, UserExtension, leadDataForFp.Records[0].LeadPhase,
                                    leadDataForFp.Records[0].Campaign, leadDataForFp.Records[0].LeadKeywork,
                                    leadDataForFp.Records[0].LeadType, leadDataForFp.Records[0].LeadPartner, "Freedom Plus");

                                if (createNewApp.success)
                                {
                                    _client.QbEditLeadToAddSalesforceApplicationId(leadId, createNewApp.id);
                                }
                                else
                                {
                                    Topic.Warning("Unable to connect to SF, transer will continue...");
                                    _context.Send(
                                        p => MessageBox.Show("Unable to connect to SF, transer will continue..."), null);
                                }
                            }
                        }
                        else
                        {
                            Topic.Warning("Unable to connect to SF, transfer will continue...");
                            _context.Send(p => MessageBox.Show("Unable to connect to SF, transfer will continue..."),
                                null);
                        }
                    }
                    else
                    {
                        _context.Send(
                            p =>
                                MessageBox.Show("Lead will be transferred to " + partner.Records[0].PartnerName,
                                    "Lead transferred to " + partner.Records[0].PartnerName, MessageBoxButtons.OK), null);
                    }

                    if (partner.Records[0].CurrentlyOpen.Equals(0))
                    {
                        DisplayHoursOfOperationForm(partner.Records[0]);
                        _client.QbCreateTransferRecord(leadId, "Closed", Environment.UserName, leadId);
                        _client.QbEditLeadToAddTransferDisposition(leadId, leadId, DateTime.Now.ToString(), "Failure", "Unknown reason");
                        return;
                    }

                    WarmTransfer(interaction, partner.Records[0].DirectNumber);
                    var transferOutcome = PromptUserForTransferOutcome();
                    _client.QbCreateTransferRecord(leadId, "Open", Environment.UserName, leadId);
                    _client.QbEditLeadToAddTransferDisposition(leadId, leadId, DateTime.Now.ToString(),
                        transferOutcome[0], transferOutcome[1]);
                    return;

                }
                else
                {
                    if (qualifiedPartnerStats.Records != null && qualifiedPartnerStats.Records.Any() && string.IsNullOrEmpty(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun))
                    {
                        Topic.Note("Qualified partners never run");
                        _context.Send(p => MessageBox.Show("Qualified Partners never run"), null);
                        return;
                    }

                    if (qualifiedPartnerStats.Records != null && qualifiedPartnerStats.Records.Any() && Convert.ToInt32(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun) > 15)
                    {
                        Topic.Note("Qualify Partners not found or expired, run again");
                        _context.Send(p => MessageBox.Show("Qualify Partners not found or expired, run again"), null);
                        return;
                    }

                    _client.QbCreateTransferRecord(leadId, "", Environment.UserName, "");
                    _client.QbEditLeadToAddTransferDisposition(leadId, "", DateTime.Now.ToString(), "Failure", "Lead does not qualify for any of the classifications");
                    return;
                }

            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        #region TRANSFER METHODS

        private void WarmTransfer(Interaction interaction, string transferDestination)
        {
            try
            {
                var userSelection = DialogResult.Cancel;
                _context.Send(
                    p =>
                    {
                        userSelection = MessageBox.Show(
                            "This call will be transferred to " + transferDestination + ". Click OK to begin the transfer. Click Cancel to return to the propsect.",
                            "Call Transfer", MessageBoxButtons.OKCancel);

                    }, null);

                if (userSelection.Equals(DialogResult.OK))
                {
                    Topic.Verbose("Agent has agreed to continue transfer of interaction {}",
                        interaction.InteractionId.Id);

                    var consultInteraction =
                        InteractionsManager.MakeConsultTransfer(
                            new ConsultTransferParameters(transferDestination,
                                interaction.InteractionId));

                    consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);

                    _context.Send(
                        p =>
                        {
                            userSelection = MessageBox.Show(
                                "Wait for an agent to answer and then announce your transfer. Please provide the parter agent with any requested information. Once the agent is ready, click OK to complete the transfer. Click Cancel to cancel the transfer and return to the propsect",
                                "Transferring to Partner Agent", MessageBoxButtons.OKCancel);
                        }, null);

                    if (userSelection.Equals(DialogResult.OK))
                    {
                        consultInteraction.Conclude();
                        _context.Send(p => MessageBox.Show("Transfer completed"), null);
                    }
                    else
                    {
                        interaction.Hold(false);
                        consultInteraction.Cancel();
                        _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                    }
                }
                else
                {
                    interaction.Hold(false);
                    Topic.Warning("Agent has cancelled transfer of interaction {}", interaction.InteractionId.Id);
                    _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                }


            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void ConferenceTransfer(Interaction interaction, string transferDestination, string transferDestinationName)
        {
            try
            {
                Topic.Verbose("Interaction {} is now on hold", interaction.InteractionId.Id);

                var consultInteraction = InteractionsManager.MakeConsultTransfer(
                    new ConsultTransferParameters(transferDestination, interaction.InteractionId));

                consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);

                var userSelection = DialogResult.Cancel;
                _context.Send(
                    p =>
                    {
                        userSelection = MessageBox.Show(
                            "Provide client info to " + transferDestinationName + " agent. When " +
                            transferDestinationName +
                            " agent is ready, click OK to connect the client. Click Cancel to cancel the transfer.",
                            "Conference Transfer", MessageBoxButtons.OKCancel);

                    }, null);

                if (userSelection.Equals(DialogResult.OK))
                {
                    consultInteraction.ChangeSpeakers(ConsultTransferParticipants.All);

                    userSelection = DialogResult.Cancel;
                    _context.Send(
                        p =>
                        {
                            userSelection = MessageBox.Show(
                                "Introduce client and " + transferDestinationName + " agent. When ready, click OK to complete the handoff. To cancel, click Cancel and ask the " +
                                transferDestinationName +
                                " agent to hang up.",
                                transferDestination,
                                MessageBoxButtons.OKCancel);

                        }, null);

                    if (userSelection.Equals(DialogResult.OK))
                    {
                        consultInteraction.Conclude();
                        _context.Send(p => MessageBox.Show(transferDestinationName + " handoff complete."), null);
                    }
                    else
                    {
                        interaction.Hold(false);
                        consultInteraction.Cancel();
                        _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                    }
                }
                else
                {
                    interaction.Hold(false);
                    consultInteraction.Cancel();
                    Topic.Verbose("Interaction {} is now off hold", interaction.InteractionId.Id);

                    _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }


        #endregion

        #region HELPER METHODS

        private Interaction ConvertIInteractionToInteraction(IInteraction interaction)
        {
            Interaction ret = null;

            try
            {
                ret = InteractionsManager.CreateInteraction(new InteractionId(interaction.InteractionId));
                Topic.Verbose("Interaction object created with ID {}", ret.InteractionId.Id);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private void CreateMiddlewareClient()
        {
            try
            {
                // Create a new Middleware Client
                var clientBinding = new BasicHttpBinding
                {
                    Name = "BasicHttpBinding_IMiddleware"
                };

                var clientEndpoint = new EndpointAddress("http://i3-pwssr2-px1:8091/MiddlewareService/soap");
                _client = new MiddlewareClient(clientBinding, clientEndpoint);

            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private QbGetQualifiedPartnerStatsFromLeadResponse QbGetQualifiedPartnerStatsFromLead(string leadId)
        {
            QbGetQualifiedPartnerStatsFromLeadResponse ret = null;
            try
            {
                ret = _client.QbGetQualifiedPartnerStatsFromLead(leadId);
                Topic.Verbose("Completed QbGetQualifiedPartnerStatsFromLead with error code {}", ret.Errcode);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }


        private void CancelTransfer(string callId)
        {
            try
            {
                _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                Topic.Status("The transfer has been cancelled for interaction {}", callId);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        #endregion

        #region Display Form Methods

        private string[] PromptUserForTransferOutcome()
        {
            var ret = new[] { "", "" };

            try
            {
                var form = new TransferOutcomeEntryForm();

                _context.Send(p =>
                {
                    using (form)
                    {
                        var result = form.ShowDialog();
                        if (result.Equals(DialogResult.OK))
                        {
                            ret[0] = form.TransferOutcome;
                            ret[1] = form.FailureReason;
                            Topic.Note("The selected transfer outcome is {}", ret);
                        }
                    }
                }, null);
            }
            catch (Exception ex)
            {
                _context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
                Topic.Exception(ex);
            }

            return ret;
        }

        private string PromptUserForEmailAddress()
        {
            var ret = "";

            try
            {
                var form = new EmailAddressEntryForm();
                _context.Send(p =>
                {
                    var result = form.ShowDialog();
                    if (result.Equals(DialogResult.OK))
                    {
                        ret = form.EmailAddress;
                        Topic.Note("The email address entered is {}", ret);
                    }
                }, null);
            }
            catch (Exception ex)
            {
                _context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
                Topic.Exception(ex);
            }

            return ret;
        }

        private void DisplayHoursOfOperationForm(QbGetPartnerByIdResponse.Record record)
        {
            try
            {
                var dictionary = CreateHoursOfOperationDictionary(record);
                var form = new HoursOfOperationForm(dictionary);
                _context.Send(p => form.ShowDialog(), null);
            }
            catch (Exception ex)
            {
                _context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
                Topic.Exception(ex);
            }
        }

        private string DisplayDesiredPartnerClassEntryForm()
        {
            var ret = "";

            try
            {
                var form = new DesiredPartnerClassEntryForm();

                _context.Send(p =>
                {
                    var result = form.ShowDialog();
                    if (result.Equals(DialogResult.OK))
                    {
                        ret = form.DesiredPartnerClass;
                        Topic.Note("The desired partner class is {}", ret);
                    }
                }, null);
            }
            catch (Exception ex)
            {
                _context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
                Topic.Exception(ex);
            }

            return ret;
        }

        #endregion

        private Dictionary<string, string[]> CreateHoursOfOperationDictionary(QbGetPartnerByIdResponse.Record partnerRecord)
        {
            var ret = new Dictionary<string, string[]>();

            try
            {
                ret.Add("Monday", new[] { partnerRecord.MondayOpenTime, partnerRecord.MondayCloseTime });
                ret.Add("Tuesday", new[] { partnerRecord.TuesdayOpenTime, partnerRecord.TuesdayCloseTime });
                ret.Add("Wednesday", new[] { partnerRecord.WednesdayOpenTime, partnerRecord.WednesdayCloseTime });
                ret.Add("Thursday", new[] { partnerRecord.ThursdayOpenTime, partnerRecord.ThursdayCloseTime });
                ret.Add("Friday", new[] { partnerRecord.FridayOpenTime, partnerRecord.FridayClosetime });
                ret.Add("Saturday", new[] { partnerRecord.SaturdayOpenTime, partnerRecord.SaturdayCloseTime });
                ret.Add("Sunday", new[] { partnerRecord.SundayOpenTime, partnerRecord.SundayCloseTime });
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private string GetUserExtension(string userId)
        {
            var ret = "";
            try
            {
                var list = new UserConfigurationList(ConfigurationManager.GetInstance(Addin.Session));
                list.StartCaching();
                var userConfig = list.GetConfigurationList().FirstOrDefault(x => x.ConfigurationId.Id.Equals(userId));
                if (userConfig != null)
                {
                    ret = userConfig.Extension.Value;
                }
                list.StopCaching();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private void DisplayAccountIdEntryForm(Interaction interaction, string attributeName)
        {
            try
            {
                var form = new ApplicationIdEntryForm(AccountIdEntryFormType.FreedomDebtRelief)
                {
                    Interaction = interaction,
                    AttributeName = attributeName
                };

                form.LoadFdrForm();
                _context.Send(p => form.ShowDialog(), null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private string GetAttributeValue(Interaction interaction, string attributeName)
        {
            var ret = "";

            try
            {
                ret = interaction.GetStringAttribute(attributeName);
                Topic.Note("The attribute {} has the value {}", attributeName, ret);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        #region Quickbase Methods

        private QbGetQualifiedPartnersRelatedToLeadResponse GetQualifiedPartnersRelatedToLead(string leadIdValue)
        {
            QbGetQualifiedPartnersRelatedToLeadResponse ret = null;

            try
            {
                ret = _client.QbGetQualifiedPartnersRelatedToLead(leadIdValue);
                Topic.Verbose("Error code on running QbGetQualifiedPartnersRelatedToLead: {}", ret.Errcode);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        #endregion

        #region SalesForce Methods

        private SfQueryResponse GetApplicationByQbLeadEmailAddress(string emailAddress)
        {
            SfQueryResponse ret = null;

            try
            {
                ret = _client.SfGetApplicationByQbLeadEmailAddress(emailAddress);
                Topic.Verbose("Error code on running SfGetApplicationRelatedToQbLead: {}", ret.errorCode);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private SfQueryResponse GetApplicationRelatedToQbLead(string leadId)
        {
            SfQueryResponse ret = null;

            try
            {
                ret = _client.SfGetApplicationRelatedToQbLead(leadId);
                Topic.Verbose("Error code on running SfGetApplicationRelatedToQbLead: {}", ret.errorCode);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private SfQueryResponse GetApplicationSpecifiedInQbLead(string recordId)
        {
            SfQueryResponse ret = null;

            try
            {
                ret = _client.SfGetApplicationSpecifiedInQbLead(recordId);
                Topic.Verbose("Error code on running SfGetApplicationSpecifiedInQbLead: {}", ret.errorCode);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        #endregion
    }
}
