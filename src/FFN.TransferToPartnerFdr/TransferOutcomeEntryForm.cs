﻿using System;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.TransferToPartnerFdr
{
    public partial class TransferOutcomeEntryForm : Form
    {
        // Public properties
        public string TransferOutcome { get; private set; }
        public string FailureReason { get; private set; }

        // Topic for client tracing
        private static readonly ITopicTracer Topic =
            TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.TransferOutcomeEntryForm");

        public TransferOutcomeEntryForm()
        {
            InitializeComponent();
        }

        private void transferOutcomeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (transferOutcomeComboBox.GetItemText(transferOutcomeComboBox.SelectedItem).Equals("Success"))
                {
                    failureReasonComboBox.Enabled = false;
                    failureReasonGroupBox.Enabled = false;
                    failureReasonComboBox.SelectedIndex = -1;
                }
                else
                {
                    failureReasonComboBox.Enabled = true;
                    failureReasonGroupBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                
                TransferOutcome = transferOutcomeComboBox.GetItemText(transferOutcomeComboBox.SelectedItem);
                FailureReason = transferOutcomeComboBox.GetItemText(transferOutcomeComboBox.SelectedItem).Equals("Failure")
                    ? failureReasonComboBox.GetItemText(failureReasonComboBox.SelectedItem)
                    : "";

                Topic.Note("Transfer outcome: {} | Failure reason: {}", TransferOutcome, FailureReason);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
    }
}
