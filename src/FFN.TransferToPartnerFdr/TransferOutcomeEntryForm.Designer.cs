﻿namespace FFN.TransferToPartnerFdr
{
    partial class TransferOutcomeEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferOutcomeEntryForm));
            this.transferOutcomeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.failureReasonGroupBox = new System.Windows.Forms.GroupBox();
            this.failureReasonComboBox = new System.Windows.Forms.ComboBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.failureReasonGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // transferOutcomeComboBox
            // 
            this.transferOutcomeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transferOutcomeComboBox.FormattingEnabled = true;
            this.transferOutcomeComboBox.Items.AddRange(new object[] {
            "Success",
            "Failure"});
            this.transferOutcomeComboBox.Location = new System.Drawing.Point(12, 29);
            this.transferOutcomeComboBox.Name = "transferOutcomeComboBox";
            this.transferOutcomeComboBox.Size = new System.Drawing.Size(260, 21);
            this.transferOutcomeComboBox.TabIndex = 0;
            this.transferOutcomeComboBox.SelectedIndexChanged += new System.EventHandler(this.transferOutcomeComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please select a transfer outcome:";
            // 
            // failureReasonGroupBox
            // 
            this.failureReasonGroupBox.Controls.Add(this.failureReasonComboBox);
            this.failureReasonGroupBox.Location = new System.Drawing.Point(13, 57);
            this.failureReasonGroupBox.Name = "failureReasonGroupBox";
            this.failureReasonGroupBox.Size = new System.Drawing.Size(259, 48);
            this.failureReasonGroupBox.TabIndex = 2;
            this.failureReasonGroupBox.TabStop = false;
            this.failureReasonGroupBox.Text = "Failure Reason";
            // 
            // failureReasonComboBox
            // 
            this.failureReasonComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.failureReasonComboBox.FormattingEnabled = true;
            this.failureReasonComboBox.Items.AddRange(new object[] {
            "Long Hold Time",
            "Rejected",
            "Unknown Issue"});
            this.failureReasonComboBox.Location = new System.Drawing.Point(6, 19);
            this.failureReasonComboBox.Name = "failureReasonComboBox";
            this.failureReasonComboBox.Size = new System.Drawing.Size(247, 21);
            this.failureReasonComboBox.TabIndex = 0;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(197, 111);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 3;
            this.submitButton.Text = "OK";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // TransferOutcomeEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 142);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.failureReasonGroupBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.transferOutcomeComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TransferOutcomeEntryForm";
            this.Text = "Transfer Outcome";
            this.TopMost = true;
            this.failureReasonGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox transferOutcomeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox failureReasonGroupBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.ComboBox failureReasonComboBox;
    }
}