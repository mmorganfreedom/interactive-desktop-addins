﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
using ININ.InteractionClient.AddIn;

[assembly: AssemblyTitle("FFN.ClickToDial.Addin")]
[assembly: AssemblyDescription(".NET Client Add-In that allows a web service to dial for an agent.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Interactive Intelligence, Inc.")]
[assembly: AssemblyProduct("FFN.ClickToDial.Addin")]
[assembly: AssemblyCopyright("Copyright © 2015 Interactive Intelligence, Inc.")]
[assembly: AssemblyTrademark("Interactive Intelligence®")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ea40de09-2096-4a65-91e8-9e36e7d38b40")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.11.*")]

[assembly: AddInVersion(AddInVersion.CurrentVersion)]
