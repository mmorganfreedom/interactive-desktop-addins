﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;

namespace FFN.ClickToDial.Addin
{
    public class Addin : IAddIn
    {
        private Session _session;
        private InteractionsManager _interactionsManager;

        private HttpListener _httpListener = new HttpListener();
        private Thread _responseThread;

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.ClickToDial.Addin");

        public void Load(IServiceProvider serviceProvider)
        {
            using (Topic.Scope("FFN.ClickToDial.Addin.Load(..)"))
            {
                try
                {
                    // Create the session and interactions manager
                    _session = serviceProvider.GetService(typeof(Session)) as Session;
                    _interactionsManager = InteractionsManager.GetInstance(_session);
                    _httpListener.Prefixes.Add("http://localhost:8001/");
                    _httpListener.Start();
                    Topic.Note("HttpListener has started on port 8001");

                    _responseThread = new Thread(ProcessRequest);

                    _responseThread.Start();
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.ClickToDial.Addin.Load:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                    throw;
                }
            }
        }

        private void ProcessRequest()
        {
            using (Topic.Scope("FFN.ClickToDial.Addin.ProcessRequest(..)"))
            {
                try
                {
                    while (_httpListener != null && _httpListener.IsListening)
                    {
                        var context = _httpListener.GetContext();

                        var query = context.Request.Url.Query;

                        var msg = MakeCallFromQueryParameters(query);

                        byte[] b = Encoding.UTF8.GetBytes("<ClickToDial><response>" + msg + 
                            "</response></ClickToDial>");
                        string bstring = b[0].ToString();
                        for (int i = 1; i < b.Length; i++)
                        {
                            bstring += ", '" + b[i].ToString() + "'";
                        }
                        context.Response.StatusCode = 200;
                        context.Response.KeepAlive = false;
                        context.Response.ContentLength64 = b.Length;

                        var output = context.Response.OutputStream;
                        
                        output.Write(b, 0, b.Length);
                        context.Response.Close();
                    }
                }
                catch (SocketException ex)
                {
                    Exception newEx =
                        new Exception("In FFN.ClickToDial.Addin.ProcessRequest:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private string MakeCallFromQueryParameters(string query)
        {
            using (Topic.Scope("FFN.ClickToDial.Addin.MakeCallFromQueryParameters(..)"))
            {
                var ret = "Unknown Error";
                try
                {
                    var phoneNumber = HttpUtility.ParseQueryString(query).Get("phoneNumber");
                    if (string.IsNullOrEmpty(phoneNumber))
                    {
                        return "No phone number was supplied in the request!";
                    }

                    var accountId = HttpUtility.ParseQueryString(query).Get("accountId");
                    if (string.IsNullOrEmpty(accountId))
                    {
                        return "No account ID was supplied in the request!";
                    }

                    var workgroup = HttpUtility.ParseQueryString(query).Get("workgroupName");
                    ret = MakeCall(phoneNumber, accountId, workgroup);
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In FFN.ClickToDial.Addin.MakeCallFromQueryParameters:\nException Occured\nException: " +
                            ex.Message);
                    ret = ex.Message;
                }
                return ret;
            }
        }

        private string ParseXml(string body)
        {
            var ret = "Unknown Error";
            using (Topic.Scope("FFN.ClickToDial.Addin.ParseXml(..)"))
            {
                try
                {
                    // Create a new XML Document
                    var xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(body);
                    var clickToDialNode = xmlDoc.SelectSingleNode("/ClickToDial");

                    if (clickToDialNode != null)
                    {
                        string phoneNumber = "", accountId = "", workgroupName = "";
                        foreach (XmlNode node in clickToDialNode.ChildNodes)
                        {
                            switch (node.Name)
                            {
                                case "phoneNumber":
                                    Topic.Note(node.InnerText);
                                    phoneNumber = node.InnerText;
                                    break;
                                
                                case "accountId":
                                    Topic.Note(node.InnerText);
                                    accountId = node.InnerText;
                                    break;
                                case "workgroupName":
                                    Topic.Note(node.InnerText);
                                    workgroupName = node.InnerText;
                                    break;
                            }
                        }

                        if (!string.IsNullOrEmpty(phoneNumber) && !string.IsNullOrEmpty(accountId))
                        {
                            ret = MakeCall(phoneNumber, accountId, workgroupName);
                        }
                    }
                    else
                    {
                        Topic.Note("In FFN.ClickToDial.Addin.ParseXml:\nVariable Assigned:\n\tclickToDialNode: Null");
                    }
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In FFN.ClickToDial.Addin.ParseXml:\nException Occured\nException: " +
                            ex.Message);
                    ret = ex.Message;

                }
            }

            return ret;
        }

        public string MakeCall(string phoneNumber, string accountId, string workgroupName)
        {
            using (Topic.Scope("FFN.ClickToDial.Addin.MakeCall(..)"))
            {
                var ret = "Unknown Error";
                try
                {
                    Interaction interaction;
                    if (!string.IsNullOrEmpty(workgroupName))
                    {
                        interaction = _interactionsManager.MakeCall(new CallInteractionParameters(phoneNumber,
                            workgroupName,
                            CallMadeStage.Allocated));
                    }
                    else
                    {
                        interaction =
                            _interactionsManager.MakeCall(
                                new CallInteractionParameters(phoneNumber, CallMadeStage.Allocated));
                    }

                    if (Regex.IsMatch(accountId, @"^\d+$"))
                    {
                        Topic.Note("Setting FF_FdrLeadId to {}", accountId);
                        interaction.SetStringAttribute("FF_FdrLeadId", accountId);
                    }
                    else
                    {
                        Topic.Note("Setting FF_FpApplicationId to {}", accountId);
                        interaction.SetStringAttribute("FF_FpApplicationId", accountId);
                    }

                    ret = "Success";
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In FFN.ClickToDial.Addin.ParseXml:\nException Occured\nException: " +
                            ex.Message);
                    ret = ex.Message;
                }

                return ret;
            }
        }

        public void Unload()
        {
            using (Topic.Scope("FFN.ClickToDial.Addin.Unload(..)"))
            {
                try
                {
                    _responseThread.Abort();
                    _httpListener.Stop();
                    _httpListener = null;
                    _session = null;
                    _interactionsManager = null;
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In FFN.ClickToDial.Addin.Unload:\nException Occured\nException: " +
                            ex.Message);
                    //throw;
                }
            }
        }
    }
}
