﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient;
using ININ.InteractionClient.Interactions;

namespace FFN.AutoDialerScreenPop
{
    public class Addin : QueueMonitor
    {
        public Session Session { get; private set; }
        public string ScreenPopUrl { get { return "https://na5.salesforce.com/"; } }

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.AutoDialerScreenPop.AddIn");

        private static readonly ITopicTracerContextAttribute<string> InteractionIdAttribute =
            TopicTracerContextAttributeFactory.CreateStringContextAttribute("FFN.AutoDialerScreenPop.InteractionId", "Interaction ID", "{}");

        public List<IInteraction> InteractionsAlreadyProcessed = new List<IInteraction>();
        public object InteractionsLock = new object();

        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            try
            {
                Session = serviceProvider.GetService(typeof(Session)) as Session;
                var service = ServiceLocator.Current.GetInstance<IClientInteractionButtonService>();
                service.Add(new Button(this));
                Topic.Status("Successfully able to load the session and create a Client Button");
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }

            base.OnLoad(serviceProvider);
        }

        protected override void OnUnload()
        {
            Session = null;
            base.OnUnload();
        }

        protected override IEnumerable<string> Attributes
        {
            get
            {
                return new[]
                {
                    "FF_CrmId",
                    "Eic_CallPurpose",
                    "Is_Attr_CampaignID",
                    InteractionAttributeName.InteractionId,
                    InteractionAttributeName.Direction
                };
            }
        }

        protected override void InteractionAdded(IInteraction interaction)
        {
            using (InteractionIdAttribute.Create(interaction.InteractionId))
            {
                try
                {
                    ProcessInteraction(interaction);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }

                base.InteractionAdded(interaction);
            }

        }

        protected override void InteractionChanged(IInteraction interaction)
        {
            using (InteractionIdAttribute.Create(interaction.InteractionId))
            {
                try
                {
                    ProcessInteraction(interaction);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }

                base.InteractionChanged(interaction);
            }

        }

        protected override void InteractionRemoved(IInteraction interaction)
        {
            using (InteractionIdAttribute.Create(interaction.InteractionId))
            {
                try
                {
                    if (InteractionsAlreadyProcessed.Contains(interaction))
                    {
                        Topic.Status("Removing the interaction {} from the processed list", interaction.InteractionId);
                        lock (InteractionsLock)
                        {
                            InteractionsAlreadyProcessed.Remove(interaction);
                        }
                    }
                    else
                    {
                        Topic.Status("Cannot remove interaction {}. It was never processed!", interaction.InteractionId);
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }

                base.InteractionRemoved(interaction);
            }

        }

        private void ProcessInteraction(IInteraction interaction)
        {
            try
            {
                var callDirection = interaction.GetAttribute(InteractionAttributeName.Direction);
                var campaignId = interaction.GetAttribute("Is_Attr_CampaignID");

                Topic.Verbose("Is_Attr_CampaignID value: {}", campaignId);

                if (!InteractionsAlreadyProcessed.Contains(interaction))
                {
                    var crmIdValue = interaction.GetAttribute("FF_CrmId");
                    if (!string.IsNullOrEmpty(crmIdValue) && callDirection.Equals(InteractionAttributeValue.Direction.Outgoing) && !string.IsNullOrEmpty(campaignId))
                    {
                        Process.Start(ScreenPopUrl + crmIdValue);
                        Topic.Status("Screen popped the URL {}", ScreenPopUrl + crmIdValue);

                        lock (InteractionsLock)
                        {
                            InteractionsAlreadyProcessed.Add(interaction);
                        }
                    }
                    else
                    {
                        Topic.Status("The interaction {} did not have the field FF_CrmId set", interaction.InteractionId);
                    }
                }
                else
                {
                    Topic.Status("The interaction {} has already been processed!", interaction.InteractionId);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

        }
    }
}
