﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.IceLib.Connection.Extensions;
using ININ.InteractionClient.Interactions;
using IInteraction = ININ.Client.Common.Interactions.IInteraction;

namespace FFN.AutoDialerScreenPop
{
    class Button : IInteractionButton
    {
        #region IInteractionButton Properties

        public Icon Icon { get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); } }
        public string Id { get { return "AutoDialerScreenPop"; } }
        public SupportedInteractionTypes SupportedInteractionTypes { get { return SupportedInteractionTypes.All; } }
        public string Text { get { return "AutoDialer Screen Pop"; } }
        public string ToolTipText { get { return "Pops a Salesforce URL to the screen if the call is made by the outbound dialer"; } }

        #endregion

        private static readonly ITopicTracerContextAttribute<string> InteractionIdAttribute =
            TopicTracerContextAttributeFactory.CreateStringContextAttribute("FFN.AutoDialerScreenPopButton.InteractionId", "Interaction ID", "{}");
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.AutoDialerScreenPop.Button");

        private readonly SynchronizationContext _context;
        private readonly Addin _addin;

        public Button(Addin addin)
        {
            try
            {
                _addin = addin;
                _context = SynchronizationContext.Current;
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }

        }

        public bool CanExecute(IInteraction selectedInteraction)
        {
            try
            {
                return selectedInteraction != null && selectedInteraction.InteractionState.Equals(InteractionState.Connected);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                return false;
            }
        }

        public void Execute(IInteraction selectedInteraction)
        {
            using (InteractionIdAttribute.Create(selectedInteraction.InteractionId))
            {
                try
                {
                    var crmIdValue = selectedInteraction.GetAttribute("FF_CrmId");
                    if (!string.IsNullOrEmpty(crmIdValue))
                    {
                        Process.Start(_addin.ScreenPopUrl + crmIdValue);
                        Topic.Status("Screen popped the URL {}", _addin.ScreenPopUrl + crmIdValue);
                    }
                    else
                    {
                        _context.Send(p => MessageBox.Show("No dialer record ID detected"), null);
                        Topic.Warning("Could not find a dialer record ID for this interaction");
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
    }
}
