﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using FFN.TransferToPartnerFp.Addin.Forms;
using FFN.TransferToPartnerFp.Addin.MiddlewareService;
using ININ.Diagnostics;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;
using IInteraction = ININ.Client.Common.Interactions.IInteraction;
using Interaction = ININ.IceLib.Interactions.Interaction;
using InteractionAttributeName = ININ.IceLib.Interactions.InteractionAttributeName;
using InteractionState = ININ.Client.Common.Interactions.InteractionState;

namespace FFN.TransferToPartnerFp.Addin
{

	class Button : IInteractionButton
	{
		private readonly string transferDestinationDefault = "6028005043";

		#region IInteractionButton Properties

		public Icon Icon {
			get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); }
		}

		public string Id {
			get { return "Transfer To Partner - Freedom Plus"; }
		}

		public SupportedInteractionTypes SupportedInteractionTypes {
			get { return SupportedInteractionTypes.Call; }
		}

		public string Text {
			get { return "XFR to Partner FP"; }
		}

		public string ToolTipText {
			get { return "Click here to start Transfer to Partner - Freedom Plus"; }
		}

		#endregion

		public static readonly ITopicTracer Topic =
				TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFreedomPlus.Button");

		private const string ApplicationIdAttributeName = "FF_FpApplicationId";
		private const string LeadIdAttributeName = "FF_FdrLeadId";
		private const string DefaultErrorMessage = "There was an error! Please refer to the logs for more details.";

		private readonly Session _session;
		private Session _adminSession;
		private InteractionsManager InteractionsManager { get; set; }

		private MiddlewareClient _client;
		private readonly Addin _addin;
		private readonly SynchronizationContext _context;
		//private readonly DateTime _startTime;

		private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzzzz";
		private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";
		private string _extension = "";
		private const string CccsPartnerId = "14", CreditRepairId = "10";

		public Button(Addin addin) {
			_addin = addin;
			_context = SynchronizationContext.Current;
			InteractionsManager = InteractionsManager.GetInstance(addin.Session);
			_session = addin.Session;
			CreateMiddlewareClient();

			CreateAdminSession();
			GetLoggedInUsersExtension();
			DisconnectAdminSession();
		}

		public bool CanExecute(IInteraction selectedInteraction) {
			// Only allow this button to work when an interaction has been selected and is connected
			return selectedInteraction != null && selectedInteraction.InteractionState.Equals(InteractionState.Connected);
		}

		public void Execute(IInteraction selectedInteraction) {
            using (Topic.Scope())
            {
                try
                {
                    SfQueryResponse sfQueryResponse;
                    var interaction = ConvertIInteractionToInteration(selectedInteraction);
                    var applicationId = GetInteractionAttributeValue(interaction, ApplicationIdAttributeName);
                    var dnis = selectedInteraction.GetAttribute("Eic_LocalId");
                    var ani = selectedInteraction.GetAttribute("Eic_RemoteId");

                    Topic.Note("The application Id set on interaction {} is {}", interaction.InteractionId.Id, applicationId);
                    if (applicationId.Length < 15)
                    {
                        do
                        {
                            var result = DisplayApplicationIdEntryForm(dnis, ani);
                            applicationId = result;
                            if (result.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
                            {
                                DisplayMessageBox("Transfer Cancelled", "Transfer Cancelled", MessageBoxButtons.OK);
                                return;
                            }
                            Topic.Note("The user entered {} as the application Id", applicationId);

                            if (applicationId.Length.Equals(8))
                            {
                                Topic.Note("Getting application by the name {}", applicationId);
                                sfQueryResponse = _client.SfGetApplicationByName(applicationId);
                                break;
                            }

                            if (!applicationId.Length.Equals(15) && !applicationId.Length.Equals(18)) { continue; }

                            Topic.Note("Getting application by the Id {}", applicationId);
                            sfQueryResponse = _client.SfGetApplicationById(applicationId);
                            break;
                        } while (true);

                        SetInteractionAttribute(interaction, ApplicationIdAttributeName, applicationId);
                    }
                    else
                    {
                        sfQueryResponse = _client.SfGetApplicationById(applicationId);
                    }

                    Topic.Note("Found {} applications with the Id {}", sfQueryResponse.totalSize, applicationId);
                    if (sfQueryResponse.totalSize.Equals(1))
                    {
                        applicationId = sfQueryResponse.records.First().Id;
                        SetInteractionAttribute(interaction, ApplicationIdAttributeName, applicationId);

                        var utmSource = string.IsNullOrEmpty(sfQueryResponse.records.First().utm_source__c)
                                ? ""
                                : sfQueryResponse.records.First().utm_source__c;

                        var loanOfficer = string.IsNullOrEmpty(sfQueryResponse.records.First().Loan_Officer_Source__c)
                                ? ""
                                : sfQueryResponse.records.First().Loan_Officer_Source__c;

                        var wholesaler = GetMatchingWholesalerRecord(utmSource, loanOfficer);

                        if (wholesaler != null)
                        {
                            Topic.Note("Transferring to {}", "Debt Relief");
                            DebtReliefLogic(interaction, sfQueryResponse, applicationId);
                            return;
                        }
                        var transferType = GetTransferType();
                        Topic.Note("The user has selected the transfer type {}", transferType);

                        if (string.IsNullOrEmpty(transferType))
                        {
                            DisplayMessageBox("Transfer Cancelled", "Transfer Cancelled", MessageBoxButtons.OK);
                            return;
                        }

                        if (transferType.Equals("Debt Relief", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Topic.Note("Transferring to {}", transferType);
                            DebtReliefLogic(interaction, sfQueryResponse, applicationId);
                            return;
                        }

                        Topic.Note("Transferring to {}", transferType);
                        var partnerId = transferType.Equals("CCCS", StringComparison.CurrentCultureIgnoreCase)
                                ? CccsPartnerId
                                : CreditRepairId;

                        OtherTypeLogic(interaction, sfQueryResponse, partnerId, applicationId);
                        return;
                    }

                    DisplayMessageBox("Unable to find application " + applicationId + ". Please try again.",
                            "Cannot find application", MessageBoxButtons.OK);

                    SetInteractionAttribute(interaction, ApplicationIdAttributeName, string.Empty);
                    Execute(selectedInteraction);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
		}

		private void DebtReliefLogic(Interaction interaction, SfQueryResponse queryResponse, string applicationId) {
			try {
				string transferOutcome;

				var utmSource = string.IsNullOrEmpty(queryResponse.records.First().utm_source__c)
						? ""
						: queryResponse.records.First().utm_source__c;

				var loanOfficer = string.IsNullOrEmpty(queryResponse.records.First().Loan_Officer_Source__c)
						? ""
						: queryResponse.records.First().Loan_Officer_Source__c;

				var wholesaler = GetMatchingWholesalerRecord(utmSource, loanOfficer);

				if (wholesaler != null)
                {
                    string wholesalerName = GetWholesalerName(wholesaler.TransferDestination);
                    var transfer = WarmTransfer(interaction, wholesaler.TransferDestination, wholesalerName);
					if (transfer.Equals("cancel", StringComparison.CurrentCultureIgnoreCase)) {
						return;
					}


					//CreateCallRecord(interaction);	//CCP-291
					transferOutcome = PromptUserForTransferOutcome();
					var failureReason = string.Empty;
					if (transferOutcome.Equals("failure", StringComparison.CurrentCultureIgnoreCase)) {
						failureReason = PromptUserForFailureReason();
					}

					QbCreateTransferRecord(applicationId, "");
					return;
				}
				else {
					string leadId;
					if (string.IsNullOrEmpty(queryResponse.records[0].Referral_Record_ID__c) || !DoesLeadExistInQb(queryResponse.records[0].Referral_Owner__c)) {
						var dialogResult = DisplayMessageBox(
								"This call will be transferred to FDR." + Environment.NewLine +
								"Click OK to copy lead info to FDR and to begin the transfer." + Environment.NewLine +
								"Click Cancel to cancel the transfer.", "Call Transfer: FDR", MessageBoxButtons.OKCancel);
						if (dialogResult.Equals(DialogResult.Cancel)) {
							CancelTransfer();
							return;
						}

						leadId = GetInteractionAttributeValue(interaction, "FF_FdrLeadId");
						CopySfApplicationToQb(interaction, leadId, applicationId);
						leadId = GetInteractionAttributeValue(interaction, "FF_FdrLeadId");

						var transfer = ConferenceTransfer(interaction, "", "Debt Relief");
						if (transfer.Equals("cancel", StringComparison.CurrentCultureIgnoreCase)) {
							return;
						}

						transferOutcome = PromptUserForTransferOutcome();
						var failureReason = string.Empty;
						if (transferOutcome.Equals("failure", StringComparison.CurrentCultureIgnoreCase)) {
							failureReason = PromptUserForFailureReason();
						}

						QbCreateTransferRecord(applicationId, "7");
						return;
					}
					else {
						leadId = queryResponse.records[0].Referral_Record_ID__c;
						SetInteractionAttribute(interaction, "FF_FdrLeadId", leadId);

						var dialogResult = DisplayMessageBox(
								"This call will be transferred to FDR." + Environment.NewLine + "FDR Lead Id: " + leadId +
								Environment.NewLine + "Click OK to continue." + Environment.NewLine +
								"Click Cancel to cancel the transfer", "Call Transfer: FDR", MessageBoxButtons.OKCancel);
						if (dialogResult.Equals(DialogResult.Cancel)) {
							CancelTransfer();
							return;
						}

						var transfer = ConferenceTransfer(interaction, "", "Debt Relief");
						if (transfer.Equals("cancel", StringComparison.CurrentCultureIgnoreCase)) {
							return;
						}

						transferOutcome = PromptUserForTransferOutcome();
						var failureReason = string.Empty;
						if (transferOutcome.Equals("failure", StringComparison.CurrentCultureIgnoreCase)) {
							failureReason = PromptUserForFailureReason();
						}

						QbCreateTransferRecord(applicationId, "7");
						return;
					}
				}
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}
		}

		private bool DoesLeadExistInQb(string referralRecordId) {
			var ret = false;
			try {
				var lead = _client.QbGetLeadWithStatusById(referralRecordId);
				ret = lead != null && lead.record != null && !string.IsNullOrEmpty(lead.record.record_id_);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
				throw;
			}
			return ret;
		}

		private void OtherTypeLogic(Interaction interaction, SfQueryResponse queryResponse, string partnerId, string applicationId) {
			try {
				var utmSource = string.IsNullOrEmpty(queryResponse.records.First().utm_source__c)
						? ""
						: queryResponse.records.First().utm_source__c;

				var loanOfficer = string.IsNullOrEmpty(queryResponse.records.First().Loan_Officer_Source__c)
						? ""
						: queryResponse.records.First().Loan_Officer_Source__c;

				var wholesaler = GetMatchingWholesalerRecord(utmSource, loanOfficer);
				if (wholesaler != null) {
					Topic.Warning("Cannot transfer {} leads to partner", utmSource);
					_context.Send(p => MessageBox.Show("Cannot transfer " + utmSource + " leads to partner"), null);
					return;
				}

				var partner = QbGetPartnerById(partnerId);
				if (partner == null || partner.Records == null || !partner.Records.Any()) {
					Topic.Warning("No partner was found with the Id {}", queryResponse.records[0].Id);
					DisplayMessageBox("Partner record not found", "Partner not found", MessageBoxButtons.OK);
					return;
				}

				if (partner.Records.First().CurrentlyOpen.Equals(1)) {
					Topic.Note("The partner is currently open");

					var transfer = WarmTransfer(interaction, partner.Records.First().TransferPhoneNumber,
							partner.Records.First().PartnerName);
					if (transfer.Equals("cancel", StringComparison.CurrentCultureIgnoreCase)) {
						return;
					}


					//CreateCallRecord(interaction);	//CCP-291
					var transferOutcome = PromptUserForTransferOutcome();
					var failureReason = string.Empty;
					if (transferOutcome.Equals("failure", StringComparison.CurrentCultureIgnoreCase)) {
						failureReason = PromptUserForFailureReason();
					}

					QbCreateTransferRecord(applicationId, partnerId);
					return;
				}
				else {
					Topic.Note("The partner is not currently open");

					DisplayHoursOfOperationForm(partner);
					QbCreateTransferRecord(applicationId, partnerId);
					return;
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}

		private void CopySfApplicationToQb(Interaction interaction, string leadId, string applicationId) {
			try {
				if (!string.IsNullOrEmpty(leadId)) {
					var leadStatus = QbGetLeadWithStatusById(leadId);
					if (leadStatus != null && leadStatus.record != null &&
							!string.IsNullOrEmpty(leadStatus.record.record_id_)) {
						if (leadStatus.record.status.IndexOf("inactive", 0, StringComparison.CurrentCultureIgnoreCase) != -1) {
							_client.QbAssignLeadToAgent(leadId, "unassigned");
						}

						return;
					}
				}

				var applicationForCopy = SfGetApplicationByIdForCopyToQb(applicationId);

				if (applicationForCopy != null && applicationForCopy.records != null && applicationForCopy.totalSize > 0) {
					var record = applicationForCopy.records[0];
					qdbapi createNewLead;
					var tryCount = 0;
					do {
						createNewLead =
								_client.QbCreateNewLeadFromFreedomPlusApplication(record.Referral_Record_ID__c,
										record.Best_Callback_Time__c, record.Day_Phone__c, record.Email_Address__c,
										record.Evening_Phone__c, record.FDR_Lead_Keyword__c, record.FDR_Lead_Partner__c,
										record.FDR_Lead_Phase__c, record.FDR_Lead_Type__c, record.Filed_BK__c,
										record.First_Name__c,
										record.Hardship_Detail__c, record.If_BK_What_Year__c, record.Last_Name__c,
										record.Other_Phone__c, record.Own_or_Rent__c, record.Past_Due_Accounts__c,
										record.Physical_Address_1__c, record.Physical_City__c, record.Physical_State__c,
										record.Physical_Zip_Code__c, record.Unsecured_Debt_Estimate__c, record.Date_of_Birth__c);

						Topic.Note("The error code for QbCreateNewLeadFromFreedomPlusApplication is {}", createNewLead.errcode);
						tryCount++;

					} while (createNewLead.errcode.Equals(4) || createNewLead.errcode.Equals(22) || tryCount.Equals(3));

					if (!createNewLead.errcode.Equals(0)) {
						DisplayMessageBox(
								"Unable to copy lead from Salesforce to Quickbase." + Environment.NewLine +
								"The transfer will continue when you click OK" + Environment.NewLine +
								"If this problem persists, please submit an IT/OPS request", "Cannot copy lead",
								MessageBoxButtons.OK);
						return;
					}

					var newLeadId = string.IsNullOrEmpty(createNewLead.rid) ? leadId : createNewLead.rid;
					_client.SfEditApplicationToAddQbLeadId(newLeadId, applicationId);
					SetInteractionAttribute(interaction, "FF_FdrLeadId", newLeadId);
				}
				else {
					Topic.Warning("No applications were found! Unable to create a new FDR lead");
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}

		private static string TrimPhoneNumber(string ani) {
			var ret = "";

			using (Topic.Scope()) {
				try {
					ret = ani.Replace("(", "");
					ret = ret.Replace(")", "");
					ret = ret.Replace(" ", "");
					ret = ret.Replace("-", "");
					ret = ret.Replace("+", "");
					ret = ret.Replace("ip:", "");
					ret = ret.Replace("sip:", "");

					if (ret.Length > 10) {
						ret = ret.Substring(1, 10);
					}

					Topic.Verbose("Trimmed the ANI: {}", ret);
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}

			return ret;
		}

		private void CreateCallRecord(Interaction interaction) {
			return; //CCP-291
            /*
			try {
				var applicationId = interaction.GetStringAttribute("FF_FpApplicationId");
				var dnis = TrimPhoneNumber(interaction.GetStringAttribute("Eic_SipNumberLocal"));
				var ani = TrimPhoneNumber(interaction.GetStringAttribute(InteractionAttributeName.RemoteId));
				var wrapUpCode = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
				var autoMatched = interaction.GetStringAttribute("FF_AutoMatched")
						.Equals("true", StringComparison.CurrentCultureIgnoreCase);
				var direction =
						interaction.GetStringAttribute(InteractionAttributes.Direction)
								.Equals(InteractionAttributeValues.Direction.Incoming)
								? "IB"
								: "OB";


				while (applicationId.Length < 15) {
					var form = new ApplicationEntryForm(dnis, ani);
					_context.Send(p => {
						var ret = form.ShowDialog();
						if (ret.Equals(DialogResult.OK)) {
							applicationId = form.ApplicationIdValue;
						}
						else {
							applicationId = "";
						}
					}, null);

					Topic.Note("The application Id has been set to {}", applicationId);
					if (applicationId.Length.Equals(8)) {
						var applicationByName = _client.SfGetApplicationByName(applicationId);
						applicationId = applicationByName.totalSize.Equals(1) ? applicationByName.records[0].Id : "";
						break;
					}

					if (applicationId.Length.Equals(15) || applicationId.Length.Equals(18)) {
						var applicationById = _client.SfGetApplicationById(applicationId);
						applicationId = applicationById.totalSize.Equals(1) ? applicationById.records[0].Id : "";
						break;
					}

					if (applicationId.Length.Equals(0)) {
						applicationId = "";
						break;
					}

				}

				var startTimeString = _startTime.ToString(DateTimeFormat);
				CreateCallRecord(wrapUpCode, _extension, applicationId, autoMatched, direction, startTimeString, dnis, ani,
						false);
				Topic.Note("Created a call record");
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
            */
		}

		private void CreateCallRecord(string wrapUpCode, string extension, string applicationId, bool autoMatched,
				string direction, string startTime, string dnis, string ani, bool wcbCall) {
			return;   /*CCP-291
			try {
				var endTime = DateTime.Now.ToString(DateTimeFormat);
				if (Convert.ToDateTime(startTime).Year <= 0001) {
					startTime = endTime;
				}

				Topic.Note("SfCreateCallRecord Wrap Up Code: {}", wrapUpCode);
				Topic.Note("SfCreateCallRecord Extension: {}", extension);
				Topic.Note("SfCreateCallRecord Application Id: {}", applicationId);
				Topic.Note("SfCreateCallRecord AutoMatched: {}", autoMatched);
				Topic.Note("SfCreateCallRecord Direction: {}", direction);
				Topic.Note("SfCreateCallRecord Start Time: {}", startTime);
				Topic.Note("SfCreateCallRecord End Time: {}", endTime);
				Topic.Note("SfCreateCallRecord DNIS: {}", dnis);
				Topic.Note("SfCreateCallRecord ANI: {}", ani);
				Topic.Note("SfCreateCallRecord WCB Call: {}", wcbCall);

				var response = _client.SfCreateCallRecord(wrapUpCode, extension, applicationId, autoMatched, direction,
						endTime, startTime, dnis, ani, wcbCall);
				if (response != null && response.success) {
					Topic.Note("SfCreateCallRecord created new call record with Id: {}", response.id);
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
            */
		}

		#region Helper Methods
		private void CreateMiddlewareClient() {
			try {
				var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };

				var clientEndpoint = new EndpointAddress(_addin.MiddlewareServiceEndpoint);
				_client = new MiddlewareClient(clientBinding, clientEndpoint);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}
		}

		private Interaction ConvertIInteractionToInteration(IInteraction selectedInteraction) {
			Interaction ret = null;

			try {
				ret = InteractionsManager.CreateInteraction(new InteractionId(selectedInteraction.InteractionId));
				Topic.Verbose("Interaction object created: {}", ret.InteractionId.Id);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}

			return ret;
		}

		private string GetInteractionAttributeValue(Interaction interaction, string attributeName) {
			var ret = "";

			try {
				ret = interaction.GetStringAttribute(attributeName);
				Topic.Note("Attribute {}'s value is {}", attributeName, ret);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}

			return ret;
		}

		private void SetInteractionAttribute(Interaction interaction, string attributeName, string attributeValue) {
			try {
				interaction.SetStringAttribute(attributeName, attributeValue);
				Topic.Note("Set attribute {} to be {}", attributeName, attributeValue);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}
		}

        private string GetWholesalerName(string transferDestinaton)
        {
            string name = transferDestinaton;
            var ret = new WholesalersResponse.Wholesaler();
            try
            {
                Topic.Note("Searching for a wholesaler with the Transfer Destination {}", transferDestinaton);
                ret =
                        _client.GetWholesalers()
                                .WholesalerList.FirstOrDefault(x => x.TransferDestination.Equals(transferDestinaton, StringComparison.InvariantCultureIgnoreCase)&&!x.Dnis.Equals("utm_source", StringComparison.InvariantCultureIgnoreCase));

                if (ret != null)
                {
                    Topic.Note("Wholesaler Name: {}", ret.Name);
                    Topic.Note("Wholesaler DNIS: {}", ret.Dnis);
                    Topic.Note("Wholesaler Transfer Destination: {}", ret.TransferDestination);
                    name = ret.Name;
                }
                else
                {
                    Topic.Warning("No wholesaler matching the Transfer Destination {} could be found!", transferDestinaton);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return name;
        }

		private WholesalersResponse.Wholesaler GetMatchingWholesalerRecord(string utmSource, string loanOfficer) {
			var ret = new WholesalersResponse.Wholesaler();
			try {
				Topic.Note("Searching for a wholesaler with the Name {}", utmSource);
				ret =
						_client.GetWholesalers()
								.WholesalerList.FirstOrDefault(x => x.Name.Equals(utmSource, StringComparison.InvariantCultureIgnoreCase) ||
																										x.Name.Equals(loanOfficer, StringComparison.InvariantCultureIgnoreCase));

				if (ret != null) {
					Topic.Note("Wholesaler Name: {}", ret.Name);
					Topic.Note("Wholesaler DNIS: {}", ret.Dnis);
					Topic.Note("Wholesaler Transfer Destination: {}", ret.TransferDestination);
				}
				else {
					Topic.Warning("No wholesaler matching the Name {} could be found!", utmSource);
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
			return ret;
		}

		private void DisplayHoursOfOperationForm(QbGetPartnerByIdResponse record) {
			try {
				var dictionary = CreateHoursOfOperationDictionary(record);
				var form = new HoursOfOperationForm(dictionary,
						string.IsNullOrEmpty(record.Records[0].PartnerName) ? "" : record.Records[0].PartnerName,
						string.IsNullOrEmpty(record.Records[0].DirectNumber) ? "" : record.Records[0].DirectNumber);
				_context.Send(p => form.ShowDialog(), null);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}

		private Dictionary<string, string[]> CreateHoursOfOperationDictionary(QbGetPartnerByIdResponse partnerRecord) {
			var ret = new Dictionary<string, string[]>();

			try {
				ret.Add("Monday", new[] { partnerRecord.Records[0].Monday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].MondayOpenTime, partnerRecord.Records[0].MondayCloseTime });
				Topic.Note("Monday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Monday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].MondayOpenTime, partnerRecord.Records[0].MondayCloseTime);
				ret.Add("Tuesday", new[] { partnerRecord.Records[0].Tuesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].TuesdayOpenTime, partnerRecord.Records[0].TuesdayCloseTime });
				Topic.Note("Tuesday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Tuesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].TuesdayOpenTime, partnerRecord.Records[0].TuesdayCloseTime);
				ret.Add("Wednesday", new[] { partnerRecord.Records[0].Wednesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].WednesdayOpenTime, partnerRecord.Records[0].WednesdayCloseTime });
				Topic.Note("Wednesday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Wednesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].WednesdayOpenTime, partnerRecord.Records[0].WednesdayCloseTime);
				ret.Add("Thursday", new[] { partnerRecord.Records[0].Thursday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].ThursdayOpenTime, partnerRecord.Records[0].ThursdayCloseTime });
				Topic.Note("Thursday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Thursday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].ThursdayOpenTime, partnerRecord.Records[0].ThursdayCloseTime);
				ret.Add("Friday", new[] { partnerRecord.Records[0].Friday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].FridayOpenTime, partnerRecord.Records[0].FridayClosetime });
				Topic.Note("Friday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Friday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].FridayOpenTime, partnerRecord.Records[0].FridayClosetime);
				ret.Add("Saturday", new[] { partnerRecord.Records[0].Saturday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SaturdayOpenTime, partnerRecord.Records[0].SaturdayCloseTime });
				Topic.Note("Saturday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Saturday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SaturdayOpenTime, partnerRecord.Records[0].SaturdayCloseTime);
				ret.Add("Sunday", new[] { partnerRecord.Records[0].Sunday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SundayOpenTime, partnerRecord.Records[0].SundayCloseTime });
				Topic.Note("Sunday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Sunday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SundayOpenTime, partnerRecord.Records[0].SundayCloseTime);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}

			return ret;
		}
		#endregion

		#region Middleware Methods
		private QbGetPartnerByIdResponse QbGetPartnerById(string partnerId) {
			var ret = new QbGetPartnerByIdResponse();
			using (Topic.Scope()) {
				try {
					Topic.Note("Finding a partner with the Id: {}", partnerId);

					ret = _client.QbGetPartnerById(partnerId);

					if (ret.Records.Any()) {
						Topic.Verbose("QbGetPartnerById Partner Name: {}", ret.Records[0].PartnerName);
						Topic.Verbose("QbGetPartnerById Monday Open Time: {}", ret.Records[0].MondayOpenTime);
						Topic.Verbose("QbGetPartnerById Monday Close Time: {}", ret.Records[0].MondayCloseTime);
						Topic.Verbose("QbGetPartnerById Tuesday Open Time: {}", ret.Records[0].TuesdayOpenTime);
						Topic.Verbose("QbGetPartnerById Tuesday Close Time: {}", ret.Records[0].TuesdayCloseTime);
						Topic.Verbose("QbGetPartnerById Wednesday Open Time: {}", ret.Records[0].WednesdayOpenTime);
						Topic.Verbose("QbGetPartnerById Wednesday Close Time: {}", ret.Records[0].WednesdayCloseTime);
						Topic.Verbose("QbGetPartnerById Thursday Open Time: {}", ret.Records[0].ThursdayOpenTime);
						Topic.Verbose("QbGetPartnerById Thursday Close Time: {}", ret.Records[0].ThursdayCloseTime);
						Topic.Verbose("QbGetPartnerById Friday Open Time: {}", ret.Records[0].FridayOpenTime);
						Topic.Verbose("QbGetPartnerById Friday Close Time: {}", ret.Records[0].FridayClosetime);
						Topic.Verbose("QbGetPartnerById Saturday Open Time: {}", ret.Records[0].SaturdayOpenTime);
						Topic.Verbose("QbGetPartnerById Saturday Close Time: {}", ret.Records[0].SaturdayCloseTime);
						Topic.Verbose("QbGetPartnerById Sunday Open Time: {}", ret.Records[0].SundayOpenTime);
						Topic.Verbose("QbGetPartnerById Sunday Close Time: {}", ret.Records[0].SundayCloseTime);
						Topic.Verbose("QbGetPartnerById Currently Open: {}", ret.Records[0].CurrentlyOpen);
						Topic.Verbose("QbGetPartnerById FDR Account Code: {}", ret.Records[0].FdrAccountCode);
						Topic.Verbose("QbGetPartnerById Direct Number: {}", ret.Records[0].DirectNumber);
					}
					else {
						Topic.Status("Did not find a partner with the Id {}", partnerId);
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
			return ret;
		}

		private void QbCreateTransferRecord(string applicationId, string partnerRecordId) {
			using (Topic.Scope()) {
				try {
					Topic.Verbose("QbCreateTransferRecord Username: {}", Environment.UserName);
					Topic.Verbose("QbCreateTransferRecord Partner Record Id: {}", partnerRecordId);

					_client.QbCreateTransferRecordFreedomPlus(applicationId, Environment.UserName,
							string.IsNullOrEmpty(partnerRecordId) ? "" : partnerRecordId);

					Topic.Note("Created a new transfer record!");
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		private qdbapi QbGetLeadWithStatusById(string leadId) {
			var ret = new qdbapi();
			using (Topic.Scope()) {
				try {
					ret = _client.QbGetLeadWithStatusById(leadId);

					if (ret != null && ret.record != null) {
						Topic.Verbose("QbGetLeadWithStatusById Record Id: {}", ret.record.record_id_);
						Topic.Verbose("QbGetLeadWithStatusById Status: {}", ret.record.status);
					}
					else {
						Topic.Warning("QbGetLeadWithStatusById could not find any records for Lead Id: {}", leadId);
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
			return ret;
		}

		private SfFdrResponse SfGetApplicationByIdForCopyToQb(string applicationId) {
			var ret = new SfFdrResponse();
			using (Topic.Scope()) {
				try {
					Topic.Verbose("SfGetApplicationByIdForCopyToQb Application Id: {}", applicationId);

					ret = _client.SfGetApplicationByIdForCopyToQb(applicationId);

					if (ret != null && ret.totalSize > 0) {
						Topic.Verbose("SfGetApplicationByIdForCopyToQb Id: {}", ret.records[0].Id);
					}
					else {
						Topic.Warning("Unable to find an application with the Application Id: {}", applicationId);
					}
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
			return ret;
		}
		#endregion

		#region Display Methods
		private void CancelTransfer() {
			try {
				Topic.Warning("Transfer Cancelled");
				_context.Send(p => MessageBox.Show("Transfer Cancelled", "Transfer Cancelled"), null);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}

		private string GetTransferType() {
			var ret = "";

			try {
				var form = new TransferTypeSelectionForm();

				_context.Send(p => {
					using (form) {
						var result = form.ShowDialog();
						if (result.Equals(DialogResult.OK)) {
							ret = form.TransferType;
							Topic.Note("The selected transfer type is {}", ret);
						}
						else if (result.Equals(DialogResult.Cancel)) {
							ret = string.Empty;
							Topic.Note("The user cancelled the transfer");
						}
					}
				}, null);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}

			return ret;
		}

		private DialogResult DisplayMessageBox(string message, string caption, MessageBoxButtons buttons) {
			var ret = DialogResult.Cancel;

			try {
				_context.Send(p => {
					ret = MessageBox.Show(message, caption, buttons);
				}, null);

				Topic.Note("Displayed message: {}", message);
				Topic.Verbose("The user selected: {}", ret);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}

			return ret;
		}

		private string DisplayApplicationIdEntryForm(string dnis, string ani) {
			var ret = "";

			try {
				var form = new ApplicationEntryForm(dnis, ani);

				_context.Send(p => {
					using (form) {
						var response = form.ShowDialog();
						if (response.Equals(DialogResult.OK)) {
							ret = form.ApplicationIdValue;
						}
						else {
							ret = "cancel";
						}

					}
				}, null);

			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}

			return ret;
		}

		private string PromptUserForTransferOutcome() {
			var ret = "";

			try {
				var form = new TransferOutcomeForm();

				_context.Send(p => {
					using (form) {
						var result = form.ShowDialog();
						if (!result.Equals(DialogResult.OK)) { return; }

						ret = form.TransferOutcome;
						Topic.Note("The selected transfer outcome is {}", ret);
					}
				}, null);
			}
			catch (Exception ex) {
				_context.Send(p => MessageBox.Show(DefaultErrorMessage), null);
				Topic.Exception(ex);
			}

			return ret;
		}

		private string PromptUserForFailureReason() {
			var ret = "";

			try {
				var form = new FailureClassificationForm();

				_context.Send(p => {
					using (form) {
						var result = form.ShowDialog();
						if (result.Equals(DialogResult.OK)) {
							ret = form.FailureReason;
							Topic.Note("The selected failure reason is {}", ret);
						}
					}
				}, null);
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}

			return ret;
		}
		#endregion

		#region Transfer Methods

		private string WarmTransfer(Interaction interaction, string transferDestination, string wholesalerName) {
			var ret = "ok";
			try {
				var userSelection = DialogResult.Cancel;
				_context.Send(
						p => {
							userSelection = MessageBox.Show(
													"This call will be transferred to " + wholesalerName + ". Click OK to begin the transfer. Click Cancel to return to the propsect.",
													"Call Transfer: " + wholesalerName,
													MessageBoxButtons.OKCancel);

						}, null);

				if (userSelection.Equals(DialogResult.OK)) {
					Topic.Verbose("Agent has agreed to continue transfer of interaction {}",
							interaction.InteractionId.Id);

					var consultInteraction =
							InteractionsManager.MakeConsultTransfer(new ConsultTransferParameters((string.IsNullOrEmpty(transferDestination) ? this.transferDestinationDefault : transferDestination)
							, interaction.InteractionId));

					var newInteraction = InteractionsManager.CreateInteraction(consultInteraction.ConsultInteractionId);
					SetInteractionAttribute(newInteraction, LeadIdAttributeName,
							interaction.GetStringAttribute(LeadIdAttributeName));
					SetInteractionAttribute(newInteraction, ApplicationIdAttributeName,
							interaction.GetStringAttribute(ApplicationIdAttributeName));
					newInteraction.SetStringAttribute("FF_TransferredCall", "true");

					consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);

					userSelection = DialogResult.Cancel;
					userSelection =
							DisplayMessageBox(
									"Wait for an agent to answer and then announce your transfer. Please provide the parter agent with any requested information. Once the agent is ready, click OK to complete the transfer. Click Cancel to cancel the transfer and return to the propsect",
									"Call Transfer: " + wholesalerName, MessageBoxButtons.OKCancel);

					if (userSelection.Equals(DialogResult.OK)) {
						consultInteraction.Conclude();
						_context.Send(p => MessageBox.Show("Transfer completed"), null);
					}
					else {
						ret = "cancel";
						interaction.Hold(false);
						consultInteraction.Cancel();
						_context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
						return ret;
					}
				}
				else {
					ret = "cancel";
					interaction.Hold(false);
					Topic.Warning("Agent has cancelled transfer of interaction {}", interaction.InteractionId.Id);
					_context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
			return ret;
		}

		private string ConferenceTransfer(Interaction interaction, string transferDestination, string partnerName) {
			var ret = "ok";
			try {
				Topic.Verbose("Interaction {} is now on hold", interaction.InteractionId.Id);

				var consultInteraction =
						InteractionsManager.MakeConsultTransfer(new ConsultTransferParameters((string.IsNullOrEmpty(transferDestination) ? this.transferDestinationDefault : transferDestination)
						, interaction.InteractionId));

				var newInteraction = InteractionsManager.CreateInteraction(consultInteraction.ConsultInteractionId);
				SetInteractionAttribute(newInteraction, LeadIdAttributeName,
						interaction.GetStringAttribute(LeadIdAttributeName));
				SetInteractionAttribute(newInteraction, ApplicationIdAttributeName,
						interaction.GetStringAttribute(ApplicationIdAttributeName));
				newInteraction.SetStringAttribute("FF_TransferredCall", "true");

				var userSelection = DialogResult.Cancel;
				_context.Send(
						p => {
							userSelection = MessageBox.Show(
													"Provide client info to " + partnerName + " agent. When " +
													partnerName +
													" agent is ready, click OK to connect the client. Click Cancel to cancel the transfer.",
													"Call Transfer: " + partnerName,
													MessageBoxButtons.OKCancel);

						}, null);

				if (userSelection.Equals(DialogResult.OK)) {
					consultInteraction.ChangeSpeakers(ConsultTransferParticipants.All);

					userSelection = DialogResult.Cancel;
					_context.Send(
							p => {
								userSelection = MessageBox.Show(
															"Introduce client and " + partnerName + " agent. When ready, click OK to complete the handoff. To cancel, click Cancel and ask the " +
															partnerName +
															" agent to hang up.",
															"Call Transfer: " + partnerName,
															MessageBoxButtons.OKCancel);

							}, null);

					if (userSelection.Equals(DialogResult.OK)) {
						consultInteraction.Conclude();
						_context.Send(p => MessageBox.Show(partnerName + " handoff complete."), null);
						//CreateCallRecord(interaction);	//CCP-291
					}
					else {
						ret = "cancel";
						interaction.Hold(false);
						consultInteraction.Cancel();
						_context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
					}
				}
				else {
					ret = "cancel";
					interaction.Hold(false);
					consultInteraction.Cancel();
					Topic.Verbose("Interaction {} is now off hold", interaction.InteractionId.Id);

					_context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
				}
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
			return ret;
		}

		#endregion

		#region Admin Session
		private void CreateAdminSession() {
			try {
				Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host, AdminUsername);
                _adminSession = new Session
                {
                    AutoReconnectEnabled = true
                };
                _adminSession.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
						new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}

		}

		private void GetLoggedInUsersExtension() {
			using (Topic.Scope()) {
				try {
					var userConfigurationList = new UserConfigurationList(ConfigurationManager.GetInstance(_adminSession));
					var querySettings = userConfigurationList.CreateQuerySettings();
					querySettings.SetPropertiesToRetrieve(new[]
					{
												UserConfiguration.Property.Id, UserConfiguration.Property.Extension
										});
					querySettings.SetFilterDefinition(
							new BasicFilterDefinition<UserConfiguration, UserConfiguration.Property>(
									UserConfiguration.Property.Id, _session.UserId));

					userConfigurationList.StartCaching(querySettings);

					var user =
							userConfigurationList.GetConfigurationList()
									.FirstOrDefault(x => x.ConfigurationId.Id.Equals(_session.UserId));

					if (user != null) {
						_extension = user.Extension.Value;
						Topic.Status("{}'s extension is '{}'", _session.UserId, _extension);
					}

					userConfigurationList.StopCaching();
				}
				catch (Exception ex) {
					Topic.Exception(ex);
				}
			}
		}

		private void DisconnectAdminSession() {
			try {
				_adminSession.Disconnect();
				Topic.Status("Proxy session has been disconnected");
			}
			catch (Exception ex) {
				Topic.Exception(ex);
			}
		}
		#endregion
	}
}
