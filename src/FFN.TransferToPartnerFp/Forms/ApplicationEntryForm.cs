﻿using System;
using System.Windows.Forms;

namespace FFN.TransferToPartnerFp.Addin.Forms
{
    public partial class ApplicationEntryForm : Form
    {
        public string Ani { get; set; }
        public string Dnis { get; set; }
        public string ApplicationIdValue { get; private set; }

        public ApplicationEntryForm(string dnis, string ani)
        {
            InitializeComponent();
            aniLabel.Text = ani;
            dnisLabel.Text = dnis;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            ApplicationIdValue = leadIdTextbox.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ApplicationIdValue = string.Empty;
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
