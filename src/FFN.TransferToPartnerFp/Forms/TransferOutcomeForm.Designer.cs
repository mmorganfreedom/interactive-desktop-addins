﻿namespace FFN.TransferToPartnerFp.Addin.Forms
{
    partial class TransferOutcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferOutcomeForm));
            this.label1 = new System.Windows.Forms.Label();
            this.transferOutcomeComboBox = new System.Windows.Forms.ComboBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please select a transfer outcome:";
            // 
            // transferOutcomeComboBox
            // 
            this.transferOutcomeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transferOutcomeComboBox.FormattingEnabled = true;
            this.transferOutcomeComboBox.Items.AddRange(new object[] {
            "Success",
            "Failure"});
            this.transferOutcomeComboBox.Location = new System.Drawing.Point(15, 25);
            this.transferOutcomeComboBox.Name = "transferOutcomeComboBox";
            this.transferOutcomeComboBox.Size = new System.Drawing.Size(367, 21);
            this.transferOutcomeComboBox.TabIndex = 1;
            this.transferOutcomeComboBox.SelectedIndexChanged += new System.EventHandler(this.transferOutcomeComboBox_SelectedIndexChanged);
            // 
            // submitButton
            // 
            this.submitButton.Enabled = false;
            this.submitButton.Location = new System.Drawing.Point(307, 52);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 3;
            this.submitButton.Text = "OK";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // TransferOutcomeForm
            // 
            this.AcceptButton = this.submitButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 83);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.transferOutcomeComboBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TransferOutcomeForm";
            this.Text = "Transfer Outcome";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox transferOutcomeComboBox;
        private System.Windows.Forms.Button submitButton;
    }
}