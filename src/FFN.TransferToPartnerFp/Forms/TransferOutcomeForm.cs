﻿using System;
using System.Windows.Forms;

namespace FFN.TransferToPartnerFp.Addin.Forms
{
    public partial class TransferOutcomeForm : Form
    {
        public string TransferOutcome { get; set; }

        public TransferOutcomeForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            TransferOutcome = transferOutcomeComboBox.GetItemText(transferOutcomeComboBox.SelectedItem);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void transferOutcomeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (transferOutcomeComboBox.SelectedIndex > -1)
            {
                submitButton.Enabled = true;
            }
        }
    }
}
