﻿using System;
using System.Windows.Forms;

namespace FFN.TransferToPartnerFp.Addin.Forms
{
    public partial class FailureClassificationForm : Form
    {
        public string FailureReason { get; private set; }

        public FailureClassificationForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            FailureReason = failureReasonCombobox.GetItemText(failureReasonCombobox.SelectedItem);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void failureReasonCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (failureReasonCombobox.SelectedIndex > -1)
            {
                submitButton.Enabled = true;
            }
        }
    }
}
