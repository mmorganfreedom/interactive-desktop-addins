﻿using System;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.TransferToPartnerFp.Addin.Forms
{
    public partial class TransferTypeSelectionForm : Form
    {
        public string TransferType { get; set; }

        public static readonly ITopicTracer Topic =
            TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFreedomPlus.TransferTypeForm");

        public TransferTypeSelectionForm()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                TransferType = transferTypeComboBox.GetItemText(transferTypeComboBox.SelectedItem);
                Topic.Note("Set the transfer type to be {}", TransferType);

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                Topic.Note("The user has cancelled the transfer type form");
                DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
    }
}
