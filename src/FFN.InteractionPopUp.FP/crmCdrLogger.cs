﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using ININ.Diagnostics;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;
using InteractionPopUpFreedomPlus.Forms;
using InteractionPopUpFreedomPlus.MiddlewareService;


namespace InteractionPopUpFreedomPlus
{

    public class CrmCdrLogger
    {
        #region init
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.InteractionPopUpFreedomPlus.AddIn");



        private IServiceProvider _serviceProvider;
        private Session _session;
        private readonly Session _adminSession;
        private MiddlewareClient _middlewareClient;
  
        private InteractionsManager _interactionsManager;
        private SynchronizationContext _synchronizationContext;
        private Interaction _theInteraction;



        private readonly string _interactionId;
        private readonly string _userId;
        private readonly string _userExtension;
        private string _theInteractionDirection;
        private string _theInteractionCallType;
        private readonly string _middlewareServiceEndpoint;
        #endregion



        #region const
        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzzzz";
        private const string FdrLeadIdAttributeName = "FF_FdrLeadId";
        #endregion



        #region CRM Logging
        public CrmCdrLogger(string interactionId, IServiceProvider serviceProvider
            , SynchronizationContext context, string middlewareServiceEndpoint
            , string userId, string theInteractionCallDirection, string userExtension)
        {
            _serviceProvider = serviceProvider;
            _synchronizationContext = context;
            _interactionId = interactionId;
            _userId = userId;
            _userExtension = userExtension;
            _theInteractionDirection = theInteractionCallDirection;
            _middlewareServiceEndpoint = middlewareServiceEndpoint;
            var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };
            var clientEndpoint = new EndpointAddress(middlewareServiceEndpoint);

            clientBinding.MaxReceivedMessageSize = 20000000;
            clientBinding.MaxBufferPoolSize = 20000000;
            clientBinding.MaxBufferSize = 20000000;

            _middlewareClient = new MiddlewareClient(clientBinding, clientEndpoint);
        }


        /* *
         * Poll the interaction for a state change every second.
         * If the call is transferred, it will throw an exception, but since the 
         * */
        public void CrmCdrLog()
        {
            using (Topic.Scope())
            {
                try
                {
                    #region init
                    _session = _serviceProvider.GetService(typeof(Session)) as Session;
                    _interactionsManager = InteractionsManager.GetInstance(_session);
                    _theInteraction = _interactionsManager.CreateInteraction(new InteractionId(_interactionId));

                    _theInteraction.StartWatching(new string[] { InteractionAttributeName.State });

                    string theInteractionLastConnectedUser = _theInteraction.GetStringAttribute("Eic_LastConnectedUser");
                    string theInteractionCurrentUser = _theInteraction.GetStringAttribute("FF_Addin_UserId");
                    string theInteractionState = _theInteraction.GetStringAttribute(InteractionAttributes.State);
                    _theInteractionCallType = _theInteraction.GetStringAttribute(InteractionAttributes.CallType);

                    bool isTheInteractionTransferred = (_theInteraction.GetStringAttribute("FF_TransferredCall")).Equals("true");
                    bool isTheInteractionInAValidDisconnectState = false;

                    int callStateWatchCount = 0;
                    #endregion

                    Topic.Note("Watching for a disconnect for IninId {}. Watch was initiated for user {}, current user is {}, last connected user was {}.", _theInteraction.InteractionId, _userId, theInteractionCurrentUser, theInteractionLastConnectedUser);

                    #region watch for disconnect
                    while (theInteractionState != InteractionAttributeValues.State.ExternalDisconnect
                        && theInteractionState != InteractionAttributeValues.State.InternalDisconnect)
                    {
                        //CIC timing issues result in these attributes not always being populated when initially queried.
                        if (string.IsNullOrEmpty(_theInteractionCallType))
                        {
                            _theInteractionCallType = _theInteraction.GetStringAttribute(InteractionAttributes.CallType);
                        }

                        isTheInteractionInAValidDisconnectState = (
                                                                    (
                                                                        (_theInteractionDirection.Equals(InteractionAttributeValues.Direction.Incoming) && _theInteractionCallType.Equals(InteractionAttributeValues.CallType.External) && !string.IsNullOrEmpty(theInteractionLastConnectedUser))
                                                                        ||
                                                                        (_theInteractionDirection.Equals(InteractionAttributeValues.Direction.Outgoing) && _theInteractionCallType.Equals(InteractionAttributeValues.CallType.External))
                                                                    )
                                                                    &&
                                                                    !isTheInteractionTransferred
                                                                    );

                        //only write out every 60s to prevent flooding the log
                        if (callStateWatchCount % 60 == 0)
                        {
                            Topic.Verbose("crmCdrLogger state IninID={}, value = {} with user {} (iteration count {})", _theInteraction.InteractionId, theInteractionState, _userId, callStateWatchCount);
                        }

                        callStateWatchCount++;
                        theInteractionCurrentUser = _theInteraction.GetStringAttribute("FF_Addin_UserId");

                        //check for call having been transferred away
                        if (theInteractionCurrentUser != _userId)
                        {
                            break;
                        }
                        else
                        {
                            theInteractionState = _theInteraction.GetStringAttribute(InteractionAttributes.State);
                            Thread.Sleep(1000);
                        }
                    }
                    #endregion

                    Topic.Note("Disconnect detected for IninID {}. Watch was initiated for user {}, current user is {}, last connected user was {}.", _theInteraction.InteractionId, _userId, theInteractionCurrentUser, theInteractionLastConnectedUser);

                    #region do crm crud?
                    if ((theInteractionCurrentUser == _userId)
                        &&
                        (theInteractionState == InteractionAttributeValues.State.ExternalDisconnect || theInteractionState == InteractionAttributeValues.State.InternalDisconnect)
                        )
                    {
                        Topic.Verbose("Disconnection state variables for IninID {}: direction={}, call type={}, current user={}, last connected user={}, attr FF_TransferredCall={}", _theInteraction.InteractionId, _theInteractionDirection, _theInteractionCallType, theInteractionCurrentUser, theInteractionLastConnectedUser, isTheInteractionTransferred);

                        if (isTheInteractionInAValidDisconnectState)
                        {
                            Topic.Note("We need to create a call record for this call!  IninID {}", _theInteraction.InteractionId);
                            CreateCallRecord(_theInteraction);
                        }
                        else
                        {
                            Topic.Note("We should not create a call record for this call.  IninID {}", _theInteraction.InteractionId);
                        }
                    }

                    _theInteraction.StopWatching();
                    #endregion
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region SF CRUD
        private void CreateCallRecord(Interaction interaction)
        {
            try
            {
                string startTime = interaction.GetStringAttribute("FF_StartTime");
                string endTime = DateTime.Now.ToString(DateTimeFormat);
                string applicationId = interaction.GetStringAttribute("FF_FpApplicationId");
                string dnis = TrimPhoneNumber(interaction.GetStringAttribute("Eic_SipNumberLocal"));
                string ani = TrimPhoneNumber(interaction.GetStringAttribute(InteractionAttributeName.RemoteId));
                string wrapUpCode = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                bool autoMatched = interaction.GetStringAttribute("FF_AutoMatched").Equals("true", StringComparison.CurrentCultureIgnoreCase);
                string callDirectionSf = (interaction.GetStringAttribute(InteractionAttributes.Direction).Equals(InteractionAttributeValues.Direction.Incoming) ? "IB" : "OB");

                if (callDirectionSf.Equals("OB"))
                {
                    dnis = string.Empty;
                    Topic.Note("This call is outbound. Setting the DNIS field to empty");
                    if (IsTransferDestinationWholeSaler(ani))
                    {
                        return;
                    }
                    //return;
                }

                while (string.IsNullOrEmpty(applicationId) || applicationId.Length < 15)
                {
                    Topic.Note("No application Id is set. Prompting for user input");
                    var form = new ApplicationEntryForm(dnis, ani);
                    _synchronizationContext.Send(p =>
                    {
                        using (form)
                        {
                            var ret = form.ShowDialog();
                            if (ret.Equals(DialogResult.OK))
                            {
                                applicationId = form.ApplicationIdValue;
                            }
                            else
                            {
                                applicationId = "";
                            }
                        }

                    }, null);

                    Topic.Note("The application Id has been set to {}", applicationId);
                    if (applicationId.Length.Equals(8))
                    {
                        var applicationByName = _middlewareClient.SfGetApplicationByName(applicationId);
                        applicationId = applicationByName.totalSize.Equals(1) ? applicationByName.records[0].Id : "";
                        break;
                    }

                    if (applicationId.Length.Equals(15) || applicationId.Length.Equals(18))
                    {
                        var applicationById = _middlewareClient.SfGetApplicationById(applicationId);
                        applicationId = applicationById.totalSize.Equals(1) ? applicationById.records[0].Id : "";
                        break;
                    }

                    if (applicationId.Length.Equals(0))
                    {
                        applicationId = "";
                        break;
                    }

                }

                SfCreateCallRecord(wrapUpCode, _userExtension, applicationId, autoMatched, callDirectionSf, startTime, dnis, ani, false);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }


        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope())
            {
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    serverParameters.StartWatching(_serverParameterName);
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
                    }
                    else
                    {
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Watch Parameter Method Failure: " + ex.Message);
                    Topic.Exception(ex);
                }
            }
        }

        private void CreateMiddlewareClient()
        {
            try
            {
                var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };
                MiddlewareServiceEndpoint = _middlewareServiceEndpoint;
                var clientEndpoint = new EndpointAddress(MiddlewareServiceEndpoint);
                _middlewareClient = new MiddlewareClient(clientBinding, clientEndpoint);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                MessageBox.Show("Middle ware client Failed: " + ex.Message);
            }
        }

        private bool IsTransferDestinationWholeSaler(string TransferDestination) 
        {
            var ret = new WholesalersResponse.Wholesaler();
            bool isWholesaler = false;
            try
            {
                Topic.Note("Searching for a wholesaler with the Transfer Destination {}", TransferDestination);
                CreateMiddlewareClient();
                ret = _middlewareClient.GetWholesalers().WholesalerList.FirstOrDefault(x => x.TransferDestination.Equals(TransferDestination));
                if (ret != null)
                {
                    isWholesaler = true;
                }
                else
                {
                    isWholesaler = false;
                    Topic.Warning("No wholesaler matching the Transfer Destination {} could be found!", TransferDestination);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in wholesale Method: " + ex.Message);
                Topic.Exception(ex);
            }
            return isWholesaler;
        }

        private void SfCreateCallRecord(string wrapUpCode, string extension
            , string applicationId, bool autoMatched, string direction
            , string startTime, string dnis, string ani, bool wcbCall
            )
        {
            try
            {
                string endTime = DateTime.Now.ToString(DateTimeFormat);

                if (Convert.ToDateTime(startTime).Year <= 0001)
                {
                    startTime = endTime;
                }

                Topic.Verbose("SfCreateCallRecord Wrap Up Code: {}", wrapUpCode);
                Topic.Verbose("SfCreateCallRecord Extension: {}", extension);
                Topic.Verbose("SfCreateCallRecord Application Id: {}", applicationId);
                Topic.Verbose("SfCreateCallRecord AutoMatched: {}", autoMatched);
                Topic.Verbose("SfCreateCallRecord Direction: {}", direction);
                Topic.Verbose("SfCreateCallRecord Start Time: {}", startTime);
                Topic.Verbose("SfCreateCallRecord End Time: {}", endTime);
                Topic.Verbose("SfCreateCallRecord DNIS: {}", dnis);
                Topic.Verbose("SfCreateCallRecord ANI: {}", ani);
                Topic.Verbose("SfCreateCallRecord WCB Call: {}", wcbCall);

                var response = _middlewareClient.SfCreateCallRecord(wrapUpCode, extension
                    , applicationId, autoMatched, direction
                    , endTime, startTime, dnis
                    , ani, wcbCall);

                if (response != null && response.success)
                {
                    Topic.Note("SfCreateCallRecord created new call record with Id: {}", response.id);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion

        #region Utilities
        private static string TrimPhoneNumber(string ani)
        {
            var ret = "";

            using (Topic.Scope())
            {
                try
                {
                    ret = ani.Replace("(", "");
                    ret = ret.Replace(")", "");
                    ret = ret.Replace(" ", "");
                    ret = ret.Replace("-", "");
                    ret = ret.Replace("+", "");
                    ret = ret.Replace("sip:", "");
                    ret = ret.Replace("ip:", "");

                    if (ret.Length > 10)
                    {
                        ret = ret.Substring(1, 10);
                    }

                    Topic.Verbose("Trimmed the phone number: {}", ret);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }
        #endregion

    }
};

