﻿using System;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace InteractionPopUpFreedomPlus.Forms
{
    public partial class ApplicationEntryForm : Form
    {
        public string Ani { get; set; }
        public string Dnis { get; set; }
        public string ApplicationIdValue { get; private set; }

        private static readonly ITopicTracer Topic =
            TopicTracerFactory.CreateTopicTracer("FFN.InteractionPopUpFreedomPlus.ApplicationEntryForm");

        public ApplicationEntryForm(string dnis, string ani)
        {
            try
            {
                InitializeComponent();
                SetLables(dnis, ani);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }
        }

        private void SetLables(string dnis, string ani)
        {
            try
            {
                var dnisValue = !string.IsNullOrEmpty(dnis) ? dnis : "No DNIS found!";
                var aniValue = !string.IsNullOrEmpty(ani) ? ani : "No ANI found!";
                Topic.Note("Setting the DNIS field to: {}", dnisValue);
                Topic.Note("Setting the ANI field to: {}", aniValue);

                dnisLabel.Text = dnisValue;
                aniLabel.Text = aniValue;
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            ApplicationIdValue = leadIdTextbox.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
