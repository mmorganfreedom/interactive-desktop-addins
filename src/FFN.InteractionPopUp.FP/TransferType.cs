﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace InteractionPopUpFreedomPlus
{
    [XmlRoot(ElementName = "transfertypes")]
    public class TransferTypes
    {
        public TransferTypes() { Items = new List<TransferType>(); }
        [XmlElement("transfertype")]
        public List<TransferType> Items { get; set; }

        public class TransferType
        {
            [XmlElement("displayname")]
            public string DisplayName { get; set; }

            [XmlElement("transfercode")]
            public string TransferCode { get; set; }

            [XmlElement("transferdestnum")]
            public string TransferDestinationNumber { get; set; }

            [XmlElement("transferdestflow")]
            public string TransferDestinationFlow { get; set; }

            [XmlElement("transfermethod")]
            public string TransferMethod { get; set; }
        }
    }
}
