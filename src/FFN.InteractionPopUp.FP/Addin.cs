﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using ININ.InteractionClient;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;
using ININ.Diagnostics;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Interactions;
using InteractionPopUpFreedomPlus.Forms;
using InteractionPopUpFreedomPlus.MiddlewareService;
using System.Collections.Specialized;
using ServerParameters = ININ.IceLib.Connection.Extensions.ServerParameters;


namespace InteractionPopUpFreedomPlus
{
    public class DotNetClientAddin : QueueMonitor
    {
        #region const
        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzzzz";
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";
        #endregion



        #region Variables
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.InteractionPopUpFreedomPlus.AddIn");

        // Interaction Management
        private readonly List<IInteraction> _currentInteractions = new List<IInteraction>();
        private readonly object _interactionsListLock = new object();

        private Session _session;
        private Session _adminSession;
        private InteractionsManager _interactionsManager;
        private SynchronizationContext _synchronizationContext;
        private MiddlewareClient _middlewareClient;
        private IServiceProvider _serviceProvider;

        private string UserId { get; set; }

        private NameValueCollection _disconnectMonitorInteractionIds = new NameValueCollection();
        private string _salesforceInstanceUrl;
        private string _extension = "";
        #endregion



        protected override IEnumerable<string> Attributes
        {
            get
            {
                return new[]
                {
                    InteractionAttributes.Direction,
                    InteractionAttributes.CallType,
                    ININ.InteractionClient.Interactions.InteractionAttributeName.RemoteId,
                    InteractionAttributes.RemoteId,
                    ININ.IceLib.Interactions.InteractionAttributeName.LocalId,
                    ININ.IceLib.Interactions.InteractionAttributeName.LocalAddress,
                    "FF_FdrLeadId",
                    "Eic_CallStateString",
                    "Eic_AniDnisString",
                    "FF_FpApplicationId",
                    "Eic_SipNumberLocal"
                };
            }
        }

        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            try
            {
                _serviceProvider = serviceProvider;
                _synchronizationContext = SynchronizationContext.Current; // Loading Icelib Session
                _session = _serviceProvider.GetService(typeof(Session)) as Session;
                _interactionsManager = InteractionsManager.GetInstance(_session);

                UserId = Environment.UserName;
                
                CreateAdminSession();
                WatchServerParameter();
                CreateMiddlewareClient();
                GetLoggedInUsersExtension();

                _salesforceInstanceUrl = RemoveForwardSlashFromEnd(_middlewareClient.IVRGetSalesforceInstanceUrl());

                Topic.Note("The URL for this SalesForce instance is {}", _salesforceInstanceUrl);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        

        protected override void InteractionChanged(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    var callDirection = interaction.GetAttribute(InteractionAttributes.Direction);
                    var callType = interaction.GetAttribute(InteractionAttributes.CallType);
                    var callStateString = interaction.GetAttribute("Eic_CallStateString");
                    var callState = interaction.GetAttribute(InteractionAttributes.State);
                    Topic.Verbose("Interaction {} Call Direction: {} | Call Type: {} | Call State: {}",interaction.InteractionId, callDirection, callType, callState);                    
                    if (callState.ToString() == "I"|| callState.ToString() == "E")
                    {
                    } 
                    if (_disconnectMonitorInteractionIds[interaction.InteractionId] == null)
                    {
                        _disconnectMonitorInteractionIds[interaction.InteractionId] = interaction.InteractionId;
                        interaction.SetAttribute("FF_Addin_UserId", UserId);
                        MonitorInteractionForDisconnect(interaction);
                    }
                    if (!_currentInteractions.Contains(interaction))
                    {
                        if (callDirection.Equals(InteractionAttributeValues.Direction.Outgoing) && callType.Equals(InteractionAttributeValues.CallType.External) &&
                            callState.Equals(InteractionAttributeValues.State.Proceeding))
                        {
                            Topic.Note("Interaction {} is ringing external outbound");
                            RingingExternalOutbound(interaction);
                            AddInteraction(interaction);
                        }
                        else if (callDirection.Equals(InteractionAttributeValues.Direction.Incoming) &&
                                 callType.Equals(InteractionAttributeValues.CallType.External) &&
                                 callState.Equals(InteractionAttributeValues.State.Connected))
                        {
                            Topic.Note("Interaction {} is alerting external inbound");
                            ExternalInboundCall(interaction);
                            AddInteraction(interaction);
                        }
                        else if (callDirection.Equals(InteractionAttributeValues.Direction.Incoming) &&
                                 callType.Equals(InteractionAttributeValues.CallType.Intercom) &&
                                 callState.Equals(InteractionAttributeValues.State.Connected))
                        {
                            Topic.Note("Interaction {} is answered internal inbound");
                            AnsweredInternalInbound(interaction);
                            AddInteraction(interaction);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                    MessageBox.Show("Interaction Change Error: " + ex.Message);
                }
            }
        }

        /// <summary>
        ///     This method is called when the interaction is removed from queue.
        /// </summary>
        /// <param name="interaction">interaction in queue</param>
        protected override void InteractionRemoved(IInteraction interaction)
        {
            RemoveInteraction(interaction);
            _disconnectMonitorInteractionIds.Remove(interaction.InteractionId);
        }

        protected override void OnUnload()
        {
            using (Topic.Scope())
            {
                Topic.Note("{} OnUnload.", "Interaction Pop-Up Freedom Plus");
                try
                {
                    Topic.Note("Stop Monitoring");
                    StopMonitoring();
                    DisconnectAdminSession();
                    _session = null;
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }



        #region Interaction List Management

        private void AddInteraction(IInteraction interaction)
        {
            using (Topic.Scope())
            {             
                try
                {
                    lock (_interactionsListLock)
                    {
                        if (!_currentInteractions.Contains(interaction))
                        {
                            Topic.Note("Interaction doesn't exist in monitoring list, adding it");
                            _currentInteractions.Add(interaction);
                            Topic.Note("_currentInteractions Count: {}", _currentInteractions.Count);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        private void RemoveInteraction(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    lock (_interactionsListLock)
                    {
                        if (_currentInteractions.Contains(interaction))
                        {
                            Topic.Verbose("Remove Interaction: {} from current list.", interaction);
                            _currentInteractions.Remove(interaction);
                        }
                    }

                    base.InteractionRemoved(interaction);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        #endregion

        #region Display Methods
        private string DisplayEmailEntryForm()
        {
            var ret = "";

            try
            {
                var form = new EmailAddressEntryForm();
                _synchronizationContext.Send(p =>
                {
                    var result = form.ShowDialog();
                    if (result.Equals(DialogResult.OK))
                    {
                        ret = form.EmailAddress;
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }
        #endregion

        #region Screen Pop Logic
        private void RingingExternalOutbound(IInteraction ringingInteraction)
        {
            using (Topic.Scope())
            {
                try
                {
                    var interaction = ConvertIInteractionToInteraction(ringingInteraction);
                    var applicationId = GetInteractionAttributeValue(interaction, "FF_FpApplicationId");

                    // Get the ANI and DNIS
                    var ani = TrimPhoneNumber(GetInteractionAttributeValue(interaction, ININ.IceLib.Interactions.InteractionAttributeName.RemoteId));

                    var startTime = DateTime.Now;
                    SetInteractionAttributeValue(interaction, "FF_StartTime", startTime.ToString(DateTimeFormat));

                    // Check if the application ID is greater than or equal to 8 characters
                    Topic.Note("The length of the application Id is {}", applicationId.Length);
                    if (applicationId.Length >= 8)
                    {
                        var applicationByName = _middlewareClient.SfGetApplicationById(applicationId);

                        // Determine how many applications were found from this name
                        Topic.Verbose("Found {} applications with the Id {}", applicationByName.totalSize, applicationId);
                        applicationId = applicationByName.totalSize.Equals(1) ? applicationByName.records[0].Id : string.Empty;
                        SetInteractionAttributeValue(interaction, "FF_FpApplicationId", applicationId);

                        var employeeByUsername = _middlewareClient.SfGetEmployeeByUsername(UserId);
                        if (employeeByUsername.TotalSize <= 0)
                        {
                            Topic.Status("Unable to find any employees with the username {}", UserId);
                            return;
                        }
                    }
                    else
                    {
                        var applicationByAni = _middlewareClient.SfGetApplicationByAni(ani);

                        if (applicationByAni == null)
                        {
                            Topic.Note("Application by ANI is null when ani is: {}", ani);
                            return;
                        }

                        Topic.Note("Application by ANI error code: {}", applicationByAni.errorCode);
                        if (applicationByAni.errorCode != null) { return; }

                        Topic.Verbose("Found {} applications with the ANI {}", applicationByAni.totalSize, ani);
                        if (!applicationByAni.totalSize.Equals(1))
                        {
                            Topic.Status("Did not find exactly 1 application that matched the ANI {}", ani);

                            applicationId = string.Empty;
                            SetInteractionAttributeValue(interaction, "FF_FpApplicationId", applicationId);
                        }
                        else
                        {
                            Topic.Note("Found exactly 1 application that matched the ANI {}", ani);

                            applicationId = applicationByAni.records[0].Id;
                            SetInteractionAttributeValue(interaction, "FF_FpApplicationId", applicationId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void AnsweredInternalInbound(IInteraction ringingInteraction)
        {
            using (Topic.Scope())
            {
                try
                {
                    var interaction = ConvertIInteractionToInteraction(ringingInteraction);
                    var applicationId = GetInteractionAttributeValue(interaction, "FF_FpApplicationId");
                    SetInteractionAttributeValue(interaction, "FF_StartTime", DateTime.Now.ToString(DateTimeFormat));
                    var leadId = string.Empty;

                    var ani = TrimPhoneNumber(GetInteractionAttributeValue(interaction, ININ.IceLib.Interactions.InteractionAttributeName.RemoteId));
                    var dnis = TrimPhoneNumber(GetInteractionAttributeValue(interaction, "Eic_SipNumberLocal"));

                    Topic.Note("The application Id length is {}", applicationId.Length);
                    if (applicationId.Length.Equals(7))
                    {
                        leadId = applicationId;
                    }

                    SetInteractionAttributeValue(interaction, "FF_FdrLeadId", applicationId);
                    Topic.Note("Attribute {} has been set to {} on interaction {}", "FF_FdrLeadId", applicationId,
                        interaction.InteractionId.Id);

                    SfQueryResponse applicationRelatedToLeadId = null;

                    if (string.IsNullOrEmpty(leadId))
                    {
                        if (!string.IsNullOrEmpty(applicationId))
                        {
                            Topic.Note("Getting Sf application by id: {}", applicationId);
                            applicationRelatedToLeadId = _middlewareClient.SfGetApplicationById(applicationId);
                        }
                        else
                        {
                            Topic.Note("No QB lead id or Sf lead id.  Exiting without screen pop");
                            //Screen pop to search page w/ original phone number would be ideal
                            return;
                        }
                    }
                    else
                    {
                        Topic.Note("Interaction has QB lead id {} and searching for it in Sf", leadId);
                        applicationRelatedToLeadId = _middlewareClient.SfGetApplicationRelatedToQbLead(leadId);
                    }


                    Topic.Note("Found {} applications related to the lead {}", applicationRelatedToLeadId.totalSize, leadId);

                    if (applicationRelatedToLeadId.totalSize <= 0)
                    {
                        SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
                        _synchronizationContext.Send(p => MessageBox.Show("Unable to screen-pop"), null);
                        return;
                    }
                    else
                    {
                        Topic.Note("Searching for an employee with the username {}", Environment.UserName);
                        var employee = SfGetEmployeeByUsername(Environment.UserName);

                        if ((string.IsNullOrEmpty(applicationRelatedToLeadId.records[0].Loan_Officer__c) ||
                            !applicationRelatedToLeadId.records[0].Referral_Owner__c.Equals("Freedom Plus")) && employee.TotalSize > 0)
                        {
                            var employeeToAssign = string.IsNullOrEmpty(employee.Records[0].Id)
                                ? ""
                                : employee.Records[0].Id;

                            _middlewareClient.SfAssignAndUnlockApplication(applicationRelatedToLeadId.records[0].Id, employeeToAssign);
                            Topic.Note("Called SfAssignAndUnlockApplication with the Id: {} and Employee: {}",
                                applicationRelatedToLeadId.records[0].Id, employeeToAssign);
                        }
                        else
                        {
                            Topic.Warning("Either loan officer and referral owner were set or no employee was found!");
                        }
                    }

                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void ExternalInboundCall(IInteraction selectedInteraction)
        {
            using (Topic.Scope())
            {
                try
                {
                    var startTime = DateTime.Now;
                    var interaction = ConvertIInteractionToInteraction(selectedInteraction);
                    SetInteractionAttributeValue(interaction, "FF_StartTime", startTime.ToString(DateTimeFormat));
                    var dnis = TrimPhoneNumber(GetInteractionAttributeValue(interaction, "Eic_SipNumberLocal"));
                    //var dnis = TrimPhoneNumber(GetInteractionAttributeValue(interaction, "Eic_RemoteAddress"));
                    if (!string.IsNullOrEmpty(interaction.GetStringAttribute("FF_FpApplicationId")))
                    {
                        return;
                    }
                    
                    var wholesaler = GetMatchingWholesalerRecord(dnis);
                    if (wholesaler != null)
                    {
                        string emailAddress;
                        do
                        {
                            emailAddress = DisplayEmailEntryForm();
                            if (string.IsNullOrEmpty(emailAddress)) { return; }
                            Topic.Note("The email address is set to {}", emailAddress);
                        } while (string.IsNullOrEmpty(emailAddress));

                        var appByEmail = SfGetApplicationByEmailAddress(emailAddress);
                        if (appByEmail == null || appByEmail.totalSize <= 0)
                        {
                            SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
                            Topic.Warning("No application found. Try pop again manually");
                            _synchronizationContext.Send(p => MessageBox.Show("No application found. Try pop again manually", "No Application Found"), null);
                            return;
                        }
                        if (appByEmail.totalSize > 1)
                        {
                            SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
                            Topic.Warning("More than 1 application found. Try pop again manually");
                            _synchronizationContext.Send(p => MessageBox.Show("More than 1 application found. Try pop again manually", "Too many Applications Found"), null);
                            return;
                        }

                        SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");
                        selectedInteraction.SetAttribute("FF_FpApplicationId", appByEmail.records[0].Id);
                        Topic.Note("Set FF_FpApplicationId to be {}", appByEmail.records[0].Id);

                        var url = _middlewareClient.IVRGetSalesforceInstanceUrl() + "/" + appByEmail.records[0].Id;
                        Topic.Note("Created the URL: {}", url);

                        Topic.Note("Searching for an employee with the username {}", Environment.UserName);
                        var employee = SfGetEmployeeByUsername(Environment.UserName);

                        if ((string.IsNullOrEmpty(appByEmail.records[0].Loan_Officer__c) ||
                            !appByEmail.records[0].Referral_Owner__c.Equals("Freedom Plus")) && employee.TotalSize > 0)
                        {
                            var employeeToAssign = string.IsNullOrEmpty(employee.Records[0].Id)
                                ? ""
                                : employee.Records[0].Id;

                            _middlewareClient.SfAssignAndUnlockApplication(appByEmail.records[0].Id, employeeToAssign);
                            Topic.Note("Called SfAssignAndUnlockApplication with the Id: {} and Employee: {}",
                                appByEmail.records[0].Id, employeeToAssign);
                        }
                        else
                        {
                            Topic.Warning("Either loan officer and referral owner were set or no employee was found!");
                        }

                        Process.Start(url);
                        Topic.Note("Popped the URL: {}", url);
                    }
                    else
                    {
                        var application = _middlewareClient.SfGetApplicationByAni(TrimPhoneNumber(selectedInteraction.GetAttribute(ININ.IceLib.Interactions.InteractionAttributeName.RemoteId)));
                        var url = "";
                        if (application.totalSize.Equals(1))
                        {
                            SetInteractionAttributeValue(interaction, "FF_AutoMatched", "true");
                            var applicationId = application.records[0].Id;
                            selectedInteraction.SetAttribute("FF_FpApplicationId", applicationId);
                            url = _middlewareClient.IVRGetSalesforceInstanceUrl() + "/" + applicationId;
                        }
                        else
                        {
                            SetInteractionAttributeValue(interaction, "FF_AutoMatched", "false");
                            const string applicationId = "";
                            selectedInteraction.SetAttribute("FF_FpApplicationId", applicationId);
                            url = _middlewareClient.IVRGetSalesforceInstanceUrl() +
                                      "/search/SearchResults?searchType=1&sen=0&setLast=1&sbstr=" + TrimPhoneNumber(selectedInteraction.GetAttribute(ININ.IceLib.Interactions.InteractionAttributeName.RemoteId)) +
                                      "&search=+Go%21+";
                        }

                        var employee = SfGetEmployeeByUsername(Environment.UserName);
                        if (application.totalSize > 0 && (string.IsNullOrEmpty(application.records[0].Loan_Officer__c) ||
                             !application.records[0].Referral_Owner__c.Equals("Freedom Plus")) && employee.TotalSize > 0)
                        {
                            var employeeToAssign = string.IsNullOrEmpty(employee.Records[0].Id)
                                ? ""
                                : employee.Records[0].Id;

                            _middlewareClient.SfAssignAndUnlockApplication(application.records[0].Id, employeeToAssign);
                            Topic.Note("Called SfAssignAndUnlockApplication with the Id: {} and Employee: {}",
                                application.records[0].Id, employeeToAssign);
                        }
                        else
                        {
                            Topic.Warning("Either loan officer and referral owner were set or no employee was found!");
                        }

                        Process.Start(url);
                        Topic.Note("Popped the URL: {}", url);
                    }


                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void CreateCallRecord(Interaction interaction)
        {
        }
        #endregion

        #region Middleware Methods
        private void CreateMiddlewareClient()
        {
            try
            {
                var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };
                var clientEndpoint = new EndpointAddress(MiddlewareServiceEndpoint);
                _middlewareClient = new MiddlewareClient(clientBinding, clientEndpoint);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                MessageBox.Show("Middle ware client Failed: " + ex.Message);
            }
        }

        private WholesalersResponse.Wholesaler GetMatchingWholesalerRecord(string dnis)
        {
            var ret = new WholesalersResponse.Wholesaler();
            try
            {
                Topic.Note("Searching for a wholesaler with the DNIS {}", dnis);
                CreateMiddlewareClient();
                ret = _middlewareClient.GetWholesalers().WholesalerList.FirstOrDefault(x => x.Dnis.Equals(dnis));
                if (ret != null)
                {
                    Topic.Note("Wholesaler Name: {}", ret.Name);
                    Topic.Note("Wholesaler DNIS: {}", ret.Dnis);
                    Topic.Note("Wholesaler Transfer Destination: {}", ret.TransferDestination);
                }
                else
                {
                    Topic.Warning("No wholesaler matching the DNIS {} could be found!", dnis);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in wholesale Method: " + ex.Message);
                Topic.Exception(ex);
            }
            return ret;
        }

        private SfGetEmployeeByUsernameResponse SfGetEmployeeByUsername(string username)
        {
            var ret = new SfGetEmployeeByUsernameResponse();
            try
            {
                ret = _middlewareClient.SfGetEmployeeByUsername(username);

                if (ret.Done && ret.TotalSize > 0)
                {
                    Topic.Note("SfGetEmployeeByUsername Id: {}", ret.Records[0].Id);
                }
                else
                {
                    Topic.Warning("No record found for SfGetEmployeeByUsername with the username {}", username);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return ret;
        }

        private SfQueryResponse SfGetApplicationByEmailAddress(string emailAddress)
        {
            var ret = new SfQueryResponse();
            try
            {
                ret = _middlewareClient.SfGetApplicationByEmailAddress(emailAddress);

                if (ret.done && ret.totalSize > 0 && ret.records.Length==1)
                {
                    Topic.Note("SfGetApplicationByEmailAddress Id: {}", ret.records[0].Id);
                    Topic.Note("SfGetApplicationByEmailAddress Loan Officer: {}", ret.records[0].Loan_Officer__c);
                    Topic.Note("SfGetApplicationByEmailAddress Referral Owner: {}", ret.records[0].Referral_Owner__c);
                }
                else if(ret.done && ret.totalSize > 0 && ret.records.Length > 1)
                {
                    Topic.Warning("More than 1 record found for SfGetApplicationByEmailAddress with the email {}", emailAddress);
                }

                else
                {
                    Topic.Warning("No record found for SfGetApplicationByEmailAddress with the email {}", emailAddress);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return ret;
        }
        #endregion

        #region Helper Methods
        private void MonitorInteractionForDisconnect(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                Topic.Note("Start of method MonitorInteractionForDisconnectv");
                try
                {
                    var callDirection = interaction.GetAttribute(InteractionAttributes.Direction);
                    var callType = interaction.GetAttribute(InteractionAttributes.CallType);
                    var callState = interaction.GetAttribute(InteractionAttributes.State);
                    var transferred = interaction.GetAttribute("FF_TransferredCall");

                    Topic.Verbose("Interaction {} (dequeued) Call Direction: {} | Call Type: {} | Call State: {}",
                        interaction.InteractionId, callDirection, callType, callState);

                    /*
                     * terrible awful no good workaround for undocumented CIC change that results in call state being 
                     * unreliable when agent disconnects.
                    */
                    if (!transferred.Equals("true"))
                    {
                        string interactionId = interaction.InteractionId;
                        CrmCdrLogger crmCdrLogIt = new CrmCdrLogger(interactionId, _serviceProvider, _synchronizationContext, MiddlewareServiceEndpoint, UserId, callDirection, _extension);
                        Thread threadCrmCdrLogger = new Thread(new ThreadStart(crmCdrLogIt.CrmCdrLog));
                        threadCrmCdrLogger.Start();
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
                Topic.Note("End of method MonitorInteractionForDisconnect");
            }
        }

        private Interaction ConvertIInteractionToInteraction(IInteraction iinteraction)
        {
            Interaction ret = null;

            using (Topic.Scope())
            {
                try
                {
                    ret = _interactionsManager.CreateInteraction(new InteractionId(iinteraction.InteractionId));
                    Topic.Verbose("Successfully created Interaction {}", ret.InteractionId.Id);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        private string GetInteractionAttributeValue(Interaction interaction, string attributeName)
        {
            var ret = "";

            using (Topic.Scope())
            {
                try
                {
                    ret = interaction.GetStringAttribute(attributeName);
                    Topic.Note("The value of the attribute {} is {}", attributeName, ret);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        private void SetInteractionAttributeValue(Interaction interaction, string attributeName, string attributeValue)
        {
            using (Topic.Scope())
            {
                try
                {
                    interaction.SetStringAttribute(attributeName, attributeValue);
                    Topic.Note("Set the interaction attribute {} to be {}", attributeName, attributeValue);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private static string TrimPhoneNumber(string ani)
        {
            var ret = "";

            using (Topic.Scope())
            {
                try
                {
                    ret = ani.Replace("(", "");
                    ret = ret.Replace(")", "");
                    ret = ret.Replace(" ", "");
                    ret = ret.Replace("-", "");
                    ret = ret.Replace("+", "");
                    ret = ret.Replace("sip:", "");
                    ret = ret.Replace("ip:", "");


                    if (ret.Length > 10)
                    {
                        ret = ret.Substring(1, 10);
                    }

                    Topic.Verbose("Trimmed the phone number: {}", ret);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        private string RemoveForwardSlashFromEnd(string url)
        {
            if (url[url.Length - 1] == '/')
            {
                url = url.Substring(0, url.Length - 1);
            }
            return url;
        }

        private string GetDNISFromAniDnisString(string aniDnisString)
        {
            try
            {
                var split = aniDnisString.Split('#');

                return split[2];
            }
            catch (Exception ex)
            {
                Topic.Error("Error when trying to access the DNIS from the AniDnisString\n"
                    + "Exception: " + ex.Message);
                return String.Empty;
            }
        }

        #endregion

        #region Screen Pop Methods
        private void ScreenPopUrl(string url)
        {
            using (Topic.Scope())
            {
                try
                {
                    Process.Start(url);
                    Topic.Note("Popped the url {}", url);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region Admin Session
        private void CreateAdminSession()
        {
            try
            {
                Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host, AdminUsername);
                _adminSession = new Session
                {
                    AutoReconnectEnabled = true
                };
                _adminSession.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                    new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Creating admin failure: " + ex.Message);
                Topic.Exception(ex);
            }

        }

        private void GetLoggedInUsersExtension()
        {
            using (Topic.Scope())
            {
                try
                {
                    var userConfigurationList = new UserConfigurationList(ConfigurationManager.GetInstance(_adminSession));
                    var querySettings = userConfigurationList.CreateQuerySettings();
                    querySettings.SetPropertiesToRetrieve(new[]
                    {
                        UserConfiguration.Property.Id, UserConfiguration.Property.Extension
                    });
                    querySettings.SetFilterDefinition(
                        new BasicFilterDefinition<UserConfiguration, UserConfiguration.Property>(
                            UserConfiguration.Property.Id, _session.UserId));

                    userConfigurationList.StartCaching(querySettings);

                    var user =
                        userConfigurationList.GetConfigurationList()
                            .FirstOrDefault(x => x.ConfigurationId.Id.Equals(_session.UserId));

                    if (user != null)
                    {
                        _extension = user.Extension.Value;
                        Topic.Status("{}'s extension is '{}'", _session.UserId, _extension);
                    }

                    userConfigurationList.StopCaching();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                    MessageBox.Show("Failed to get User Extension: " + ex.Message);
                }
            }
        }

        private void DisconnectAdminSession()
        {
            try
            {
                _adminSession.Disconnect();
                Topic.Status("Proxy session has been disconnected");
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion

        #region Server Parameter
        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope())
            {
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    serverParameters.StartWatching(_serverParameterName);
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
                    }
                    else
                    {
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Watch Parameter Method Failure: " + ex.Message);
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region String Format

        private string FormatPhoneNumber(string phoneNumber)
        {
            string formattedPhoneNumber = phoneNumber;

            try
            {
                if (formattedPhoneNumber.Length == 10)
                {
                    formattedPhoneNumber = "(" + formattedPhoneNumber;
                    formattedPhoneNumber = formattedPhoneNumber.Substring(0, 4) + ") " + formattedPhoneNumber.Substring(4);
                    formattedPhoneNumber = formattedPhoneNumber.Substring(0, 9) + "-" + formattedPhoneNumber.Substring(9);
                }
            }
            catch (Exception ex)
            {
                Topic.Note("There was an error when formatting the phone number, returning the original phone number: " + phoneNumber
                    + "\nException: " + ex.Message);
                return phoneNumber;
            }
            return formattedPhoneNumber;
        }

        #endregion
    }
}