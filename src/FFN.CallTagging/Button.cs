﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using FFN.CallTagging.MiddlewareService;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.InteractionClient.Interactions;

namespace FFN.CallTagging
{
    internal class Button : IInteractionButton
    {
        #region IInteraction Button Attributes

        public Icon Icon
        {
            get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); }
        }

        public string Id
        {
            get { return "CallTaggingAddIn"; }
        }

        public SupportedInteractionTypes SupportedInteractionTypes
        {
            get { return SupportedInteractionTypes.Call; }
        }

        public string Text
        {
            get { return "Tag Call"; }
        }

        public string ToolTipText
        {
            get { return "Tag a call with specific fields for reporting purposes"; }
        }

        #endregion

        #region Tracing

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.CallTagging.Button");

        private static readonly ITopicTracerContextAttribute<string> InteractionIdAttribute =
            TopicTracerContextAttributeFactory.CreateStringContextAttribute("FFN.CallTagging.InteractionId",
                "Interaction ID", "{}");

        #endregion

        private readonly List<string> _callTypes = new List<string>();
        private readonly SynchronizationContext _context;
        private MiddlewareClient _client;

        public Button(Addin addin)
        {
            using (Topic.Scope("FFN.CallTagging.Button.Button(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.CallTagging.Button.Button\nParameter\n\taddin: " + addin.ToString());
                    _context = SynchronizationContext.Current;
                    Topic.Note("In FFN.CallTagging.Button.Button\nVariable Assigned\n\t_context: " + _context.ToString());
                    CreateMiddlewareClient(addin.MiddlewareServiceEndpoint);
                    Topic.Note(
                        "In FFN.CallTagging.Button.Button\nExecuted CreateMiddlewareClient\nParameter\n\taddin.MiddlewareServiceEndpoint: " +
                        addin.MiddlewareServiceEndpoint.ToString());
                    _callTypes.AddRange(GetCallTypesFromMiddleware());
                    Topic.Note(
                        "In FFN.CallTagging.Button.Button\nExecuted _callTypes.AddRange\nParameter\n\tGetCallTypesFromMiddleware(): " +
                        GetCallTypesFromMiddleware().ToString());
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.Button.Button:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                    throw;
                }
            }
        }

        public bool CanExecute(IInteraction selectedInteraction)
        {
            using (Topic.Scope("FFN.CallTagging.Button.CanExecute(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.CallTagging.Button.CanExecute\nParameter\n\tselectedInteraction");
                    if (selectedInteraction != null)
                    {
                        Topic.Note("In FFN.CallTagging.Button.CanExecute\nParameter\n\tselectedInteraction: " + selectedInteraction.ToString());
                        Topic.Note("In FFN.CallTagging.Button.CanExecute\n(selectedInteraction != null) is true");
                        return selectedInteraction.InteractionState.Equals(InteractionState.Connected);
                    }
                    else
                    {
                        Topic.Note("In FFN.CallTagging.Button.CanExecute\nParameter\n\tselectedInteraction: null");
                        Topic.Note("In FFN.CallTagging.Button.CanExecute\n(selectedInteraction != null) is false");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.Button.CanExecute:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                    return false;
                }
            }
        }

        public void Execute(IInteraction selectedInteraction)
        {
            using (Topic.Scope("FFN.CallTagging.Button.Execute(..)"))
            {
                Topic.Note("Entering FFN.CallTagging.Button.Execute\nParameter\n\tselectedInteraction: " + selectedInteraction.ToString());
                using (InteractionIdAttribute.Create(selectedInteraction.InteractionId))
                {
                    try
                    {
                       
                        PromptUserForCallTagging(selectedInteraction);
                        Topic.Note("In FFN.CallTagging.Button.Execute\nExecuted PromptUserForCallTagging\nParameter\n\tselectedInteraction: " + selectedInteraction.ToString());
                    }
                    catch (Exception ex)
                    {
                        Exception newEx =
                            new Exception("In FFN.CallTagging.Button.Execute:\nException Occured\nException: " +
                                          ex.Message);
                        Topic.Exception(newEx);
                    }
                }
            }
        }

        #region Middleware Methods
        private void CreateMiddlewareClient(string endpoint)
        {
            using (Topic.Scope("FFN.CallTagging.Button.CreateMiddlewareClient(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.CallTagging.Button.CreateMiddlewareClient\nParameter\n\tendpoint: " + endpoint.ToString());
                    var clientBinding = new BasicHttpBinding
                    {
                        Name = "BasicHttpBinding_IMiddleware"
                    };

                    var clientEndpoint = new EndpointAddress(endpoint);
                    _client = new MiddlewareClient(clientBinding, clientEndpoint);
                    Topic.Note("In FFN.CallTagging.Button.CreateMiddlewareClient\nVariable Assignment:\n\tclientBinding: " + clientBinding.ToString() + "\n\tclientEndpoint: " + clientEndpoint.ToString() + "\n\t_client: " + _client.ToString() );

                    Topic.Status("Successfully created the Middleware Client connection");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.Button.CreateMiddlewareClient:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private IEnumerable<string> GetCallTypesFromMiddleware()
        {
            var ret = new List<string>();

            using (Topic.Scope("FFN.CallTagging.Button.GetCallTypesFromMiddleware(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.CallTagging.Button.GetCallTypesFromMiddleware");
                    ret = _client.GetCallTypesForCallTagging().ToList();
                    Topic.Note("In FFN.CallTagging.Button.GetCallTypesFromMiddleware\nVariable Assignment\n\tret: " + ret.ToString());
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.Button.GetCallTypesFromMiddleware:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }

            return ret;
        }
        #endregion

        #region Display Methods

        private void PromptUserForCallTagging(IInteraction selectedInteraction)
        {
            using (Topic.Scope("FFN.CallTagging.Button.PromptUserForCallTagging(..)"))
            {
                try
                {
                    Topic.Note(
                        "Entering FFN.CallTagging.Button.PromptUserForCallTagging\nParameter:\n\tselectedInteraction: " +
                        selectedInteraction.ToString());
                    var form = new CallTaggingForm
                    {
                        CallTypeList = _callTypes,
                        TopMost = true
                    };

                    Topic.Note(
                        "In FFN.CallTagging.Button.PromptUserForCallTagging\nVariable Assigned:\n\tform: " +
                        form.ToString());
                    _context.Send(p =>
                    {
                        using (form)
                        {
                            Topic.Note(
                                "In FFN.CallTagging.Button.PromptUserForCallTagging\nIn context.Send\nUsing Form");
                            var result = form.ShowDialog();
                            Topic.Note(
                                "In FFN.CallTagging.Button.PromptUserForCallTagging\nIn context.Send\nUsing Form\nVariable Assignment:\n\tresult: " +
                                result.ToString());
                            if (!result.Equals(DialogResult.OK))
                            {
                                Topic.Note(
                                    "In FFN.CallTagging.Button.PromptUserForCallTagging\nIn context.Send\nUsing Form\n(!result.Equals(DialogResult.OK)) is true");
                                return;
                            }

                            selectedInteraction.SetAttribute("FFN_TransactionId", form.SettlementId);
                            Topic.Note(
                                "In FFN.CallTagging.Button.PromptUserForCallTagging\nIn context.Send\nUsing Form\nExecuted \nParameters\n\tParam1: \"FFN_TransactionId\"\n\tform.SettlmentId: " +
                                form.SettlementId.ToString());
                            selectedInteraction.SetAttribute("FFN_ClientId", form.ClientId);
                            Topic.Note(
                                "In FFN.CallTagging.Button.PromptUserForCallTagging\nIn context.Send\nUsing Form\nExecuted \nParameters\n\tParam1: \"FFN_ClientId\"\n\tform.ClientId: " +
                                form.ClientId.ToString());
                            selectedInteraction.SetAttribute("FFN_CallTagCallType", form.CallType);
                            Topic.Note(
                                "In FFN.CallTagging.Button.PromptUserForCallTagging\nIn context.Send\nUsing Form\nExecuted \nParameters\n\tParam1: \"FFN_CallTagCallType\"\n\tform.CallType: " +
                                form.CallType.ToString());
                            Topic.Note(
                                "Successfully set call tagging attributes (eg FFN_CallTagCallType=`" + form.CallType +
                                "`) on interaction {}",
                                selectedInteraction.InteractionId);
                        }
                    }, null);
                    Topic.Note(
                        "In FFN.CallTagging.Button.PromptUserForCallTagging\nExecuted _context.Send");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In FFN.CallTagging.Button.PromptUserForCallTagging:\nException Occured\nException: " +
                            ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        #endregion
    }
}