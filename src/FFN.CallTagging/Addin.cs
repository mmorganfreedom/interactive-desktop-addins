﻿using System;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.InteractionClient;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;
using ServerParameters = ININ.IceLib.Connection.Extensions.ServerParameters;

namespace FFN.CallTagging
{
    public class Addin : IAddIn
    {
        private Session _session;
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.CallTagging.AddIn");

        public void Load(IServiceProvider serviceProvider)
        {
            using (Topic.Scope("FFN.CallTagging.AddIn.Load(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.CallTagging.AddIn.Load:\nParameter:\n\tserviceProvider: " + serviceProvider.ToString());
                    _session = serviceProvider.GetService(typeof(Session)) as Session;
                    Topic.Note("In  FFN.CallTagging.AddIn.Load:\nVariable Assigned:\n\t_session: " + _session.ToString());

                    CreateAdminSession();
                    Topic.Note("In  FFN.CallTagging.AddIn.Load:\nExecuted CreateAdminSession()");
                    WatchServerParameter();
                    Topic.Note("In  FFN.CallTagging.AddIn.Load:\nExecuted WatchServerParameter()");
                    DisconnectAdminSession();
                    Topic.Note("In  FFN.CallTagging.AddIn.Load:\nExecuted DisconnectAdminSession()");

                    var service = ServiceLocator.Current.GetInstance<IClientInteractionButtonService>();
                    Topic.Note("In  FFN.CallTagging.AddIn.Load:\nVariable Assigned:\n\tservice: " + service.ToString());
                    service.Add(new Button(this));
                    Topic.Note("In  FFN.CallTagging.AddIn.Load:\nExecuted service.Add(new Button(this))");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.AddIn.Load:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                    throw;
                }
            }
        }

        public void Unload()
        {
            using (Topic.Scope("FFN.CallTagging.AddIn.Unload(..)"))
            {
                try
                {

                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                    //throw;
                }
            }
        }

        #region Server Parameter
        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope("FFN.CallTagging.AddIn.WatchServerParameter(..)"))
            {
                Topic.Note("Entering FFN.CallTagging.AddIn.WatchServerParameter:");
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    Topic.Note("In FFN.CallTagging.AddIn.WatchServerParameter:\nVariable Assignment\n\tserverParameters: " + serverParameters.ToString());
                    serverParameters.StartWatching(_serverParameterName);
                    Topic.Note(
                        "In FFN.CallTagging.AddIn.WatchServerParameter:\nExecuted serverParameters.StartWatching(_serverParameterName)");
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Note("In FFN.CallTagging.AddIn.WatchServerParameter:\nVariable Assignment\n\tparameterList: " + parameterList.ToString());
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        Topic.Note(
                            "In FFN.CallTagging.AddIn.WatchServerParameter:\n(parameterList.Count.Equals(1) is true");
                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        Topic.Note(
                            "In FFN.CallTagging.AddIn.WatchServerParameter:\n(parameterList.Count.Equals(1) is true\nVariable Assignment:\n\tMiddlewareServiceEndpoint: " + MiddlewareServiceEndpoint.ToString());
                        Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
                    }
                    else
                    {
                        Topic.Note(
                            "In FFN.CallTagging.AddIn.WatchServerParameter:\n(parameterList.Count.Equals(1) is false");
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                    Topic.Note(
                        "In FFN.CallTagging.AddIn.WatchServerParameter:\nExecuted serverParameters.StopWatching()");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.AddIn.WatchServerParameter:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        #endregion

        #region Admin Session
        private Session _adminSession;
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

        private void CreateAdminSession()
        {
            using (Topic.Scope("FFN.CallTagging.AddIn.CreateAdminSession(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.CallTagging.AddIn.CreateAdminSession:");
                    Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host,
                        AdminUsername);
                    _adminSession = new Session
                    {
                        AutoReconnectEnabled = true
                    };
                    Topic.Note("In FFN.CallTagging.AddIn.CreateAdminSession:\nVariables Assigned:\n\t_adminSession: " + _adminSession.ToString() + "\n\t_adminSession.AutoReconnectEnabled: " + _adminSession.AutoReconnectEnabled.ToString());
                    _adminSession.Connect(new SessionSettings(),
                        new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                        new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
                    Topic.Note(
                        "In FFN.CallTagging.AddIn.CreateAdminSession:\nExecuted _adminSession.Connect(new SessionSettings(),new HostSettings(new HostEndpoint(_session.Endpoint.Host)), new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings())");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.AddIn.CreateAdminSession:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }

        }

        private void DisconnectAdminSession()
        {
            using (Topic.Scope("FFN.CallTagging.AddIn.DisconnectAdminSession(..)"))
            {
                try
                {
                    _adminSession.Disconnect();
                    Topic.Note(
                        "In FFN.CallTagging.AddIn.CreateAdminSession:\nExecuted _adminSession.Disconnect()");
                    Topic.Status("Proxy session has been disconnected");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFN.CallTagging.AddIn.DisconnectAdminSession:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        #endregion
    }
}