﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.CallTagging
{
    public partial class CallTaggingForm : Form
    {
        public List<string> CallTypeList { get; set; }
        public string SettlementId { get; private set; }
        public string ClientId { get; private set; }
        public string CallType { get; private set; }

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.CallTagging.Form");

        public CallTaggingForm()
        {
            using (Topic.Scope("FFN.CallTagging.Form.CallTaggingForm(...)"))
            {
                try
                {
                    Topic.Note("In FFN.CallTagging.Form.CallTaggingForm:\nEntering scope.");
                    InitializeComponent();
                    Topic.Note("In FFN.CallTagging.Form.CallTaggingForm:\nExecuted InitializeComponent()");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In FFN.CallTagging.Form.CallTaggingForm:\nException occured\nException: {}" +
                            ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void CallTaggingForm_Load(object sender, EventArgs e)
        {
            using (Topic.Scope("FFN.CallTagging.Form.CallTaggingForm_Load(...)"))
            {
                try
                {
                    Topic.Note("In FFN.CallTagging.Form.CallTaggingForm_Load:\nFound {} call types", CallTypeList.Count);
                    callTypeCombobox.DataSource = CallTypeList;
                    Topic.Note("In FFN.CallTagging.Form.CallTaggingForm_Load:\nVariable Assigned: \n\tcallTypeCombobox.DataSource: {}", callTypeCombobox.DataSource.ToString());
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.CallTagging.Form.CallTaggingForm_Load:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            using (Topic.Scope("FFN.CallTagging.Form.submitButton_Click(..)"))
            {
                try
                {
                    Topic.Verbose("The user has entered {} for the Settlement ID", settlementIdTextbox.Text);
                    Topic.Verbose("The user has entered {} for the Client ID", clientIdTextbox.Text);
                    Topic.Verbose("The user has entered {} for the Call Type", callTypeCombobox.GetItemText(callTypeCombobox.SelectedItem));

                    SettlementId = settlementIdTextbox.Text;
                    ClientId = clientIdTextbox.Text;
                    CallType = callTypeCombobox.GetItemText(callTypeCombobox.SelectedItem);
                    Topic.Note("In FFN.CallTagging.Form.submitButton_Click:\nVariables Assignment:\n\tSettlementId: " + SettlementId.ToString() + "\n\tClientId: " + ClientId.ToString() + "\n\tCallType: " + CallType.ToString());
                    DialogResult = DialogResult.OK;
                    Topic.Note("In FFN.CallTagging.Form.submitButton_Click:\nVariable Assignment:\n\tDialogResult: " + DialogResult.ToString());
                    Close();
                    Topic.Note("In FFN.CallTagging.Form.submitButton_Click:\nExecuted Close()");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.CallTagging.Form.submitButton_Click:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            using (Topic.Scope("FFN.CallTagging.Form.cancelButton_Click(..)"))
            {
                try
                {
                    Topic.Warning("The user has cancelled the call tagging dialog box");

                    DialogResult = DialogResult.Cancel;
                    Topic.Note("In FFN.CallTagging.Form.cancelButton_Click:\nVariable Assignment:\n\tDialogResult: " + DialogResult.ToString());
                    Close();
                    Topic.Note("In FFN.CallTagging.Form.cancelButton_Click:\nExecuted Close()");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.CallTagging.Form.cancelButton_Click:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
    }
}