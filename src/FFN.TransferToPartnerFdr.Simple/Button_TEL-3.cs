﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using FFN.TransferToPartnerFdr.Simple.Forms;
using FFN.TransferToPartnerFdr.Simple.MiddlewareService;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;
using IInteraction = ININ.Client.Common.Interactions.IInteraction;
using Interaction = ININ.IceLib.Interactions.Interaction;
using InteractionState = ININ.Client.Common.Interactions.InteractionState;

namespace FFN.TransferToPartnerFdr.Simple
{
    public class Button : IInteractionButton
    {
        #region IInteractionButton Properties
        public Icon Icon { get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); } }
        public string ToolTipText { get { return "Transfer to Partner FDR"; } }
        public string Text { get { return "XFR to Partner FDR"; } }
        public string Id { get { return "Transfer to Partner FDR Simple"; } }
        public SupportedInteractionTypes SupportedInteractionTypes { get { return SupportedInteractionTypes.Call; } }
        #endregion

        private InteractionsManager InteractionsManager { get; set; }

        private Addin Addin { get; set; }
        private MiddlewareClient _client;
        private readonly SynchronizationContext _context = SynchronizationContext.Current;

        private string UserExtension { get; set; }
        private const string LeadIdAttributeName = "FF_FdrLeadId";

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.Simple.Button");

        public Button(Addin addin)
        {
            try
            {
                Addin = addin;
                InteractionsManager = InteractionsManager.GetInstance(Addin.Session);

                // Get the logged in user's extension
                UserExtension = GetUserExtension(Addin.Session.UserId);
                
                // Creates a client to connect to the middleware
                CreateMiddlewareClient();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }
        }

        public bool CanExecute(IInteraction selectedInteraction)
        {
            // Only allow this button to work when an interaction has been selected and is connected
            return selectedInteraction != null && selectedInteraction.InteractionState.Equals(InteractionState.Connected);
        }

        public void Execute(IInteraction selectedInteraction)
        {
            /*
            try
            {
                var leadId = "3751477";
                var interaction = ConvertIInteractionToInteraction(selectedInteraction);





                //TEL-3 Tyler Style Remove SF record creation and instead fetch SF ID from QB.
                var qbLeadData = QbGetLeadDataRequiredByFreedomPlus(leadId);

                if (!qbLeadData.Records.Any())
                {
                    Topic.Warning("Lead not found for QuickBase Lead ID `" + leadId + "`");
                    _context.Send(p => MessageBox.Show("Lead not found for QuickBase Lead ID `" + leadId + "`", "Lead not found for QuickBase Lead ID `" + leadId + "`"), null);
                    CancelTransfer();
                    return;
                }
                else
                {
                    string sfId = qbLeadData.Records[0].FreedomPlusRecord;

                    if (String.IsNullOrWhiteSpace(sfId))
                    {
                        Topic.Warning("F+ SalesForce ID is missing for QuickBase Lead ID `" + leadId + "`");
                        _context.Send(p => MessageBox.Show("F+ SalesForce ID is blank for QuickBase Lead ID `" + leadId + "`", "F+ SalesForce ID is blank for QuickBase Lead ID `" + leadId + "`"), null);
                        CancelTransfer();
                        return;
                    }
                    else
                    {
                        interaction.SetStringAttribute("FF_FpApplicationId", sfId);
                    }
                }
            }
            catch(Exception ex)
            {
                _context.Send(p => MessageBox.Show(ex.Message), null);
            }

            return;
            */

            try
            {
                var interaction = ConvertIInteractionToInteraction(selectedInteraction);
                var leadId = GetAttributeValue(interaction, LeadIdAttributeName);
                QbGetQualifiedPartnerStatsFromLeadResponse qualifiedPartnerStats;

                if (string.IsNullOrEmpty(leadId))
                {
                    PromptForLeadId(interaction, leadId);
                    leadId = GetAttributeValue(interaction, LeadIdAttributeName);
                    if (leadId.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
                    {
                        interaction.SetStringAttribute(LeadIdAttributeName, "");
                        return;
                    }
                }
                else
                {
                    qualifiedPartnerStats = QbGetQualifiedPartnerStatsFromLead(leadId);
                    if (!qualifiedPartnerStats.Records.Any())
                    {
                        leadId = string.Empty;
                        selectedInteraction.SetAttribute(LeadIdAttributeName, leadId);
                        Execute(selectedInteraction);
                        return;
                    }
                }

                Topic.Note("The Lead Id is currently {}", leadId);
                qualifiedPartnerStats = QbGetQualifiedPartnerStatsFromLead(leadId);
                if (qualifiedPartnerStats == null || string.IsNullOrEmpty(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun))
                {
                    Topic.Note("No qualified partner stats were found OR minutes since qualified last run is empty");
                    Topic.Warning("Qualify Partners never run");
                    _context.Send(p => MessageBox.Show("Qualify Partners never run"), null);
                    return;
                }

                if (Convert.ToDouble(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun) >
                    Convert.ToDouble(qualifiedPartnerStats.Records[0].MinutesSinceLastModification) + 0.1)
                {
                    Topic.Note("Minutes since qualified last run is greater than minutes since last modifications + 0.1");
                    Topic.Warning("Lead modified after Qualified Partners run");
                    _context.Send(p => MessageBox.Show("Lead modified after Qualified Partners run"), null);
                    return;
                }

                if (Convert.ToDouble(qualifiedPartnerStats.Records[0].MinutesSinceQualifiedLastRun) >= 15)
                {
                    Topic.Note("Minutes since qualified last run is greater than or equal to 15");
                    Topic.Warning("Qualified Partners expired");
                    _context.Send(p => MessageBox.Show("Qualified Partners expired"), null);
                    return;
                }

                var qualifiedPartnersRelatedToLead = QbGetQualifiedPartnersRelatedToLead(leadId);
                if (qualifiedPartnersRelatedToLead != null && qualifiedPartnersRelatedToLead.Records.Any())
                {
                    Topic.Note("Found qualified partners related to lead");
                    var partnerClass = DisplayDesiredPartnerClassEntryForm(qualifiedPartnersRelatedToLead);
                    var selectedPartner = qualifiedPartnersRelatedToLead.Records.FirstOrDefault(x => x.PartnerClass.Equals(partnerClass));

                    if (partnerClass.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
                    {
                        CancelTransfer();
                        return;
                    }

                    if (partnerClass.Equals("No Interest", StringComparison.CurrentCultureIgnoreCase) || selectedPartner == null)
                    {
                        Topic.Note("Now creating a transfer record");
                        QbCreateTransferRecord(leadId, string.Empty, string.Empty);
                        QbUpdateLeadWithTransferDisposition(leadId, string.Empty, "Lead is not interested in any of the classifications", "No Transfer Indicated");
                        return;
                    }

                    var transferCode = "Referred to " + partnerClass;

                    Topic.Note("Attempting to find a partner with the Id {}", selectedPartner.PartnerId);
                    var partner = QbGetPartnerById(selectedPartner.PartnerId);
                    if (!partner.Records.Any())
                    {
                        Topic.Warning("Partner record not found");
                        _context.Send(p => MessageBox.Show("Partner record not found"), null);
                        return;
                    }

                    if (partner.Records[0].PartnerName.Equals("Freedom Plus", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Topic.Note("Getting lead data required by FP with the Id {}", leadId);
                        var leadData = QbGetLeadDataRequiredByFreedomPlus(leadId);
                        if (!leadData.Records.Any())
                        {
                            Topic.Warning("Lead not found");
                            _context.Send(p => MessageBox.Show("Lead not found", "Lead not found"), null);
                            CancelTransfer();
                            return;
                        }

                        var dialogResult = DialogResult.Cancel;
                        _context.Send(p =>
                        {
                            Topic.Note("Lead will be transferred to FreedomPlus");
                            dialogResult = MessageBox.Show("Lead will be transferred to FreedomPlus",
                                "Call Transfer: FreedomPlus", MessageBoxButtons.OKCancel);

                        }, null);

                        if (dialogResult.Equals(DialogResult.Cancel))
                        {
                            CancelTransfer();
                            return;
                        }

                        //TEL-3 Tyler Style Remove SF record creation and instead fetch SF ID from QB.
                        var qbLeadData = QbGetLeadDataRequiredByFreedomPlus(leadId);

                        if (!qbLeadData.Records.Any())
                        {
                            Topic.Warning("Lead not found for QuickBase Lead ID `" + leadId + "`");
                            _context.Send(p => MessageBox.Show("Lead not found for QuickBase Lead ID `" + leadId + "`", "Lead not found for QuickBase Lead ID `" + leadId + "`"), null);
                            CancelTransfer();
                            return;
                        }
                        else
                        {
                            string sfId = qbLeadData.Records[0].FreedomPlusRecord;

                            if (String.IsNullOrWhiteSpace(sfId))
                            {
                                Topic.Warning("F+ SalesForce ID is missing for QuickBase Lead ID `" + leadId + "`");
                                _context.Send(p => MessageBox.Show("F+ SalesForce ID is blank for QuickBase Lead ID `" + leadId + "`", "F+ SalesForce ID is blank for QuickBase Lead ID `" + leadId + "`"), null);
                                CancelTransfer();
                                return;
                            }
                            else
                            {
                                interaction.SetStringAttribute("FF_FpApplicationId", sfId);
                            }
                        }
                    }

                    if (partner.Records != null && partner.Records.Any() && partner.Records[0].CurrentlyOpen.Equals(1))
                    {
                        if (partner.Records[0].PartnerName.Equals("Freedom Plus",
                            StringComparison.CurrentCultureIgnoreCase))
                        {
                            var transfer = ConferenceTransfer(interaction, partner.Records[0].PartnerName,
                                partner.Records[0].TransferPhoneNumber);
                            if (transfer.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
                            {
                                return;
                            }
                        }
                        else
                        {
                            var transfer = WarmTransfer(interaction, partner.Records[0].PartnerName,
                                partner.Records[0].TransferPhoneNumber);
                            if (transfer.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
                            {
                                return;
                            }
                        }

                        var failureReason = "";
                        var transferOutcome = PromptUserForTransferOutcome();
                        var transferDisposition = "Success - Open";
                        if (transferOutcome.Equals("failure", StringComparison.InvariantCultureIgnoreCase))
                        {
                            failureReason = PromptUserForFailureReason();
                            if (failureReason.Equals("hold time too long", StringComparison.CurrentCultureIgnoreCase))
                            {
                                transferDisposition = "Transfer partner hold time too long";
                            }
                            else if (failureReason.Equals("partner rejected", StringComparison.CurrentCultureIgnoreCase))
                            {
                                transferDisposition = "Transfer Partner Rejected";
                            }
                            else if (failureReason.Equals("something went wrong", StringComparison.CurrentCultureIgnoreCase))
                            {
                                transferDisposition = "Transfer partner hold time too long";
                            }
                        }

                        CreateCallRecord(interaction, partner.Records[0].TransferPhoneNumber,
                                                        partner.Records[0].PartnerName);
                        CreateTransferRecord(leadId, partner.Records[0].CurrentlyOpen.Equals(1) ? "Open" : "Closed", selectedPartner.PartnerId);
                        QbUpdateLeadWithTransferDisposition(leadId, selectedPartner.PartnerId, transferDisposition, transferCode);
                    }
                    else
                    {
                        DisplayHoursOfOperationForm(partner);
                        CreateTransferRecord(leadId, partner.Records[0].CurrentlyOpen.Equals(1) ? "Open" : "Closed", selectedPartner.PartnerId);
                        QbUpdateLeadWithTransferDisposition(leadId, selectedPartner.PartnerId, "Success - Closed", transferCode);
                    }
                }
                else
                {
                    Topic.Warning("No Qualified Partners found");
                    _context.Send(p => MessageBox.Show("No Qualified Partners found"), null);
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        #region Transfer Methods

        private string WarmTransfer(Interaction interaction, string partnerName, string transferDestination)
        {
            var ret = "ok";
            try
            {
                var userSelection = DialogResult.Cancel;
                _context.Send(
                    p =>
                    {
                        userSelection = MessageBox.Show(
                            "This call will be transferred to " + partnerName + ". Click OK to begin the transfer. Click Cancel to return to the propsect.",
                            "Call Transfer: " + partnerName, MessageBoxButtons.OKCancel);

                    }, null);

                if (userSelection.Equals(DialogResult.OK))
                {
                    Topic.Verbose("Agent has agreed to continue transfer of interaction {}",
                        interaction.InteractionId.Id);

                    var consultInteraction =
                        InteractionsManager.MakeConsultTransfer(
                            new ConsultTransferParameters(transferDestination,
                                interaction.InteractionId));

                    var newInteraction = InteractionsManager.CreateInteraction(consultInteraction.ConsultInteractionId);
                    newInteraction.SetStringAttribute(LeadIdAttributeName, interaction.GetStringAttribute(LeadIdAttributeName));
                    newInteraction.SetStringAttribute("FF_FpApplicationId", interaction.GetStringAttribute("FF_FpApplicationId"));
                    newInteraction.SetStringAttribute("FF_TransferredCall", "true");

                    consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);

                    _context.Send(
                        p =>
                        {
                            userSelection = MessageBox.Show(
                                "Wait for an agent to answer and then announce your transfer. Please provide the parter agent with any requested information. Once the agent is ready, click OK to complete the transfer. Click Cancel to cancel the transfer and return to the propsect",
                                "Call Transfer: " + partnerName, MessageBoxButtons.OKCancel);
                        }, null);

                    if (userSelection.Equals(DialogResult.OK))
                    {
                        consultInteraction.Conclude();
                        _context.Send(p => MessageBox.Show("Transfer completed"), null);
                        Topic.Note("We need to create a call record for this call!");
                    }
                    else
                    {
                        ret = "cancel";
                        interaction.Hold(false);
                        consultInteraction.Cancel();
                        _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                    }
                }
                else
                {
                    ret = "cancel";
                    interaction.Hold(false);
                    Topic.Warning("Agent has cancelled transfer of interaction {}", interaction.InteractionId.Id);
                    _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                }


            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return ret;
        }

        private string ConferenceTransfer(Interaction interaction, string partnerName, string transferDestination)
        {
            using (Topic.Scope())
            {
                string ret = "ok";
                try
                {
                    interaction.SetStringAttribute("FF_TransferredCall", "true");

                    Topic.Verbose("Interaction {} is now on hold", interaction.InteractionId.Id);

                    var consultInteraction = InteractionsManager.MakeConsultTransfer(
                        new ConsultTransferParameters(transferDestination, interaction.InteractionId));

                    var newInteraction = InteractionsManager.CreateInteraction(consultInteraction.ConsultInteractionId);
                    newInteraction.SetStringAttribute(LeadIdAttributeName, interaction.GetStringAttribute(LeadIdAttributeName));
                    newInteraction.SetStringAttribute("FF_FpApplicationId", interaction.GetStringAttribute("FF_FpApplicationId"));
                    newInteraction.SetStringAttribute("FF_TransferredCall", "true");

                    consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);

                    var userSelection = DialogResult.Cancel;
                    _context.Send(
                        p =>
                        {
                            userSelection = MessageBox.Show(
                                "Provide client info to " + partnerName + " agent. When " +
                                partnerName +
                                " agent is ready, click OK to connect the client. Click Cancel to cancel the transfer.",
                                "Call Transfer: " + partnerName,
                                MessageBoxButtons.OKCancel);

                        }, null);

                    if (userSelection.Equals(DialogResult.OK))
                    {
                        consultInteraction.ChangeSpeakers(ConsultTransferParticipants.All);

                        userSelection = DialogResult.Cancel;
                        _context.Send(
                            p =>
                            {
                                userSelection = MessageBox.Show(
                                    "Introduce client and " + partnerName + " agent. When ready, click OK to complete the handoff. To cancel, click Cancel and ask the " +
                                    partnerName +
                                    " agent to hang up.", "Call Transfer: " + partnerName,
                                    MessageBoxButtons.OKCancel);

                            }, null);

                        if (userSelection.Equals(DialogResult.OK))
                        {
                            consultInteraction.Conclude();
                            _context.Send(p => MessageBox.Show(partnerName + " handoff complete."), null);
                            Topic.Status("{} handoff complete", partnerName);
                            CreateCallRecord(interaction, transferDestination, partnerName);
                        }
                        else
                        {
                            ret = "cancel";
                            interaction.Hold(false);
                            consultInteraction.Cancel();
                            _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                            Topic.Status("Transfer cancelled");
                        }
                    }
                    else
                    {
                        ret = "cancel";
                        interaction.Hold(false);
                        consultInteraction.Cancel();
                        Topic.Verbose("Interaction {} is now off hold", interaction.InteractionId.Id);

                        _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                        Topic.Status("Transfer cancelled");
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
                return ret;
            }
        }

        #endregion

        #region Helper Methods
        private Interaction ConvertIInteractionToInteraction(IInteraction interaction)
        {
            Interaction ret = null;

            try
            {
                ret = InteractionsManager.CreateInteraction(new InteractionId(interaction.InteractionId));
                Topic.Verbose("Interaction object created with ID {}", ret.InteractionId.Id);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private void CreateMiddlewareClient()
        {
            try
            {
                // Create a new Middleware Client
                var clientBinding = new BasicHttpBinding
                {
                    Name = "BasicHttpBinding_IMiddleware"
                };

                var clientEndpoint = new EndpointAddress(Addin.MiddlewareServiceEndpoint);
                _client = new MiddlewareClient(clientBinding, clientEndpoint);

            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void CancelTransfer(string callId)
        {
            try
            {
                _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                Topic.Status("The transfer has been cancelled for interaction {}", callId);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private string GetUserExtension(string userId)
        {
            var ret = "";
            try
            {
                var list = new UserConfigurationList(ConfigurationManager.GetInstance(Addin.Session));
                list.StartCaching();
                var userConfig = list.GetConfigurationList().FirstOrDefault(x => x.ConfigurationId.Id.Equals(userId));
                if (userConfig != null)
                {
                    ret = userConfig.Extension.Value;
                }
                list.StopCaching();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private string GetAttributeValue(Interaction interaction, string attributeName)
        {
            var ret = "";

            try
            {
                ret = interaction.GetStringAttribute(attributeName);
                Topic.Note("The attribute {} has the value {}", attributeName, ret);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private string TrimPhoneNumber(string phoneNumber)
        {
            var ret = "";

            using (Topic.Scope())
            {
                try
                {
                    ret = phoneNumber.Replace("(", "");
                    ret = ret.Replace(")", "");
                    ret = ret.Replace(" ", "");
                    ret = ret.Replace("-", "");
                    ret = ret.Replace("+", "");
                    ret = ret.Replace("sip:", "");
                    ret = ret.Replace("ip:", "");

                    if (ret.Length > 10)
                    {
                        ret = ret.Substring(1, 10);
                    }

                    Topic.Verbose("Trimmed the phone number: {}", ret);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }
        #endregion

        #region Quickbase Methods
        private void CreateTransferRecord(string leadId, string openStatus, string partnerId)
        {
            try
            {
                var ret = _client.QbCreateTransferRecord(leadId, openStatus, Environment.UserName, partnerId);
                Topic.Verbose("Error code on running QbCreateTransferRecord: {}", ret.Errcode);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private QbGetPartnerByIdResponse QbGetPartnerById(string partnerId)
        {
            var ret = new QbGetPartnerByIdResponse();
            using (Topic.Scope())
            {
                try
                {
                    ret = _client.QbGetPartnerById(partnerId);

                    if (ret.Records.Any())
                    {
                        Topic.Verbose("QbGetPartnerById Partner Name: {}", ret.Records[0].PartnerName);
                        Topic.Verbose("QbGetPartnerById Monday Open Time: {}", ret.Records[0].MondayOpenTime);
                        Topic.Verbose("QbGetPartnerById Monday Close Time: {}", ret.Records[0].MondayCloseTime);
                        Topic.Verbose("QbGetPartnerById Tuesday Open Time: {}", ret.Records[0].TuesdayOpenTime);
                        Topic.Verbose("QbGetPartnerById Tuesday Close Time: {}", ret.Records[0].TuesdayCloseTime);
                        Topic.Verbose("QbGetPartnerById Wednesday Open Time: {}", ret.Records[0].WednesdayOpenTime);
                        Topic.Verbose("QbGetPartnerById Wednesday Close Time: {}", ret.Records[0].WednesdayCloseTime);
                        Topic.Verbose("QbGetPartnerById Thursday Open Time: {}", ret.Records[0].ThursdayOpenTime);
                        Topic.Verbose("QbGetPartnerById Thursday Close Time: {}", ret.Records[0].ThursdayCloseTime);
                        Topic.Verbose("QbGetPartnerById Friday Open Time: {}", ret.Records[0].FridayOpenTime);
                        Topic.Verbose("QbGetPartnerById Friday Close Time: {}", ret.Records[0].FridayClosetime);
                        Topic.Verbose("QbGetPartnerById Saturday Open Time: {}", ret.Records[0].SaturdayOpenTime);
                        Topic.Verbose("QbGetPartnerById Saturday Close Time: {}", ret.Records[0].SaturdayCloseTime);
                        Topic.Verbose("QbGetPartnerById Sunday Open Time: {}", ret.Records[0].SundayOpenTime);
                        Topic.Verbose("QbGetPartnerById Sunday Close Time: {}", ret.Records[0].SundayCloseTime);
                        Topic.Verbose("QbGetPartnerById Currently Open: {}", ret.Records[0].CurrentlyOpen);
                        Topic.Verbose("QbGetPartnerById FDR Account Code: {}", ret.Records[0].FdrAccountCode);
                        Topic.Verbose("QbGetPartnerById Direct Number: {}", ret.Records[0].DirectNumber);
                    }
                    else
                    {
                        Topic.Status("Did not find a partner with the Id {}", partnerId);
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
            return ret;
        }

        private QbGetLeadDataRequiredByFreedomPlusResponse QbGetLeadDataRequiredByFreedomPlus(string leadId)
        {
            var ret = new QbGetLeadDataRequiredByFreedomPlusResponse();

            using (Topic.Scope())
            {
                try
                {
                    ret = _client.QbGetLeadDataRequiredByFreedomPlus(leadId);


                    if (ret.Records.Any())
                    {
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Client Last: {}", ret.Records[0].ClientLast);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Day Phone: {}", ret.Records[0].DayPhone);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Evening Phone: {}", ret.Records[0].EvePhone);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Cell Phone: {}", ret.Records[0].CellPhone);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Email Address: {}", ret.Records[0].EmailAddress);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Address: {}", ret.Records[0].Address);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus City: {}", ret.Records[0].City);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus State: {}", ret.Records[0].State);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Zip: {}", ret.Records[0].Zip);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Hardship Detail/Notes for Settlemend Department: {}", ret.Records[0].HardshipDetailNotes);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Ever Filed BK: {}", ret.Records[0].EverFiledBk);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus If BK What Year?: {}", ret.Records[0].IfBkWhatYear);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Own or Rent: {}", ret.Records[0].OwnOrRent);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Client First: {}", ret.Records[0].ClientFirst);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Sales Agent: {}", ret.Records[0].SalesAgent);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Unsecured Debt Estimate: {}", ret.Records[0].UnsecuredDebtEstimate);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Best Time to Contact: {}", ret.Records[0].BestTimeToContact);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Lead Phase: {}", ret.Records[0].LeadPhase);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Lead Type: {}", ret.Records[0].LeadType);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Lead Keyword: {}", ret.Records[0].LeadKeywork);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Lead Partner: {}", ret.Records[0].LeadPartner);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Sales Agent Phone: {}", ret.Records[0].SalesAgentPhone);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Campaign: {}", ret.Records[0].Campaign);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Freedom Plus Record: {}", ret.Records[0].FreedomPlusRecord);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Client DOB: {}", ret.Records[0].ClientDobText);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Requested Transfer Option: {}", ret.Records[0].RequestedTransferOption);
                        Topic.Verbose("QbGetLeadDataRequiredByFreedomPlus Past Due Accounts: {}", ret.Records[0].PastDueAccounts);
                    }
                    else
                    {
                        Topic.Status("Did not find any lead data for the lead Id {}", leadId);
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        private QbGetQualifiedPartnerStatsFromLeadResponse QbGetQualifiedPartnerStatsFromLead(string leadId)
        {
            var ret = new QbGetQualifiedPartnerStatsFromLeadResponse();

            using (Topic.Scope())
            {
                try
                {
                    ret = _client.QbGetQualifiedPartnerStatsFromLead(leadId);

                    Topic.Note("Found {} matches for QbGetQualifiedPartnerStatsFromLead with the lead Id of {}", ret.Records.Count(), leadId);
                    if (ret.Records.Any())
                    {
                        Topic.Verbose("QbGetQualifiedPartnerStatsFromLead Minutes Since Last Modification: {}",
                            ret.Records[0].MinutesSinceLastModification);
                        Topic.Verbose("QbGetQualifiedPartnerStatsFromLead Minutes Since Qualified Last Run: {}",
                            ret.Records[0].MinutesSinceQualifiedLastRun);
                    }
                    else
                    {
                        Topic.Status("QbGetQualifiedPartnerStatsFromLead did not find any matches with the lead Id {}",
                            leadId);
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        private QbGetQualifiedPartnersRelatedToLeadResponse QbGetQualifiedPartnersRelatedToLead(string leadId)
        {
            var ret = new QbGetQualifiedPartnersRelatedToLeadResponse();

            using (Topic.Scope())
            {
                try
                {
                    ret = _client.QbGetQualifiedPartnersRelatedToLead(leadId);

                    Topic.Note("Found {} matches for QbGetQualifiedPartnersRelatedToLead with the lead Id of {}", ret.Records.Count(), leadId);
                    if (ret.Records.Any())
                    {
                        Topic.Verbose("QbGetQualifiedPartnersRelatedToLead Partner Id: {}", ret.Records[0].PartnerId);
                        Topic.Verbose("QbGetQualifiedPartnersRelatedToLead Partner Class: {}", ret.Records[0].PartnerClass);
                        Topic.Verbose("QbGetQualifiedPartnersRelatedToLead Minutes Since Created: {}", ret.Records[0].MinutesSinceCreated);
                    }
                    else
                    {
                        Topic.Status("QbGetQualifiedPartnersRelatedToLead did not find any matches with the lead Id {}",
                            leadId);
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            return ret;
        }

        private void QbCreateTransferRecord(string leadId, string openStatus, string partnerRecordId)
        {
            using (Topic.Scope())
            {
                try
                {
                    Topic.Verbose("QbCreateTransferRecord Lead Id: {}", leadId);
                    Topic.Verbose("QbCreateTransferRecord Open Status: {}", openStatus);
                    Topic.Verbose("QbCreateTransferRecord Username: {}", Environment.UserName);
                    Topic.Verbose("QbCreateTransferRecord Partner Record Id: {}", partnerRecordId);

                    _client.QbCreateTransferRecord(leadId,
                        string.IsNullOrEmpty(openStatus) ? "" : openStatus, Environment.UserName,
                        string.IsNullOrEmpty(partnerRecordId) ? "" : partnerRecordId);

                    Topic.Note("Created a new transfer record!");
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void QbUpdateLeadWithTransferDisposition(string leadId, string partnerRecordId, string transferDisposition, string transferCode)
        {
            using (Topic.Scope())
            {
                try
                {
                    Topic.Verbose("QbCreateTransferRecord Lead Id: {}", leadId);
                    Topic.Verbose("QbCreateTransferRecord Partner Record Id: {}", partnerRecordId);
                    Topic.Verbose("QbCreateTransferRecord DateTime Now: {}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    Topic.Verbose("QbCreateTransferRecord Success State: {}", transferDisposition);
                    Topic.Verbose("QbCreateTransferRecord Transfer Classification: {}", transferCode);

                    _client.QbEditLeadToAddTransferDisposition(leadId,
                        string.IsNullOrEmpty(partnerRecordId) ? "" : partnerRecordId, DateTime.Now.ToString(CultureInfo.InvariantCulture),
                        string.IsNullOrEmpty(transferDisposition) ? "" : transferDisposition,
                        string.IsNullOrEmpty(transferCode) ? "" : transferCode);

                    Topic.Note("Created a new transfer record!");
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void QbEditLeadToAddSalesforceApplicationId(string leadId, string applicationId)
        {
            using (Topic.Scope())
            {
                try
                {
                    Topic.Verbose("QbEditLeadToAddSalesforceApplicationId Lead Id: {}", leadId);
                    Topic.Verbose("QbEditLeadToAddSalesforceApplicationId Application Id: {}", applicationId);

                    var response =_client.QbEditLeadToAddSalesforceApplicationId(leadId, applicationId);

                    if (response != null && response.NumberOfFieldsChanged > 0)
                    {
                        Topic.Note("QbEditLeadToAddSalesforceApplicationId was successful! Update Id: {}",
                            response.UpdateId);
                    }
                    else
                    {
                        Topic.Warning("QbEditLeadToAddSalesforceApplicationId was unsuccessful");
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void CreateCallRecord(Interaction interaction, string transferDestination, string transferDestinationName)
        {
            try
            {
                var leadId = interaction.GetStringAttribute("FF_FdrLeadId");
                Topic.Note("The Lead Id is {}", leadId);
                var dnis = TrimPhoneNumber(interaction.GetStringAttribute("Eic_SipNumberLocal"));
                Topic.Note("The DNIS is {}", dnis);
                var ani = TrimPhoneNumber(interaction.GetStringAttribute("Eic_RemoteId"));
                Topic.Note("The ANI is {}", ani);
                var duration = Convert.ToDateTime(GetAttributeValue(interaction, "FF_StartTime")) - DateTime.Now;
                Topic.Note("The duration of the call is {}", duration.Duration().ToString());
                var wrapUpCode = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                Topic.Note("The wrap up code is {}", wrapUpCode);
                var autoMatched = interaction.GetStringAttribute("FF_AutoMatched")
                    .Equals("true", StringComparison.CurrentCultureIgnoreCase);
                Topic.Note("Automatched is set to {}", autoMatched);
                var direction =
                    interaction.GetStringAttribute(InteractionAttributes.Direction)
                        .Equals(InteractionAttributeValues.Direction.Incoming)
                        ? "IB"
                        : "OB";
                Topic.Note("The call direction is {}", direction);

                if (leadId.Length < 7)
                {
                    autoMatched = false;
                    interaction.SetStringAttribute("FF_AutoMatched", "false");
                    Topic.Note("Set automatched to false");
                    var form = new LeadEntryForm(dnis, ani);
                    _context.Send(p =>
                    {
                        var ret = form.ShowDialog();
                        if (ret.Equals(DialogResult.OK))
                        {
                            leadId = form.LeadIdValue;
                        }
                    }, null);
                }

                _client.QbCreateCallRecord(leadId, Environment.UserName, dnis, ani, direction, duration.Duration().ToString(),
                    wrapUpCode, transferDestination, transferDestinationName, autoMatched, false);
                Topic.Note("Created a call record");
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion

        #region Salesforce Methods
        private SfQueryResponse SfQueryForApplicationAssociatedWithQbLead(string leadId)
        {
            var ret = new SfQueryResponse();
            using (Topic.Scope())
            {
                try
                {
                    ret = _client.SfGetApplicationRelatedToQbLead(leadId);
                    Topic.Note("SfGetApplicationRelatedToQbLead Lead Id: {}", leadId);

                    if (ret.totalSize > 0)
                    {
                        Topic.Note("SfGetApplicationRelatedToQbLead Id: {}", ret.records[0].Id);
                        Topic.Note("SfGetApplicationRelatedToQbLead Loan Officer Source: {}", ret.records[0].Loan_Officer_Source__c);
                        Topic.Note("SfGetApplicationRelatedToQbLead Loan Officer: {}", ret.records[0].Loan_Officer__c);
                        Topic.Note("SfGetApplicationRelatedToQbLead UTM Source: {}", ret.records[0].utm_source__c);
                    }
                    else
                    {
                        Topic.Note("No records were found for SfGetApplicationRelatedToQbLead");
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
            return ret;
        }

        private SfCreateResponse SfCreateApplicationRecord(string lastName, string firstName, string dayPhone,
            string eveningPhone, string otherPhone, string otherPhoneType, string ownOrRent, string filedBk, string dob,
            string emailAddress, string hardshipDetail, string pastDueAmount, string unsecuredDebtEstimate,
            string ifBkWhatYear, string bestCallbackTime, string referralId, string address, string city, string state,
            string zip, string loanOfficerSource, string channel, string fdrName, string fdrExtension, string leadPhrase,
            string leadCampaign, string leadKeyword, string leadType, string leadPartner, string referralOwner)
        {
            var ret = new SfCreateResponse();
            using (Topic.Scope())
            {
                try
                {
                    Topic.Verbose("SfCreateNewApplication Last Name: {}", lastName);
                    Topic.Verbose("SfCreateNewApplication First Name: {}", firstName);
                    Topic.Verbose("SfCreateNewApplication Day Phone: {}", dayPhone);
                    Topic.Verbose("SfCreateNewApplication Eve Phone: {}", eveningPhone);
                    Topic.Verbose("SfCreateNewApplication Other Phone: {}", otherPhone);
                    Topic.Verbose("SfCreateNewApplication Own Or Rent: {}", ownOrRent);
                    Topic.Verbose("SfCreateNewApplication FiledBk: {}", filedBk);
                    Topic.Verbose("SfCreateNewApplication DOB: {}", dob);
                    Topic.Verbose("SfCreateNewApplication Email Address: {}", emailAddress);
                    Topic.Verbose("SfCreateNewApplication Hardship Detail: {}", hardshipDetail);
                    Topic.Verbose("SfCreateNewApplication Past Due Amount: {}", pastDueAmount);
                    Topic.Verbose("SfCreateNewApplication Unsecured Debt Estimate: {}", unsecuredDebtEstimate);
                    Topic.Verbose("SfCreateNewApplication If BK What Year: {}", ifBkWhatYear);
                    Topic.Verbose("SfCreateNewApplication Best Callback Time: {}", bestCallbackTime);
                    Topic.Verbose("SfCreateNewApplication Referral Id: {}", referralId);
                    Topic.Verbose("SfCreateNewApplication Address: {}", address);
                    Topic.Verbose("SfCreateNewApplication City: {}", city);
                    Topic.Verbose("SfCreateNewApplication State: {}", state);
                    Topic.Verbose("SfCreateNewApplication Zip: {}", zip);
                    Topic.Verbose("SfCreateNewApplication Loan Officer Source: {}", loanOfficerSource);
                    Topic.Verbose("SfCreateNewApplication Channel: {}", channel);
                    Topic.Verbose("SfCreateNewApplication FDR Name: {}", fdrName);
                    Topic.Verbose("SfCreateNewApplication FDR Extension: {}", fdrExtension);
                    Topic.Verbose("SfCreateNewApplication Lead Phrase: {}", leadPhrase);
                    Topic.Verbose("SfCreateNewApplication Lead Campaign: {}", leadCampaign);
                    Topic.Verbose("SfCreateNewApplication Lead Keyword: {}", leadKeyword);
                    Topic.Verbose("SfCreateNewApplication Lead Type: {}", leadType);
                    Topic.Verbose("SfCreateNewApplication Lead Partner: {}", leadPartner);
                    Topic.Verbose("SfCreateNewApplication Referral Owner: {}", referralOwner);

                    ret = _client.SfCreateNewApplication(lastName, firstName, dayPhone, eveningPhone, otherPhone,
                        otherPhoneType, ownOrRent, filedBk, dob, emailAddress, hardshipDetail, pastDueAmount,
                        unsecuredDebtEstimate, ifBkWhatYear, bestCallbackTime, referralId, address, city, state, zip,
                        loanOfficerSource, channel, fdrName, fdrExtension, leadPhrase, leadCampaign, leadKeyword,
                        leadType, leadPartner, referralOwner);

                    if (ret != null && ret.success)
                    {
                        Topic.Verbose("SfCreateNewApplication Success: {}", ret.success);
                        Topic.Verbose("SfCreateNewApplication Id: {}", ret.id);
                        Topic.Note("Successfully created a new Salesforce Application");
                    }
                    else
                    {
                        Topic.Warning("Could not create a new Salesforce Application!");
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
            return ret;
        }
        #endregion

        #region Display Methods
        private void CancelTransfer()
        {
            try
            {
                Topic.Warning("Transfer Cancelled");
                _context.Send(p => MessageBox.Show("Transfer Cancelled", "Transfer Cancelled"), null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void DisplayAccountIdEntryForm(Interaction interaction)
        {
            try
            {

                var ani = TrimPhoneNumber(interaction.GetStringAttribute(InteractionAttributes.RemoteId));
                var dnis = TrimPhoneNumber(interaction.GetStringAttribute("Eic_SipNumberLocal"));
                var form = new LeadEntryForm(dnis, ani);

                _context.Send(p =>
                {
                    using (form)
                    {
                        var result = form.ShowDialog();
                        if (result.Equals(DialogResult.OK))
                        {
                            Topic.Note("Setting Lead Id to be {}", form.LeadIdValue);
                            interaction.SetStringAttribute(LeadIdAttributeName, form.LeadIdValue);
                        }
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private string PromptUserForTransferOutcome()
        {
            var ret = "";

            try
            {
                var form = new TransferOutcomeEntryForm();

                _context.Send(p =>
                {
                    using (form)
                    {
                        var result = form.ShowDialog();
                        if (result.Equals(DialogResult.OK))
                        {
                            ret = form.TransferOutcome;
                            Topic.Note("The selected transfer outcome is {}", ret);
                        }
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private string PromptUserForFailureReason()
        {
            var ret = "";

            try
            {
                var form = new FailureClassificationForm();

                _context.Send(p =>
                {
                    using (form)
                    {
                        var result = form.ShowDialog();
                        if (result.Equals(DialogResult.OK))
                        {
                            ret = form.FailureReason;
                            Topic.Note("The selected failure reason is {}", ret);
                        }
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private string DisplayDesiredPartnerClassEntryForm(QbGetQualifiedPartnersRelatedToLeadResponse record)
        {
            var ret = "";

            try
            {
                var form = new PartnerClassForm(record);

                _context.Send(p =>
                {
                    var result = form.ShowDialog();
                    if (result.Equals(DialogResult.OK))
                    {
                        ret = form.PartnerClass;
                        Topic.Note("The desired partner class is {}", ret);
                    }
                    else
                    {
                        ret = "cancel";
                        Topic.Warning("The user cancelled the partner class form");
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private void DisplayHoursOfOperationForm(QbGetPartnerByIdResponse record)
        {
            try
            {
                var dictionary = CreateHoursOfOperationDictionary(record);
                var form = new HoursOfOperationForm(dictionary,
                    string.IsNullOrEmpty(record.Records[0].PartnerName) ? "" : record.Records[0].PartnerName,
                    string.IsNullOrEmpty(record.Records[0].DirectNumber) ? "" : record.Records[0].DirectNumber);
                _context.Send(p => form.ShowDialog(), null);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private Dictionary<string, string[]> CreateHoursOfOperationDictionary(QbGetPartnerByIdResponse partnerRecord)
        {
            var ret = new Dictionary<string, string[]>();

            try
            {
                ret.Add("Monday", new[] { partnerRecord.Records[0].Monday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].MondayOpenTime, partnerRecord.Records[0].MondayCloseTime });
                Topic.Note("Monday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Monday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].MondayOpenTime, partnerRecord.Records[0].MondayCloseTime);
                ret.Add("Tuesday", new[] { partnerRecord.Records[0].Tuesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].TuesdayOpenTime, partnerRecord.Records[0].TuesdayCloseTime });
                Topic.Note("Tuesday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Tuesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].TuesdayOpenTime, partnerRecord.Records[0].TuesdayCloseTime);
                ret.Add("Wednesday", new[] { partnerRecord.Records[0].Wednesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].WednesdayOpenTime, partnerRecord.Records[0].WednesdayCloseTime });
                Topic.Note("Wednesday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Wednesday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].WednesdayOpenTime, partnerRecord.Records[0].WednesdayCloseTime);
                ret.Add("Thursday", new[] { partnerRecord.Records[0].Thursday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].ThursdayOpenTime, partnerRecord.Records[0].ThursdayCloseTime });
                Topic.Note("Thursday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Thursday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].ThursdayOpenTime, partnerRecord.Records[0].ThursdayCloseTime);
                ret.Add("Friday", new[] { partnerRecord.Records[0].Friday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].FridayOpenTime, partnerRecord.Records[0].FridayClosetime });
                Topic.Note("Friday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Friday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].FridayOpenTime, partnerRecord.Records[0].FridayClosetime);
                ret.Add("Saturday", new[] { partnerRecord.Records[0].Saturday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SaturdayOpenTime, partnerRecord.Records[0].SaturdayCloseTime });
                Topic.Note("Saturday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Saturday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SaturdayOpenTime, partnerRecord.Records[0].SaturdayCloseTime);
                ret.Add("Sunday", new[] { partnerRecord.Records[0].Sunday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SundayOpenTime, partnerRecord.Records[0].SundayCloseTime });
                Topic.Note("Sunday: Is Open {}, Open Time: {}, Close Time: {}", partnerRecord.Records[0].Sunday.ToString(CultureInfo.InvariantCulture), partnerRecord.Records[0].SundayOpenTime, partnerRecord.Records[0].SundayCloseTime);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private void PromptForLeadId(Interaction interaction, string leadId)
        {
            using (Topic.Scope())
            {
                try
                {
                    while (string.IsNullOrEmpty(leadId))
                    {
                        DisplayAccountIdEntryForm(interaction);
                        leadId = GetAttributeValue(interaction, LeadIdAttributeName);
                        if (leadId.Equals("cancel", StringComparison.CurrentCultureIgnoreCase) || string.IsNullOrEmpty(leadId))
                        {
                            CancelTransfer(interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture));
                            return;
                        }

                        var qualifiedPartnerStats = _client.QbGetQualifiedPartnerStatsFromLead(leadId);
                        if (qualifiedPartnerStats.Records.Any()) { break; }
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region String Formatting
        private static string FormatPhoneNumber(string phoneNumber)
        {
            string formattedPhoneNumber = phoneNumber;

            try
            {
                if (formattedPhoneNumber.Length == 10)
                {
                    formattedPhoneNumber = "(" + formattedPhoneNumber;
                    formattedPhoneNumber = formattedPhoneNumber.Substring(0, 4) + ") " + formattedPhoneNumber.Substring(4);
                    formattedPhoneNumber = formattedPhoneNumber.Substring(0, 9) + "-" + formattedPhoneNumber.Substring(9);
                }
            }
            catch (Exception ex)
            {
                Topic.Note("There was an error when formatting the phone number, returning the original phone number: {}.", phoneNumber);
                return phoneNumber;
            }
            return formattedPhoneNumber;
        }

        private static string FormatDate(string date)
        {
            var formattedDate = date;

            try
            {
                if (formattedDate.IndexOf("-", StringComparison.InvariantCultureIgnoreCase) == 2)
                {
                    var month = formattedDate.Substring(0, 2);
                    var day = formattedDate.Substring(3, 2);
                    var year = formattedDate.Substring(6, 4);

                    formattedDate = year + "-" + month + "-" + day;
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return formattedDate;
        }
        #endregion
    }
}
