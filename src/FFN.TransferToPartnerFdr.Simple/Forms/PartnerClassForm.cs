﻿using System;
using System.Linq;
using System.Windows.Forms;
using FFN.TransferToPartnerFdr.Simple.MiddlewareService;

namespace FFN.TransferToPartnerFdr.Simple.Forms
{
    public partial class PartnerClassForm : Form
    {
        public string PartnerClass { get; set; }

        public PartnerClassForm(QbGetQualifiedPartnersRelatedToLeadResponse record)
        {
            InitializeComponent();
            var classes = record.Records.Select(x => x.PartnerClass);
            foreach (var item in classes)
            {
                partnerClassCombobox.Items.Add(item);
            }

            partnerClassCombobox.Items.Add("No Interest");
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            PartnerClass = partnerClassCombobox.GetItemText(partnerClassCombobox.SelectedItem);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
