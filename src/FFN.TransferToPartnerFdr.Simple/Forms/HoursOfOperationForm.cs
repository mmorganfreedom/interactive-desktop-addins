﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.TransferToPartnerFdr.Simple.Forms
{
    public partial class HoursOfOperationForm : Form
    {
        public string PartnerName { get; set; }
        public string PartnerDirectNumber { get; set; }

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.Simple.HoursOfOperationForm");

        public HoursOfOperationForm(Dictionary<string, string[]> hoursOfOperation, string partnerName, string directNumber)
        {
            try
            {
                InitializeComponent();
                InitializeGridView(hoursOfOperation);
                label1.Text = partnerName + " is closed. the direct number is " + directNumber +
                              ". The hours of operation are:";
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
                throw;
            }

        }

        private void InitializeGridView(Dictionary<string, string[]> hoursOfOperation)
        {
            try
            {
                foreach (var day in hoursOfOperation)
                {
                    dataGridView1.Rows.Add(day.Key,
                                day.Value[0].Equals("0") ? "Closed" : ConvertMillisecondsToTime(day.Value[1]),
                                day.Value[0].Equals("0") ? "Closed" : ConvertMillisecondsToTime(day.Value[2]));
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

        }

        private string ConvertMillisecondsToTime(string milliseconds)
        {
            var ret = "";
            try
            {
                var span = TimeSpan.FromMilliseconds(Convert.ToDouble(milliseconds));
                var time = DateTime.Today.Add(span);
                var displayTime = time.ToString("hh:mm:ss tt");

                ret = displayTime;
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            return ret;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
