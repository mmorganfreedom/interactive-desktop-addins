﻿using System;
using System.Windows.Forms;

namespace FFN.TransferToPartnerFdr.Simple.Forms
{
    public partial class LeadEntryForm : Form
    {
        public string Ani { get; set; }
        public string Dnis { get; set; }
        public string LeadIdValue { get; private set; }

        public LeadEntryForm(string dnis, string ani)
        {
            InitializeComponent();
            aniLabel.Text = ani;
            dnisLabel.Text = dnis;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            LeadIdValue = leadIdTextbox.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
