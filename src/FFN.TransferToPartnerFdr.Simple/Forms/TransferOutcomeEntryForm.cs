﻿using System;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.TransferToPartnerFdr.Simple.Forms
{
    public partial class TransferOutcomeEntryForm : Form
    {
        // Public properties
        public string TransferOutcome { get; private set; }

        // Topic for client tracing
        private static readonly ITopicTracer Topic =
            TopicTracerFactory.CreateTopicTracer("FFN.TransferToPartnerFDR.Simple.TransferOutcomeEntryForm");

        public TransferOutcomeEntryForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                TransferOutcome = transferOutcomeComboBox.GetItemText(transferOutcomeComboBox.SelectedItem);
                Topic.Note("Transfer outcome: {}", TransferOutcome);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void transferOutcomeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (transferOutcomeComboBox.SelectedIndex > -1)
                {
                    submitButton.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
    }
}
