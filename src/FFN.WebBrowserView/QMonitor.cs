﻿///<summary>
///	Inject SMS interaction data into a web page in an Interactive Desktop view.
///</summary>
///<para>
///SMS Outbound filter:  Eic_InteractionType = SMSObject
///SMS Inbound filter:  Eic_InteractionMediaSubtype = SMS
/// </para>
///<jira><see cref="https://billsdev.atlassian.net/browse/CCP-144"/></jira>
///<author>Tyler Style &lt;tstyle@bills.com&gt;</author>

using ININ.InteractionClient.AddIn;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WebBrowserView
{
	public class QMonitor : QueueMonitor
	{
		#region Constants
		const string URL_INTERACTIONS_REPORT = "http://ccps.freedomdebtrelief.com/reporting/InteractionDetails/report/agentIds";

		const string ATTR_EIC_STATE_NAME = "Eic_State";
		const string ATTR_EIC_SMS_RESULTS_NAME = "Eic_SMS_Results";
		const string ATTR_EIC_INTERACTION_TYPE_NAME = "Eic_InteractionType";
		const string ATTR_EIC_INTERACTION_MEDIA_SUBTYPE_NAME = "Eic_InteractionMediaSubtype";
		const string ATTR_EIC_CALL_ID_KEY = "Eic_CallIdKey";

		const string ATTR_EIC_INTERACTION_TYPE_VALUE_SMS_OBJECT = "SMSObject";
		const string ATTR_EIC_INTERACTION_MEDIA_SUBTYPE_VALUE_SMS = "SMS";
		#endregion



		#region Members
		private ITraceContext traceContext;

		protected List<string> interactionsOutboundSentToBrowserViewReport = new List<string>();
		protected List<string> interactionsInboundSentToBrowserViewReport = new List<string>();
		protected string userId;
		#endregion



		#region Enumeration
		/// <summary>
		/// Let the interaction client know what attributes we want to retrieve the value of
		/// </summary>
		protected override IEnumerable<string> Attributes {
			get {
				return new string[] { QMonitor.ATTR_EIC_STATE_NAME
															, QMonitor.ATTR_EIC_SMS_RESULTS_NAME
															, QMonitor.ATTR_EIC_INTERACTION_TYPE_NAME
															, QMonitor.ATTR_EIC_INTERACTION_MEDIA_SUBTYPE_NAME
				};
			}
		}

		protected override IEnumerable<string> SupportingAttributes {
			get {
				return new string[] { InteractionAttributes.InteractionType
															, InteractionAttributes.RemoteName
															, InteractionAttributes.RemoteId
															, QMonitor.ATTR_EIC_CALL_ID_KEY 
				};
			}
		}
		#endregion



		#region QueueMonitor Overrides
		protected override void OnLoad(IServiceProvider serviceProvider) {
			base.OnLoad(serviceProvider);

			this.traceContext = (ITraceContext)serviceProvider.GetService(typeof(ITraceContext));
			this.traceContext.Note("WebBrowserView queue monitor loaded.");

			try {
				this.userId = Environment.UserName;
				string urlReport = URL_INTERACTIONS_REPORT + "/" + this.userId + "/sms";

				WebBrowserWidget.startingUrl = urlReport;
			}
			catch (Exception e) {
				MessageBox.Show("Error on loading webbrowser view queue monitor:  " + e);
			}

			base.StartMonitoring();
		}

		protected override void InteractionAdded(IInteraction interaction) {
			base.InteractionAdded(interaction);
		}

		protected override void InteractionChanged(IInteraction interaction) {
			base.InteractionAdded(interaction);

			try {
				string interactionId = interaction.InteractionId;
				string interactionIdKey = interaction.GetAttribute(QMonitor.ATTR_EIC_CALL_ID_KEY);
				string interactionType = interaction.GetAttribute(InteractionAttributes.InteractionType);
				string callState = interaction.GetAttribute(InteractionAttributes.State);
				string eicSmsResults = interaction.GetAttribute(QMonitor.ATTR_EIC_SMS_RESULTS_NAME);
				string eicInteractionType = interaction.GetAttribute(QMonitor.ATTR_EIC_INTERACTION_TYPE_NAME);
				string eicInteractionMediaSubtype = interaction.GetAttribute(QMonitor.ATTR_EIC_INTERACTION_MEDIA_SUBTYPE_NAME);
				
				if (callState == "I"	//Disconnect
						&&
					  (eicInteractionMediaSubtype == QMonitor.ATTR_EIC_INTERACTION_MEDIA_SUBTYPE_VALUE_SMS	//Outbound SMS
				    || eicInteractionType == QMonitor.ATTR_EIC_INTERACTION_TYPE_VALUE_SMS_OBJECT          //Inbound SMS
				    )
					) {
					if (this.interactionsInboundSentToBrowserViewReport.Contains(interactionId)) {
						this.interactionsInboundSentToBrowserViewReport.Remove(interactionId);
					}
					else if (this.interactionsOutboundSentToBrowserViewReport.Contains(interactionId)) {
						this.interactionsOutboundSentToBrowserViewReport.Remove(interactionId);
					}
					else {
						var webBrowserWidget = WebBrowserWidget.Instance;

						if (webBrowserWidget != null) {
							this.traceContext.Note("Adding CDR to web browser report for interaction: " + interactionIdKey);
							webBrowserWidget.InvokeAddRecordCollectionById(interactionIdKey);
						}
					}
				}
			}
			catch (Exception e) {
				MessageBox.Show("Error on interaction removed (webbrowser view queue monitor):  " + e);
			}
		}

		protected override void InteractionRemoved(IInteraction interaction) {
			base.InteractionRemoved(interaction);
		}
		#endregion
	}
}
