﻿using System.Windows.Forms;

namespace WebBrowserView
{
	public partial class WebBrowserWidget : UserControl
	{
		private static WebBrowserWidget s_controlInstance = null;
		public static string startingUrl { get; set; } = "";

		public WebBrowserWidget() {
			InitializeComponent();
			s_controlInstance = this;
			SetButtonStates();
			this.NavigateToUrl(WebBrowserWidget.startingUrl);
		}

		public void NavigateToUrl(string url) {
			webBrowser.Navigate(url);
		}

		public void DisplayText(string text) {
			webBrowser.DocumentText = text;
			SetButtonStates();
		}

		//see:  https://msdn.microsoft.com/en-us/library/a1hetckb%28v=vs.110%29.aspx
		//see:  https://stackoverflow.com/questions/5268281/c-sharp-system-invalidcastexception
		public delegate string InvokeJavascript(string interactionIdKey);
		
		public string InvokeAddRecordCollectionById(string interactionIdKey) {
			if (InvokeRequired)
				return Invoke(new InvokeJavascript(InvokeAddRecordCollectionById), new object[] { interactionIdKey }) as string;
			else {
				object[] invocationArgs = { };
				string invokeAddRecordCollectionById = "";

				try {
					if (this.webBrowser.ReadyState != WebBrowserReadyState.Complete) {
						invokeAddRecordCollectionById = "Web browser view page not yet loaded.";
					}
					else { 
						var webBrowserDocument = this.webBrowser.Document;
						var invocationResponse = webBrowserDocument.InvokeScript("addRecordCollectionById", new object[] { interactionIdKey });

						invokeAddRecordCollectionById = invocationResponse.ToString();
					}
				}
				catch (System.Exception e) {
					MessageBox.Show("Error in webbrowser invoking script: " + e);
					invokeAddRecordCollectionById = e.Message.ToString();
				}

				return invokeAddRecordCollectionById;
			}
		}



		/// <summary>
		/// We will use a static instance of this control so that we can 
		/// navigate to a new page from the queue monitor.
		/// </summary>
		public static WebBrowserWidget Instance {
			get {
				return s_controlInstance;
			}
		}

		#region UI Browsing Controls Event Handlers
		private void SetButtonStates() {
			btnBack.Enabled = webBrowser.CanGoBack;
			btnForward.Enabled = webBrowser.CanGoForward;
			btnReload.Visible = !webBrowser.IsBusy;
			btnStop.Visible = webBrowser.IsBusy;
		}

		private void OnBackClick(object sender, System.EventArgs e) {
			webBrowser.GoBack();
		}

		private void OnForwardClick(object sender, System.EventArgs e) {
			webBrowser.GoForward();
		}

		private void OnStopClick(object sender, System.EventArgs e) {
			webBrowser.Stop();
		}

		private void OnReloadClick(object sender, System.EventArgs e) {
			webBrowser.Refresh();
		}

		private void OnUrlKeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				webBrowser.Navigate(txtUrl.Text);
			}
		}
		#endregion

		private void OnWebBrowserNavigating(object sender, WebBrowserNavigatingEventArgs e) {
			SetButtonStates();
		}

		private void OnWebBrowserDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
			txtUrl.Text = webBrowser.Url.ToString();
			SetButtonStates();
		}

		private void OnWebBrowserNavigated(object sender, WebBrowserNavigatedEventArgs e) {
			SetButtonStates();
		}
	}
}
