﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ININ.Diagnostics;

namespace FFN.NonSalesTransfer
{
    public partial class TransferTypeEntryForm : Form
    {
        public IEnumerable<string> TransferTypeNames { get; set; }
        public string SelectedTransferType { get; private set; }

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.NonSalesTransfer.TransferTypeEntryForm");


        public TransferTypeEntryForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedTransferType = transferTypeComboBox.Text;
                Topic.Note("{} selected as the transfer type", SelectedTransferType);
                Topic.Verbose("Closing the Transfer Type Entry form now");

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void TransferTypeEntryForm_Load(object sender, EventArgs e)
        {
            transferTypeComboBox.DataSource = TransferTypeNames;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                Topic.Status("The agent has cancelled the transfer");
                DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
            
        }

        private void transferTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
