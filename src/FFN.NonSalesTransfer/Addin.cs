﻿using System;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.InteractionClient;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Interactions;

namespace FFN.NonSalesTransfer
{
    public class Addin : IAddIn
    {
        public Session Session { get; private set; }
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.NonSalesTransfer.AddIn");

        public void Load(IServiceProvider serviceProvider)
        {
            using (Topic.Scope("FFN.NonSalesTransfer.AddIn.Load(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.AddIn.Load\nParameter\n\tserviceProvider: " + serviceProvider.ToString());
                    Session = serviceProvider.GetService(typeof(Session)) as Session;
                    var service = ServiceLocator.Current.GetInstance<IClientInteractionButtonService>();
                    Topic.Note("In FFN.NonSalesTransfer.AddIn.Load\nVariables Assigned:\n\tSession: " + Session.ToString() + "\n\tservice: " + service.ToString());
                    service.Add(new Button(Session));
                    
                    Topic.Note("In FFN.NonSalesTransfer.AddIn.Load\nExecuted service.Add(new Button(Session))");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFFN.NonSalesTransfer.AddIn.Load:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                    throw;
                }

            }
        }

        public void Unload()
        {
            using (Topic.Scope("FFN.NonSalesTransfer.AddIn.Unload(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.AddIn.Unload");
                    Session = null;

                    Topic.Note("In FFN.NonSalesTransfer.AddIn.Unload\nVariable Assignment\n\tSession: null");
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception("In FFFN.NonSalesTransfer.AddIn.Unload:\nException Occured\nException: " + ex.Message);
                    Topic.Exception(newEx);
                    //throw;
                }
            }
        }
    }
}
