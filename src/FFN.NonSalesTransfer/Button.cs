﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.Interactions;
using Interaction = ININ.IceLib.Interactions.Interaction;
using InteractionState = ININ.Client.Common.Interactions.InteractionState;

namespace FFN.NonSalesTransfer
{
    class Button : IInteractionButton
    {
        #region IInteraction Button Attributes
        public string ToolTipText { get { return "This button will allow you to select a transfer target for the selected call."; } }
        public string Text { get { return "NonSales XFR"; } }
        public string Id { get { return "NonSalesTransfer"; } }
        public SupportedInteractionTypes SupportedInteractionTypes { get { return SupportedInteractionTypes.Call; } }
        public Icon Icon { get { return Icon.ExtractAssociatedIcon("Addins\\I3Logo.ico"); } }
        #endregion

        private Session _session;
        private InteractionsManager InteractionsManager { get; set; }

        private MiddlewareClient.MiddlewareClient _client;
        private TransferTypes _transferTypes;
        private readonly SynchronizationContext _context;

        // Tracing
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.NonSalesTransfer.Button");

        public Button(Session session)
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.Button(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.Button\nParameter\n\tsession: " + session.ToString());
                    _session = session;
                    _context = SynchronizationContext.Current;
                    InteractionsManager = InteractionsManager.GetInstance(session);
                    Topic.Note("In FFN.NonSalesTransfer.Button.Button\nVariable Assignments\n\t_session: " + _session.ToString() + "\n\t_context: " + _context.ToString() + "\n\tInteractionsManager: " + InteractionsManager.ToString());
         

                    CreateAdminSession();
                    Topic.Note("In FFN.NonSalesTransfer.Button.Button\nExecuted CreateAdminSession()");
                    WatchServerParameter();
                    Topic.Note("In FFN.NonSalesTransfer.Button.Button\nExecuted WatchServerParameter()");
                    DisconnectAdminSession();
                    Topic.Note("In FFN.NonSalesTransfer.Button.Button\nExecuted DisconnectAdminSession()");
                    CreateMiddlewareClient();
                    Topic.Note("In FFN.NonSalesTransfer.Button.Button\nExecuted CreateMiddlewareClient()");
                    GetTransferTypes();
                    Topic.Note("In FFN.NonSalesTransfer.Button.Button\nExecuted GetTransferTypes()");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.Button:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                    throw;
                }
            }
        }

        public bool CanExecute(IInteraction selectedInteraction)
        {
            return selectedInteraction.InteractionState.Equals(InteractionState.Connected);
        }

        public void Execute(IInteraction selectedInteraction)
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.Execute(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.Execute:\nParameter:\n\tselectedInteraction: " + selectedInteraction.ToString());
                    var interaction = ConvertIInteractionToInteraction(selectedInteraction);
                    var transferFormResponse = PromptAgentForTransferType();
                    Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\nVariable Assignments:\n\tinteraction: " + interaction.ToString() + "\n\ttransferFormResponse: " + transferFormResponse.ToString());
                    if (transferFormResponse.Equals("Cancel")) { return; }

                    var transferType = _transferTypes.Items.FirstOrDefault(x => x.DisplayName.Equals(transferFormResponse));
                    Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\nVariable Assignments:\n\ttransferType: " +
                               transferType.ToString());

                    if (transferType == null)
                    {
                        Topic.Error("No transfer type selected in the Transfer Type Entry form!");
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType == null is true");
                        return;
                    }

                    if (transferType.TransferMethod.Equals("warm"))
                    {
                        WarmTransfer(interaction, transferType);
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType.TransferMethod.Equals(\"warm\") is true\nExecuted WarmTransfer:\nParameters:\n\tinteraction: " + interaction.ToString() + "\n\ttransferType: " + transferType.ToString());
                        Topic.Note("Warm transfer completed successfully");
                    }
                    else if (transferType.TransferMethod.Equals("cold"))
                    {
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType.TransferMethod.Equals(\"cold\") is true");
                        ColdTransfer(interaction, transferType);
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType.TransferMethod.Equals(\"cold\") is true\nExecuted ColdTransfer:\nParameters:\n\tinteraction: " + interaction.ToString() + "\n\ttransferType: " + transferType.ToString());
                        Topic.Note("Cold transfer completed successfully");
                    }
                    else if (transferType.TransferMethod.Equals("conference"))
                    {
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType.TransferMethod.Equals(\"conference\") is true");
                        ConferenceTransfer(interaction, transferType);
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType.TransferMethod.Equals(\"conference\") is true\nExecuted ConferenceTransfer:\nParameters:\n\tinteraction: " + interaction.ToString() + "\n\ttransferType: " + transferType.ToString());
                        Topic.Note("Conference transfer completed successfully");
                    }
                    else
                    {
                        Topic.Note("In FFN.NonSalesTransfer.Button.Execute:\ntransferType.TransferMethod.Equals is False: Else path");
                        Topic.Error(
                            "The transfer type '{}' is not allowed! Please check the configuration file in the Middleware",
                            transferType.TransferMethod);
                    }
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.Execute:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        #region TRANSFER METHODS

        private void WarmTransfer(Interaction interaction, TransferTypes.TransferType transferType)
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.WarmTransfer(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.WarmTransfer\nParameters\n\tinteraction: " + interaction .ToString() + "\n\ttransferType: " + transferType.ToString());
                    var userSelection = DialogResult.Cancel;
                    Topic.Note("In FFN.NonSalesTransfer.Button.WarmTransfer\nVariable Assigned:\n\tuserSelection: " + userSelection.ToString());
                    _context.Send(
                        p =>
                        {
                            userSelection = MessageBox.Show(
                                "This call will be transferred to {}. Click OK to begin the transfer. Click Cancel to return to the propsect.",
                                transferType.TransferDestinationNumber.ToString(CultureInfo.InvariantCulture),
                                MessageBoxButtons.OKCancel);

                        }, null);

                    Topic.Note("In FFN.NonSalesTransfer.Button.WarmTransfer\nExecuted _context.Send");
                    if (userSelection.Equals(DialogResult.OK))
                    {
                        Topic.Note("In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true");
                        Topic.Verbose("Agent has agreed to continue transfer of interaction {}",
                            interaction.InteractionId.Id);

                        var consultInteraction =
                            InteractionsManager.MakeConsultTransfer(
                                new ConsultTransferParameters(transferType.TransferDestinationNumber,
                                    interaction.InteractionId));

                        var newInteraction = InteractionsManager.CreateInteraction(consultInteraction.ConsultInteractionId);

                        Topic.Note("In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\nVariable Assignment:\n\tconsultInteraction: " + consultInteraction .ToString() + "\n\t: " + newInteraction.ToString());
                        newInteraction.SetStringAttribute("FF_FdrLeadId", interaction.GetStringAttribute("FF_FdrLeadId"));
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\nExecuted newInteraction.SetStringAttribute\nParameters:\n\t\"FF_FdrLeadId\": \"FF_FdrLeadId\"\n\tinteraction.GetStringAttribute(\"FF_FdrLeadId\"): " +
                            interaction.GetStringAttribute("FF_FdrLeadId").ToString());

                       newInteraction.SetStringAttribute("FF_FpApplicationId", interaction.GetStringAttribute("FF_FpApplicationId"));
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\nExecuted newInteraction.SetStringAttribute\nParameters:\n\t\"FF_FpApplicationId\": \"FF_FpApplicationId\"\n\tinteraction.GetStringAttribute(\"FF_FpApplicationId\"): " +
                            interaction.GetStringAttribute("FF_FpApplicationId").ToString());

                        consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\nExecuted consultInteraction.ChangeSpeakers\nParameter:\n\tConsultTransferParticipants.Consult: " +
                            ConsultTransferParticipants.Consult.ToString());

                        interaction.SetStringAttribute("FF_AccountCode", transferType.TransferCode);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\nExecuted interaction.SetStringAttribute\nParameters:\n\t\"FF_AccountCode\": \"FF_AccountCode\"\n\ttransferType.TransferCode: " +
                            transferType.TransferCode.ToString());

                        _context.Send(
                            p =>
                            {
                                userSelection = MessageBox.Show(
                                    "Wait for an agent to answer and then announce your transfer. Please provide the parter agent with any requested information. Once the agent is ready, click OK to complete the transfer. Click Cancel to cancel the transfer and return to the propsect",
                                    "Transferring to Partner Agent", MessageBoxButtons.OKCancel);
                            }, null);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\nExecuted  _context.Send");

                        if (userSelection.Equals(DialogResult.OK))
                        {
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\n(userSelection.Equals(DialogResult.OK)) is true");

                            consultInteraction.Conclude();
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\n(userSelection.Equals(DialogResult.OK)) is true\nExecuted consultInteraction.Conclude()");
                            _context.Send(p => MessageBox.Show("Transfer completed"), null);
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\n(userSelection.Equals(DialogResult.OK)) is true\nExecuted  _context.Send");

                            Topic.Status("Transfer completed");
                        }
                        else
                        {
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\n(userSelection.Equals(DialogResult.OK)) is false");
                            interaction.Hold(false);
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\n(userSelection.Equals(DialogResult.OK)) is false\nExecuted consultInteraction.Cancel()");
                            consultInteraction.Cancel();
                            _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is true\n(userSelection.Equals(DialogResult.OK)) is false\nExecuted _context.Send");
                            Topic.Status("Transfer cancelled");
                        }
                    }
                    else
                    {
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is false:");
                        interaction.Hold(false);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is false:\nExecuted interaction.Hold\nParameter:\n\tfalse:");
                        Topic.Warning("Agent has cancelled transfer of interaction {}", interaction.InteractionId.Id);
                        _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WarmTransfer\nuserSelection.Equals(DialogResult.OK) is false:\nExecuted _context.Send");
                        Topic.Status("Transfer cancelled");
                    }


                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.WarmTransfer:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void ColdTransfer(Interaction interaction, TransferTypes.TransferType transferType)
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.ColdTransfer(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.ColdTransfer:\nParameters:\n\tinteraction: " + interaction.ToString() + "\n\ttransferType: " + transferType.ToString());
                    interaction.SetStringAttribute("FF_AccountCode", transferType.TransferCode.ToString(CultureInfo.InvariantCulture));
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ColdTransfer:\nExecuted interaction.SetStringAttribute:\nParameters\n\t\"FF_AccountCode\": \"FF_AccountCode\"\n\ttransferType.TransferCode.ToString(CultureInfo.InvariantCulture: " +
                        transferType.TransferCode.ToString(CultureInfo.InvariantCulture).ToString());
                    Topic.Note("Set FF_AccountCode to be {}", transferType.TransferCode.ToString(CultureInfo.InvariantCulture));

                    interaction.BlindTransfer(transferType.TransferDestinationNumber.ToString(CultureInfo.InvariantCulture));
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ColdTransfer:\nExecuted interaction.BlindTransfer:\nParameters\n\ttransferType.TransferCode.ToString(CultureInfo.InvariantCulture: " +
                        transferType.TransferCode.ToString(CultureInfo.InvariantCulture).ToString());
                    Topic.Status("Transfered interaction {} to {}", interaction.InteractionId.Id, transferType.TransferDestinationNumber);

                    _context.Send(p => MessageBox.Show("Transfer completed"), null);
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ColdTransfer:\nExecuted _context.Send");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.ColdTransfer:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void ConferenceTransfer(Interaction interaction, TransferTypes.TransferType transferType)
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.ConferenceTransfer(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.ConferenceTransfer:\nParameters:\n\tinteraction: " + interaction.ToString() + "\n\ttransferType: " + transferType.ToString());
                    interaction.SetStringAttribute("FF_AccountCode", transferType.TransferCode.ToString(CultureInfo.InvariantCulture));

                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nExecuted interaction.SetStringAttribute\nParameters:\n\t\"FF_AccountCode\": \"FF_AccountCode\"\n\ttransferType.TransferCode.ToString(CultureInfo.InvariantCulture): " +
                        transferType.TransferCode.ToString(CultureInfo.InvariantCulture).ToString());
                    Topic.Verbose("Interaction {} is now on hold", interaction.InteractionId.Id);

                    var consultInteraction = InteractionsManager.MakeConsultTransfer(
                        new ConsultTransferParameters(transferType.TransferDestinationNumber, interaction.InteractionId));

                    var newInteraction = InteractionsManager.CreateInteraction(consultInteraction.ConsultInteractionId);
                    newInteraction.SetStringAttribute("FF_FdrLeadId", interaction.GetStringAttribute("FF_FdrLeadId"));
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nVariables Assigned:\n\tconsultInteraction: " + consultInteraction.ToString()
                        + "\n\tnewInteraction: " + newInteraction.ToString());
                    newInteraction.SetStringAttribute("FF_FpApplicationId", interaction.GetStringAttribute("FF_FpApplicationId"));
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nExecuted newInteraction.SetStringAttribute\nParameters\n\t\"FF_FpApplicationId\": \"FF_FpApplicationId\"\n\tinteraction.GetStringAttribute(\"FF_FpApplicationId\"): " + interaction.GetStringAttribute("FF_FpApplicationId").ToString());

                    consultInteraction.ChangeSpeakers(ConsultTransferParticipants.Consult);
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nExecuted consultInteraction.ChangeSpeakers\nParameters\n\tConsultTransferParticipants.Consult: " + ConsultTransferParticipants.Consult.ToString());

                    var userSelection = DialogResult.Cancel;
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nVariable Assigned\n\tuserSelection: " + userSelection.ToString());
                    _context.Send(
                        p =>
                        {
                            userSelection = MessageBox.Show(
                                "Provide client info to " + transferType.DisplayName + " agent. When " +
                                transferType.DisplayName +
                                " agent is ready, click OK to connect the client. Click Cancel to cancel the transfer.",
                                "Transfer to " + transferType.DisplayName,
                                MessageBoxButtons.OKCancel);

                        }, null);
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nExecuted _context.Send");

                    if (userSelection.Equals(DialogResult.OK))
                    {
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true");
                        interaction.SetStringAttribute("FF_AccountCode", transferType.TransferCode);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\nExecuted interaction.SetStringAttribute\nParameters\n\tParam1: \"FF_AccountCode\"\n\ttransferType.TransferCode: " + transferType.TransferCode.ToString());
                        Topic.Verbose("Set FF_AccountCode to be {}", transferType.TransferCode);

                        consultInteraction.ChangeSpeakers(ConsultTransferParticipants.All);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\nExecuted  consultInteraction.ChangeSpeakers\nParameter\n\t(ConsultTransferParticipants.All: " +
                            (ConsultTransferParticipants.All.ToString()));

                        userSelection = DialogResult.Cancel;
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\nVariable Assigned\n\tuserSelection: " + userSelection.ToString());
                        _context.Send(
                            p =>
                            {
                                userSelection = MessageBox.Show(
                                    "Introduce client and " + transferType.DisplayName + " agent. When ready, click OK to complete the handoff. To cancel, click Cancel and ask the " +
                                    transferType.DisplayName +
                                    " agent to hang up.", "Transfer to " + transferType.DisplayName,
                                    MessageBoxButtons.OKCancel);

                            }, null);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\nExecuted _context.Send");
                        if (userSelection.Equals(DialogResult.OK))
                        {
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is true:");
                            consultInteraction.Conclude();
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is true:\nExecuted consultInteraction.Conclude");
                            _context.Send(p => MessageBox.Show(transferType.DisplayName + " handoff complete."), null);
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is true:\nExecuted  _context.Send");
                            Topic.Status("{} handoff complete", transferType.DisplayName);
                        }
                        else
                        {
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is false:");
                            interaction.Hold(false);
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is false:\nExecuted interaction.Hold(false)");
                            consultInteraction.Cancel();
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is false:\nExecuted consultInteraction.Cancel()");
                            _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                            Topic.Note(
                                "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is true\n(userSelection.Equals(DialogResult.OK)) is false:\nExecuted _context.Send");
                            Topic.Status("Transfer cancelled");
                        }
                    }
                    else
                    {
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is false:");
                        interaction.Hold(false);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is false:\nExecuted interaction.Hold(false)");
                        consultInteraction.Cancel();
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is false:\nExecuted consultInteraction.Cancel()");
                        Topic.Verbose("Interaction {} is now off hold", interaction.InteractionId.Id);

                        _context.Send(p => MessageBox.Show("Transfer Cancelled"), null);
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.ConferenceTransfer:\n(userSelection.Equals(DialogResult.OK)) is false:\nExecuted _context.Send");
                        Topic.Status("Transfer cancelled");
                    }
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.ConferenceTransfer:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        #endregion

        #region HELPER METHODS

        private void CreateMiddlewareClient()
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.CreateMiddlewareClient(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.CreateMiddlewareClient");
                    var clientBinding = new BasicHttpBinding
                    {
                        Name = "BasicHttpBinding_IMiddleware"
                    };

                    var clientEndpoint = new EndpointAddress(MiddlewareServiceEndpoint);
                    _client = new MiddlewareClient.MiddlewareClient(clientBinding, clientEndpoint);
                    Topic.Note("In FFN.NonSalesTransfer.Button.CreateMiddlewareClient:\nVariables Assigned:\n\tclientBinding: " + clientBinding.ToString() + "\n\tclientEndpoint: " + clientEndpoint.ToString() + "\n\t_client: " + _client .ToString());
                    Topic.Status("Successfully created the Middleware Client connection");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.CreateMiddlewareClient:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private Interaction ConvertIInteractionToInteraction(IInteraction iinteraction)
        {
            Interaction ret = null;

            using (Topic.Scope("FFN.NonSalesTransfer.Button.ConvertIInteractionToInteraction(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.ConvertIInteractionToInteraction\nParameter:\n\tiinteraction: " + iinteraction.ToString());
                    ret = InteractionsManager.CreateInteraction(new InteractionId(iinteraction.InteractionId));
                    Topic.Note("In FFN.NonSalesTransfer.Button.ConvertIInteractionToInteraction\nVariable Assigned\n\tret: " + ret.ToString());
                    Topic.Verbose("Successfully created Interaction {}", ret.InteractionId.Id);
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.ConvertIInteractionToInteraction:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }

            return ret;
        }

        private void GetTransferTypes()
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.GetTransferTypes(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.GetTransferTypes");
                    var typesXml = _client.GetTransferTypes();
                    var serializer = new XmlSerializer(typeof(TransferTypes));
                    var reader = new StringReader(typesXml);
                    _transferTypes = (TransferTypes)serializer.Deserialize(reader);
                    Topic.Note("In FFN.NonSalesTransfer.Button.GetTransferTypes\nVariable Assignment:\n\ttypesXml: " + typesXml.ToString() + "\n\tserializer: " + serializer.ToString() + "\n\treader: " + reader.ToString() + "\n\t_transferTypes: " + _transferTypes.ToString());
                    Topic.Status("Found {} transfer types", _transferTypes.Items.Count);
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.GetTransferTypes:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        #endregion

        #region DISPLAY METHODS

        private string PromptAgentForTransferType()
        {
            var ret = "";

            using (Topic.Scope("FFN.NonSalesTransfer.Button.PromptAgentForTransferType(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.PromptAgentForTransferType");
                    var form = new TransferTypeEntryForm
                    {
                        TransferTypeNames = _transferTypes.Items.Select(x => x.DisplayName).ToList(),
                        TopMost = true
                    };

                    Topic.Note("In FFN.NonSalesTransfer.Button.PromptAgentForTransferType\nVariable Assignment:\n\tform: " + form.ToString());
                    _context.Send(p =>
                    {
                        using (form)
                        {
                            var result = form.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                ret = form.SelectedTransferType;
                                Topic.Note("Form return value: {}", ret);
                            }
                            else if (result == DialogResult.Cancel)
                            {
                                ret = "Cancel";
                                MessageBox.Show("Transfer Cancelled");
                            }
                        }
                    }, null);
                    Topic.Note("In FFN.NonSalesTransfer.Button.PromptAgentForTransferType\nExecuted _context.Send");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.PromptAgentForTransferType:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }

            return ret;
        }

        #endregion

        #region Server Parameter
        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        public string MiddlewareServiceEndpoint { get; private set; }

        private void WatchServerParameter()
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.WatchServerParameter(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.WatchServerParameter");
                    var serverParameters = new ServerParameters(_adminSession);
                    Topic.Note("In FFN.NonSalesTransfer.Button.WatchServerParameter\nVariable Assignment\n\tserverParameters: " + serverParameters.ToString());
                    serverParameters.StartWatching(_serverParameterName);
                    Topic.Note("In FFN.NonSalesTransfer.Button.WatchServerParameter\nExecuted serverParameters.StartWatching\nParameter\n\t_serverParameterName: " + _serverParameterName.ToString());
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Note("In FFN.NonSalesTransfer.Button.WatchServerParameter\nVariable Assignment\n\tparameterList: " + parameterList.ToString());
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WatchServerParameter\n(parameterList.Count.Equals(1)) is true");


                        MiddlewareServiceEndpoint = parameterList[0].Value;
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WatchServerParameter\n(parameterList.Count.Equals(1)) is true\nVariable Assignment\n\tMiddlewareServiceEndpoint: " + MiddlewareServiceEndpoint.ToString());
                        Topic.Note("The endpoint for the middleware server is set to: {}", MiddlewareServiceEndpoint);
                    }
                    else
                    {
                        Topic.Note(
                            "In FFN.NonSalesTransfer.Button.WatchServerParameter\n(parameterList.Count.Equals(1)) is false");
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.WatchServerParameter\nExecuted serverParameters.StopWatching");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.WatchServerParameter:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        #endregion

        #region Admin Session
        private Session _adminSession;
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

        private void CreateAdminSession()
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.CreateAdminSession(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.CreateAdminSession");
                    Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host,
                        AdminUsername);
                    _adminSession = new Session
                    {
                        AutoReconnectEnabled = true
                    };
                    Topic.Note("In FFN.NonSalesTransfer.Button.CreateAdminSession\nVariable Assignment:\n\t_adminSession: " + _adminSession.ToString() + "\n\t_adminSession.AutoReconnectEnabled: " + _adminSession.AutoReconnectEnabled.ToString());
                    _adminSession.Connect(new SessionSettings(),
                        new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                        new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
                    Topic.Note(
                        "In FFN.NonSalesTransfer.Button.CreateAdminSession\nExecuted _adminSession.Connect(new SessionSettings(),new HostSettings(new HostEndpoint(_session.Endpoint.Host)),new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings())");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.CreateAdminSession:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }

        }

        private void DisconnectAdminSession()
        {
            using (Topic.Scope("FFN.NonSalesTransfer.Button.DisconnectAdminSession(..)"))
            {
                try
                {
                    Topic.Note("Entering FFN.NonSalesTransfer.Button.DisconnectAdminSession");
                    _adminSession.Disconnect();
                    Topic.Note("In FFN.NonSalesTransfer.Button.DisconnectAdminSession\nExecuted _adminSession.Disconnect()");
                    Topic.Status("Proxy session has been disconnected");
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In FFN.NonSalesTransfer.Button.DisconnectAdminSession:\nException occured\nException: {}" + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }
        #endregion
    }
}
