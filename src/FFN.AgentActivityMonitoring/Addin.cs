﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Timers;
using FFN.AgentActivityMonitoring.MiddlewareService;
using ININ.Diagnostics;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.People;
using ININ.InteractionClient.AddIn;

namespace FFN.AgentActivityMonitoring
{
    public class Addin : QueueMonitor
    {
        private Session _session;
        private MiddlewareClient _client;
        private UserStatusList _userStatusList;

        private string _extension = "";

        private Timer _agentUpdateTimer;
        private readonly object _interactionsLock = new object();
        private readonly List<IInteraction> _interactionsAlreadyProcessed = new List<IInteraction>();

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("FFN.AgentActivityMonitoring.AddIn");

        protected override IEnumerable<string> Attributes
        {
            get
            {
                return new[]
                {
                    InteractionAttributes.State
                };
            }
        }

        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            using (Topic.Scope())
            {
                try
                {
                    _session = serviceProvider.GetService(typeof(Session)) as Session;

                    CreateAdminSession();
                    WatchServerParameter();
                    CreateMiddlewareClient();

                    GetLoggedInUsersExtension();

                    UpdateUserStatusInWcb("ready");
                    StartAgentUpdateTimer();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }

                base.OnLoad(serviceProvider);
            }
        }

        protected override void InteractionChanged(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    var callState = interaction.GetAttribute(InteractionAttributes.State);
                    var callStateString = interaction.GetAttribute(InteractionAttributes.StateDisplay);

                    Topic.Note("Interaction {}'s state has changed to {}", interaction.InteractionId, callState);
                    Topic.Verbose("Interaction {}'s call state string has changed to {}", interaction.InteractionId, callStateString);

                    if (!_interactionsAlreadyProcessed.Contains(interaction))
                    {
                        if (callState.Equals(InteractionAttributeValues.State.Connected) ||
                            callState.Equals(InteractionAttributeValues.State.Offering) ||
                            callState.Equals(InteractionAttributeValues.State.Alerting) ||
                            callState.Equals(InteractionAttributeValues.State.Proceeding))
                        {
                            Topic.Note("The interaction {} has not been processed yet!", interaction.InteractionId);
                            UpdateUserStatusInWcb("talking");

                            lock (_interactionsLock)
                            {
                                _interactionsAlreadyProcessed.Add(interaction);
                                Topic.Note("Added interaction {} to the processed list", interaction.InteractionId);
                            }
                        }
                    }
                    else
                    {
                        Topic.Status("The interaction {} has already been processed!", interaction.InteractionId);

                        if (callState.Equals(InteractionAttributeValues.State.ExternalDisconnect) ||
                            callState.Equals(InteractionAttributeValues.State.InternalDisconnect))
                        {
                            Topic.Note("Interaction {} has been disconnected!", interaction.InteractionId);
                            UpdateUserStatusInWcb("ready");

                            lock (_interactionsLock)
                            {
                                _interactionsAlreadyProcessed.Remove(interaction);
                                Topic.Note("Removed interaction {} to the processed list", interaction.InteractionId);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }

            base.InteractionChanged(interaction);
        }

        protected override void InteractionRemoved(IInteraction interaction)
        {
            using (Topic.Scope())
            {
                try
                {
                    Topic.Note("Interaction {} has been removed from the user queue", interaction.InteractionId);

                    if (_interactionsAlreadyProcessed.Contains(interaction))
                    {
                        UpdateUserStatusInWcb("ready");

                        lock (_interactionsLock)
                        {
                            _interactionsAlreadyProcessed.Remove(interaction);
                            Topic.Note("Removed interaction {} to the processed list", interaction.InteractionId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        protected override void OnUnload()
        {
            using (Topic.Scope())
            {
                try
                {
                    Topic.Note("Agent Activity Monitoring being unloaded");
                    UpdateUserStatusInWcb("inactive");
                    DisconnectAdminSession();

                    _agentUpdateTimer.Stop();
                    _agentUpdateTimer.Elapsed -= _agentUpdateTimer_Elapsed;
                    _agentUpdateTimer = null;
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        #region Timer Methods
        private void StartAgentUpdateTimer()
        {
            using (Topic.Scope())
            {
                try
                {
                    Topic.Status("Starting agent update timer.");

                    var numberOfSeconds = _client.GetNumberSecondsForAgentStatusUpdate();
                    _agentUpdateTimer = new Timer(numberOfSeconds * 1000);
                    _agentUpdateTimer.Elapsed += _agentUpdateTimer_Elapsed;
                    _agentUpdateTimer.Start();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void _agentUpdateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Topic.Verbose("Getting the agents status.");
                lock (_interactionsLock)
                {
                    if (_interactionsAlreadyProcessed.Count > 0 || !IsUserInAcdStatus())
                    {

                        Topic.Verbose("Sending agent status to talking.");
                        UpdateUserStatusInWcb("talking");
                    }
                    else
                    {
                        Topic.Verbose("Sending agent status to ready.");
                        UpdateUserStatusInWcb("ready");
                    }
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion

        #region Admin Session
        private Session _adminSession;
        private const string AdminUsername = "MiddlewareAdmin", AdminPassword = "middleware";

        private void CreateAdminSession()
        {
            try
            {
                Topic.Note("Attempting to connect to {} with the account {}", _session.Endpoint.Host, AdminUsername);
                _adminSession = new Session
                {
                    AutoReconnectEnabled = true
                };
                _adminSession.ConnectionStateChanged += ConnectionStateChanged;
                _adminSession.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_session.Endpoint.Host)),
                    new ICAuthSettings(AdminUsername, AdminPassword), new StationlessSettings());
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

        }

        private void ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            try
            {
                if (e.State.Equals(ConnectionState.Up))
                {
                    StartWatchingUserStatus();
                }
                else
                {
                    StopWatchingUserStatus();
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private void DisconnectAdminSession()
        {
            try
            {
                _adminSession.Disconnect();
                Topic.Status("Proxy session has been disconnected");
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion

        #region Get ACD Status
        private void StartWatchingUserStatus()
        {
            try
            {
                _userStatusList = new UserStatusList(PeopleManager.GetInstance(_adminSession));
                _userStatusList.StartWatching(new[] { _session.UserId });
                Topic.Note("Now watching the user {}'s status", _session.UserId);
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        private bool IsUserInAcdStatus()
        {
            var ret = false;

            try
            {
                if (_userStatusList != null && _userStatusList.IsWatching(_session.UserId))
                {
                    var status = _userStatusList.GetUserStatus(_session.UserId);
                    Topic.Note("The user {} is currently in the status {}", _session.UserId,
                        status.StatusMessageDetails.MessageText);
                    ret = status.StatusMessageDetails.IsAcdStatus;
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }

            return ret;
        }

        private void StopWatchingUserStatus()
        {
            try
            {
                if (_userStatusList != null)
                {
                    _userStatusList.StopWatching();
                    _userStatusList = null;
                    Topic.Note("Stopped watching user status");
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
        #endregion

        #region Helper Methods
        private void CreateMiddlewareClient()
        {
            using (Topic.Scope())
            {
                try
                {
                    var clientBinding = new BasicHttpBinding { Name = "BasicHttpBinding_IMiddleware" };
                    var clientEndpoint = new EndpointAddress(_middlewareServerEndpoint);
                    _client = new MiddlewareClient(clientBinding, clientEndpoint);

                    Topic.Status("Successfully created the Middleware Client bound to endpoint: {}",
                        clientEndpoint.Uri.AbsolutePath);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void GetLoggedInUsersExtension()
        {
            using (Topic.Scope())
            {
                try
                {
                    var userConfigurationList = new UserConfigurationList(ConfigurationManager.GetInstance(_adminSession));
                    var querySettings = userConfigurationList.CreateQuerySettings();
                    querySettings.SetPropertiesToRetrieve(new[]
                    {
                        UserConfiguration.Property.Id, UserConfiguration.Property.Extension
                    });
                    querySettings.SetFilterDefinition(
                        new BasicFilterDefinition<UserConfiguration, UserConfiguration.Property>(
                            UserConfiguration.Property.Id, _session.UserId));

                    userConfigurationList.StartCaching(querySettings);

                    var user =
                        userConfigurationList.GetConfigurationList()
                            .FirstOrDefault(x => x.ConfigurationId.Id.Equals(_session.UserId));

                    if (user != null)
                    {
                        _extension = user.Extension.Value;
                        Topic.Status("{}'s extension is '{}'", _session.UserId, _extension);
                    }

                    userConfigurationList.StopCaching();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        private void UpdateUserStatusInWcb(string status)
        {
            using (Topic.Scope())
            {
                try
                {
                    _client.UpdateUserStatusInWcb(_extension, status);
                    Topic.Note("Called UpdateUserStatusInWcb with extension: {} | status: {}", _extension, status);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion

        #region Server Parameter
        private readonly string[] _serverParameterName = { "FFN_MiddlewareServer" };
        private string _middlewareServerEndpoint = "";

        private void WatchServerParameter()
        {
            using (Topic.Scope())
            {
                try
                {
                    var serverParameters = new ServerParameters(_adminSession);
                    serverParameters.StartWatching(_serverParameterName);
                    var parameterList = serverParameters.GetServerParameters(_serverParameterName);
                    Topic.Status("Number of server parameter results: {}; Retrieving the server name now...", parameterList.Count);

                    if (parameterList.Count.Equals(1))
                    {
                        _middlewareServerEndpoint = parameterList[0].Value;
                        Topic.Note("The endpoint for the middleware server is set to: {}", _middlewareServerEndpoint);
                    }
                    else
                    {
                        Topic.Error("Could not find an endpoint for the middleware!");
                    }

                    serverParameters.StopWatching();
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }
        #endregion
    }
}
