﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using ScreenPopManager.FDR.Spanish;
using ININ.Diagnostics;

namespace ScreenPopManager.FDR.Spanish.Forms
{
    public partial class BillboardFromUri : Form
    {
        #region Constants
        public const string RESOURCE_TYPE_URI = "uri";
        public const string RESOURCE_TYPE_HTML = "html";
        #endregion

        #region Force Form Always on Top
        //see http://stackoverflow.com/a/34703664/90400
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("ScreenPopManager.FDR.Forms");

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        #endregion

        public BillboardFromUri(string windowTitle, string resource, string resourceType, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("BillboardFromUri.BillboardFromUri(..)"))
            {
                InitializeComponent();
                SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS); //force form to always be on top

                try
                {
                    string htmlBodyText = "ERROR: NO RESOURCE LOADED";
                    int browserLoadTime = 0;

                    this.Text = windowTitle;
                    this.dismissBillboardButton.Text = "Close";

                    this.billboardWebBrowser.Navigating +=
                        new WebBrowserNavigatingEventHandler(BillboardWebBrowser_HijackLinkClicks);

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.DocumentText = "0";
                    this.billboardWebBrowser.Document.OpenNew(true);
                    switch (resourceType)
                    {
                        case RESOURCE_TYPE_URI:
                            this.billboardWebBrowser.Navigate(new Uri(resource));
                            while (this.billboardWebBrowser.ReadyState != WebBrowserReadyState.Complete &&
                                   browserLoadTime < ScreenPopManager.FDR.Spanish.DotNetClientAddin.billboardBrowserLoadTimeMax)
                            {
                                Application.DoEvents();
                                System.Threading.Thread.Sleep(1000);
                                browserLoadTime++;

                            }

                            if (browserLoadTime == ScreenPopManager.FDR.Spanish.DotNetClientAddin.billboardBrowserLoadTimeMax)
                            {
                                htmlBodyText = "<h1>TIME OUT TRYING TO LOAD BILLBOARD HTML SCRIPT `" + resource +
                                               "`</h1>";
                            }
                            else
                            {
                                htmlBodyText = billboardWebBrowser.DocumentText;
                            }

                            break;
                        case RESOURCE_TYPE_HTML:
                            htmlBodyText = resource;
                            break;
                        default:
                            //throw new Exception("SCREEN POP CONFIGURATION ERROR! Unhandled resource type `" +
                            //                    resourceType + "`.");
                            break;
                    }

                    //perform variable data replacement
                    htmlBodyText = htmlBodyText.Replace("(AE)", customText.AgentName);
                    htmlBodyText = htmlBodyText.Replace("(prospect)", customText.ProspectName);
                    htmlBodyText = htmlBodyText.Replace("(ani)", customText.ClientPhone);
                    htmlBodyText = htmlBodyText.Replace("(ani pretty)", customText.ClientPhonePretty);

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.Document.Write(htmlBodyText);
                    //this.billboardWebBrowser.DocumentText = htmlBodyText;
                    this.billboardWebBrowser.Refresh();
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardFromUri.BillboardFromUri:\nException occurred:\nScreen Pop Manager Billboard has encountered an error:\nError Message: {}", ex);
                    Topic.Exception(newEx);
                }
            }
        }

        private void DismissBillboardButton_Click(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.dismissBillboardButton_Click(..)"))
            {
                try
                {
                    this.dismissBillboardButton.Enabled = false;
                    this.Close();
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardFromUri.dismissBillboardButton_Click\nException occured in dismissBillboardButton_Click. Method Parameters:\n\tsender: " + sender.ToString() + "\n\te: " + e.ToString() + ". Failed to execute command this.close().\n Exception: {0}", ex);
                    Topic.Exception(newEx);
                }
            }
        }

        private void BillboardWebBrowser_HijackLinkClicks(object sender, WebBrowserNavigatingEventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.billboardWebBrowser_HijackLinkClicks(..)"))
            {
                try
                {
                    string url = e.Url.ToString();
                    if (url != "about:blank")
                    {
                        e.Cancel = true;
                        Process.Start(e.Url.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Exception newex = new Exception("BillboardFromUri.billboardWebBrowser_HijackLinkClicks\nException occurred in billboardWebBrowser_HijackLinkClicks:\nException: " + ex.Message);
                    Topic.Exception(newex);
                }
            }
        }

        private void BillboardWebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.billboardWebBrowser_DocumentCompleted(..)"))
            {
                Topic.Note("Entering and Exiting billboardWebBrowser_DocumentCompleted:\nParameters\n\tsender " + sender.ToString() + "\n\te: {}", e);
            }

        }

        private void BillboardFromUri_Load(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardFromUri.BillboardFromUri_Load(..)"))
            {
                Topic.Note("Entering and Exiting BillboardFromUri_Load:\nParameters\n\tsender " + sender.ToString() + "\n\te: {}", e);
            }
        }
    }
}

