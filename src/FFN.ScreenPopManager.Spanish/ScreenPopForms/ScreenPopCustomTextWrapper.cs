﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace ScreenPopManager.FDR.Spanish
{
    public class ScreenPopCustomTextWrapper
    {
        private string _clientSsn;
        private string _clientSsnLastFourDigits;
        private string _clientPhone;


        public string ClientSsn { get { return this._clientSsn; } set { if (value.Length == 9) { this._clientSsn = value;  this.ClientSsnLastFourDigits = value.Substring(5, 8); } else { throw new Exception("Invalid SSN `" + value + "`; must by exactly 9 characters."); } } }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string ClientSsnPretty => (ClientSsn.Insert(2, "-")).Insert(7,"-");
        public string ClientSsnLastFourDigits { get { return this._clientSsnLastFourDigits; } set { if (value.Length == 4) { this._clientSsnLastFourDigits = value; } else { throw new Exception("Invalid SSN last 4 digits - `" + value + "`; must by exactly 4 characters."); } } }
        public string ClientDateOfBirthAsString { get; set; }
        public string ClientMonthlyDraftAmount { get; set; }
        public string ClientPhone { get { return this._clientPhone; } set { this._clientPhone = System.Text.RegularExpressions.Regex.Replace(value, "[^0-9]", ""); if (this._clientPhone.Length == 11 && (this._clientPhone.Substring(0,1) == "1")) { this._clientPhone = this._clientPhone.Substring(1, 10); } } }
        public string ClientPhonePretty => (ClientPhone.Insert(0, "(").Insert(4, ") ").Insert(9, "-"));
        public string CoClientName { get; set; }
        public string ClientEmail { get; set; }
        public string ProspectName { get; set; }
        public string AgentName { get; set; }
        public bool ClientSsnLastFourDigitsVerified  { get; set; }
        public bool ClientDateOfBirthAsStringVerified { get; set; }
        public bool ClientMonthlyDraftAmountVerified { get; set; }
        public bool ClientSsnLastFourDigitsActive { get; set; }
        public bool ClientDateOfBirthAsStringActive { get; set; }
        public bool ClientMonthlyDraftAmountActive { get; set; }

        public string ClientSalesforceUrl { get; set; }
        public string ClientSalesforceSummaryUrl { get; set; }



        public ScreenPopCustomTextWrapper()
        {
            this._clientSsn                        = "000000000";
            this._clientSsnLastFourDigits          = "0000";
            this.ClientName                        = "NO CLIENT NAME";
            this.ClientAddress                     = "NO CLIENT ADDRESS";
            this.ClientDateOfBirthAsString         = "1900-01-01";
            this.ClientMonthlyDraftAmount          = "0.00";
            this.ClientPhone                       = "0000000000";
            this.CoClientName                      = "NO CO-CLIENT NAME";
            this.ClientEmail                       = "NO CLIENT EMAIL";
            this.ProspectName                      = "NO PROSPECT NAME";
            this.AgentName                         = "NO AGENT NAME";
            this.ClientSsnLastFourDigitsVerified   = false;
            this.ClientDateOfBirthAsStringVerified = false;
            this.ClientMonthlyDraftAmountVerified  = false;
            this.ClientSsnLastFourDigitsActive = false;
            this.ClientDateOfBirthAsStringActive = false;
            this.ClientMonthlyDraftAmountActive = false;
            this.ClientSalesforceUrl               = "";
            this.ClientSalesforceSummaryUrl        = "";
        }

    }
}
 