﻿using System;

namespace ScreenPopManager.FDR.Spanish.Forms
{

    partial class BillboardVerificationSecurityQuestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.Dispose(..)"))
            {
                try
                {
                    if (disposing && (components != null))
                    {
                        components.Dispose();
                    }

                    base.Dispose(disposing);
                }
                catch (Exception ex)
                {
                    Topic.Exception(ex);
                }
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.billboardWebBrowser = new System.Windows.Forms.WebBrowser();
            this.verificationResultsWebBrowser = new System.Windows.Forms.WebBrowser();
            this.webBrowserPanel = new System.Windows.Forms.TableLayoutPanel();
            this.billboardDismissButton = new System.Windows.Forms.Button();
            this.webBrowserPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // billboardWebBrowser
            // 
            this.billboardWebBrowser.AllowWebBrowserDrop = false;
            this.billboardWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billboardWebBrowser.IsWebBrowserContextMenuEnabled = false;
            this.billboardWebBrowser.Location = new System.Drawing.Point(3, 264);
            this.billboardWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.billboardWebBrowser.Name = "billboardWebBrowser";
            this.billboardWebBrowser.ScriptErrorsSuppressed = true;
            this.billboardWebBrowser.Size = new System.Drawing.Size(778, 255);
            this.billboardWebBrowser.TabIndex = 8;
            this.billboardWebBrowser.WebBrowserShortcutsEnabled = false;
            this.billboardWebBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.BillboardWebBrowser_DocumentCompleted);
            // 
            // verificationResultsWebBrowser
            // 
            this.verificationResultsWebBrowser.AllowWebBrowserDrop = false;
            this.verificationResultsWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verificationResultsWebBrowser.IsWebBrowserContextMenuEnabled = false;
            this.verificationResultsWebBrowser.Location = new System.Drawing.Point(3, 3);
            this.verificationResultsWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.verificationResultsWebBrowser.Name = "verificationResultsWebBrowser";
            this.verificationResultsWebBrowser.ScriptErrorsSuppressed = true;
            this.verificationResultsWebBrowser.Size = new System.Drawing.Size(778, 255);
            this.verificationResultsWebBrowser.TabIndex = 7;
            this.verificationResultsWebBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // webBrowserPanel
            // 
            this.webBrowserPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.webBrowserPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.webBrowserPanel.ColumnCount = 1;
            this.webBrowserPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.webBrowserPanel.Controls.Add(this.verificationResultsWebBrowser, 0, 0);
            this.webBrowserPanel.Controls.Add(this.billboardWebBrowser, 0, 1);
            this.webBrowserPanel.Controls.Add(this.billboardDismissButton, 0, 2);
            this.webBrowserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserPanel.Location = new System.Drawing.Point(0, 0);
            this.webBrowserPanel.Name = "webBrowserPanel";
            this.webBrowserPanel.RowCount = 3;
            this.webBrowserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.webBrowserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.webBrowserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.webBrowserPanel.Size = new System.Drawing.Size(784, 562);
            this.webBrowserPanel.TabIndex = 8;
            // 
            // billboardDismissButton
            // 
            this.billboardDismissButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billboardDismissButton.Location = new System.Drawing.Point(3, 525);
            this.billboardDismissButton.Name = "billboardDismissButton";
            this.billboardDismissButton.Size = new System.Drawing.Size(778, 34);
            this.billboardDismissButton.TabIndex = 9;
            this.billboardDismissButton.Text = "dismissBillboardButton";
            this.billboardDismissButton.UseVisualStyleBackColor = true;
            this.billboardDismissButton.Click += new System.EventHandler(this.BillboardDismissButton_Click);
            // 
            // BillboardVerificationSecurityQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.webBrowserPanel);
            this.Name = "BillboardVerificationSecurityQuestion";
            this.Text = "VerificationSecurityQuestion";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.VerificationSecurityQuestion_Load);
            this.webBrowserPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.WebBrowser billboardWebBrowser;
        private System.Windows.Forms.WebBrowser verificationResultsWebBrowser;
        private System.Windows.Forms.TableLayoutPanel webBrowserPanel;
        private System.Windows.Forms.Button billboardDismissButton;
    }
}