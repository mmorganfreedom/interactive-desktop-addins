﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using ScreenPopManager.FDR.Spanish;
using ININ.Diagnostics;
using System.Threading;
//using mshtml;
//using System.Text.RegularExpressions;

namespace ScreenPopManager.FDR.Spanish.Forms
{
    public partial class BillboardVerificationSecurityQuestion : Form
    {
        #region Constants
        public const string RESOURCE_TYPE_URI = "uri";
        public const string RESOURCE_TYPE_HTML = "html";

        private const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS = "<span class='verifiedPass'>&#10004;</span>";
        private const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL = "<span class='verifiedFail'>X</span>";
        private const string SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL = "<span class='verifiedNull'>&nbsp;</span>";

        private const string SALESFORCE_CLIENT_HIGHLIGHTS_CSS_LINK = @"https://cs61.salesforce.com/resource/1479988015000/SLDS/assets/styles/salesforce-lightning-design-system-ltng.css";
        #endregion

        #region Variables
        private string _salesforceClientProfileUrl = "";
        private string _salesforceClientHighlightsUrl = "";
        private bool _salesforceLoginSubmitted = false;
        private bool _debug = false;
        #endregion
        #region Force Form Always on Top
        //see http://stackoverflow.com/a/34703664/90400
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("ScreenPopManager.FDR.Forms");

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        #endregion

        public BillboardVerificationSecurityQuestion(string windowTitle, string resource, string resourceType, ScreenPopCustomTextWrapper customText)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion(..)"))
            {

                try
                {
                    InitializeComponent();
                    SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS); //force form to always be on top

                    this.FormClosing += new FormClosingEventHandler(Billboard_Exit);

                    #region Init

                    this._salesforceClientProfileUrl = customText.ClientSalesforceUrl;
                    this._salesforceClientHighlightsUrl = customText.ClientSalesforceSummaryUrl;
                    string htmlBodyText = "ERROR: NO RESOURCE LOADED";
                    bool verificationResult = (customText.ClientSsnLastFourDigitsActive
                                                  ? customText.ClientSsnLastFourDigitsVerified
                                                  : true)
                                              && (customText.ClientDateOfBirthAsStringActive
                                                  ? customText.ClientDateOfBirthAsStringVerified
                                                  : true)
                                              && (customText.ClientMonthlyDraftAmountActive
                                                  ? customText.ClientMonthlyDraftAmountVerified
                                                  : true)
                        ;

                    this.Text = windowTitle;
                    this.webBrowserPanel.BackColor = verificationResult ? Color.ForestGreen : Color.DarkRed;
                    this.billboardDismissButton.Text = "Continue >>";


                    if (this._debug)
                    {
                        this.MaximizeBox = false;
                        this.TopMost = false;
                        this.billboardWebBrowser.IsWebBrowserContextMenuEnabled = true;
                        this.verificationResultsWebBrowser.IsWebBrowserContextMenuEnabled = true;
                    }

                    //this.billboardWebBrowser.Navigating                  += new WebBrowserNavigatingEventHandler(webBrowser_HijackLinkClicks);
                    //this.verificationResultsWebBrowser.Navigating        += new WebBrowserNavigatingEventHandler(webBrowser_HijackLinkClicks);

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.DocumentText = "0";

                    this.billboardWebBrowser.Document.OpenNew(true);

                    switch (resourceType)
                    {
                        case RESOURCE_TYPE_HTML:
                            htmlBodyText = resource;
                            break;
                        default:
                            Topic.Note(
                                "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nIn Switch:\nrosourceType == default:\nException Occurred:\n\tSCREEN POP CONFIGURATION ERROR!Unhandled resource type\n\t\tresourceType: {}",
                                resourceType);
                            /*throw new Exception("SCREEN POP CONFIGURATION ERROR! Unhandled resource type `" +
                                                resourceType + "`.");*/
                            break;
                    }

                    #endregion

                    #region Variable Data Insertion

                    //show/hide VSQs
                    htmlBodyText = htmlBodyText.Replace("(rowVsqMonthlyDepositClass)",
                        (customText.ClientMonthlyDraftAmountActive ? "visible" : "hidden"));
                    htmlBodyText = htmlBodyText.Replace("(rowVsqClientSsnClass)",
                        (customText.ClientSsnLastFourDigitsActive ? "visible" : "hidden"));
                    htmlBodyText = htmlBodyText.Replace("(rowVsqDobClass)",
                        (customText.ClientDateOfBirthAsStringActive ? "visible" : "hidden"));

                    //perform variable data replacement
                    htmlBodyText = htmlBodyText.Replace("(verified?:pass_fail)",
                        (verificationResult
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));

                    htmlBodyText = htmlBodyText.Replace("(response:ssn_last_four_digits)",
                        customText.ClientSsnLastFourDigits);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:ssn_last_four_digits)",
                        (customText.ClientSsnLastFourDigitsVerified
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));

                    htmlBodyText =
                        htmlBodyText.Replace("(response:date_of_birth)", customText.ClientDateOfBirthAsString);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:date_of_birth)",
                        (customText.ClientDateOfBirthAsStringVerified
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));

                    htmlBodyText =
                        htmlBodyText.Replace("(response:monthly_deposit)", customText.ClientMonthlyDraftAmount);
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:monthly_deposit)",
                        (customText.ClientMonthlyDraftAmountVerified
                            ? SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_PASS
                            : SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_FAIL));

                    //currently unhandled
                    htmlBodyText = htmlBodyText.Replace("(verified?:clientinfo:fullname)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    htmlBodyText = htmlBodyText.Replace("(clientinfo:fullname)", "");

                    htmlBodyText = htmlBodyText.Replace("(verified?:clientinfo:fullname)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    htmlBodyText = htmlBodyText.Replace("(clientinfo:fullname)", "");

                    htmlBodyText = htmlBodyText.Replace("(response:phone)", "");
                    htmlBodyText = htmlBodyText.Replace("(verified?:response:phone)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);

                    htmlBodyText = htmlBodyText.Replace("(verified?:clientinfo:coclient_fullname)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    htmlBodyText = htmlBodyText.Replace("(clientinfo:coclient_fullname)", "");

                    htmlBodyText = htmlBodyText.Replace("(response:email)", "");
                    htmlBodyText = htmlBodyText.Replace("(verified?:email)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);
                    htmlBodyText = htmlBodyText.Replace("(response:address_full)", "");
                    htmlBodyText = htmlBodyText.Replace("(verified?:address_full)",
                        SCREEN_POP_SECURITY_QUESTION_VERIFCATION_HTML_NULL);

                    #endregion

                    #region Set Up Webbrowsers & Load Pages

                    //this solution to force the is bollocks, but robust.  See http://stackoverflow.com/a/23736063/90400
                    this.billboardWebBrowser.Document.Write(htmlBodyText);
                    this.billboardWebBrowser.Refresh();

                    this.verificationResultsWebBrowser.DocumentCompleted +=
                        new WebBrowserDocumentCompletedEventHandler(WebBrowser_CheckForSfLogin);
                    this.verificationResultsWebBrowser.Navigate(this._salesforceClientHighlightsUrl);
                    //this.verificationResultsWebBrowser.Navigated += this.webBrowser_injectSfCss;

                    Process.Start(this._salesforceClientProfileUrl);

                    #endregion
                }
                catch (Exception ex)
                {
                    Exception newEx =
                        new Exception(
                            "In BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion\nException occurred:\n\tScreen Pop Manager Billboard has encountered an error:\n\tException: {}",
                            ex);
                    Topic.Exception(newEx);
                }
            }
        }

        void BillboardVerificationSecurityQuestion_FormClosing(object sender, FormClosedEventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion_FormClosing(..)"))
            {
                Topic.Note("Entering and exiting method BillboardVerificationSecurityQuestion.BillboardVerificationSecurityQuestion_FormClosing:\nParameters\n\tsender: " + sender.ToString() + "\n\te: {}", e);
            }
        }

        private void WebBrowser_injectSfCss(object sender, WebBrowserNavigatedEventArgs e)
        {
            //can be deleted, left in for personal future reference
            using (Topic.Scope("BillboardVerificationSecurityQuestion.webBrowser_injectSfCss(..)"))
            {
                Topic.Note("Entering and exiting method BillboardVerificationSecurityQuestion.webBrowser_injectSfCss:\nParameters\n\tsender: " + sender.ToString() + "\n\te: {}", e);
            }
        }

        private void WebBrowser_CheckForSfLogin(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin(..)"))
            {
                try
                {
                    string urlAbsolutePath = e.Url.AbsolutePath;
                    //string url = e.Url.ToString().Split('?')[0];
                    //string[] urlDomainElements = e.Url.GetComponents(UriComponents.Host, UriFormat.Unescaped).Split('.');
                    //string urlSubdomain = (urlDomainElements.Count() == 3) ? urlDomainElements[0] : "";

                    //this is terrible awful no good and MUST be refactored when there is more time - far, far too fragile!
                    //https://test.salesforce.com/s.gif
                    //https://test.salesforce.com/?ec=302&startURL=/apex/ClientHighlightsPanel_CS?id=0017000000v3c4iAAA
                    //https://c.salesforce.com/login-messages/promos.html?r=https://test.salesforce.com/apex/ClientHighlightsPanel_CS?id=0017000000v3c4iAAA
                    if (this.verificationResultsWebBrowser.ReadyState == WebBrowserReadyState.Complete)
                    {
                        if (!this._salesforceLoginSubmitted && urlAbsolutePath.Contains("login"))
                        {
                            HtmlElement elUsername =
                                this.verificationResultsWebBrowser.Document.GetElementById("username");
                            HtmlElement elUn =
                                this.verificationResultsWebBrowser.Document.All.GetElementsByName("un")[0];
                            HtmlElement elPassword =
                                this.verificationResultsWebBrowser.Document.GetElementById("password");
                            HtmlElement elLoginForm =
                                this.verificationResultsWebBrowser.Document.GetElementById("login_form");
                            elUsername.SetAttribute("Value",
                                ScreenPopManager.FDR.Spanish.DotNetClientAddin._salesforceUsernameInintelephony);
                            elUn.SetAttribute("Value",
                                ScreenPopManager.FDR.Spanish.DotNetClientAddin._salesforceUsernameInintelephony);
                            elPassword.SetAttribute("Value",
                                ScreenPopManager.FDR.Spanish.DotNetClientAddin._salesforcePasswordInintelephony);
                            elLoginForm.InvokeMember("submit");
                            this._salesforceLoginSubmitted = true;
                        }
                        else if (urlAbsolutePath.Contains("ClientHighlightsPanel_CS"))
                        {
                            HtmlDocument document = this.verificationResultsWebBrowser.Document;
                            HtmlElement sfClientDataDiv = null;
                            HtmlElementCollection sfAllDivs = document.GetElementsByTagName("div");

                            int[] countsColumns = new int[] { 0, 0, 0 };
                            int indexCurrentColumn = -1;
                            string innerHtml = null;

                            HtmlElement table = null;
                            HtmlElement thead = null;
                            HtmlElement row = null;
                            HtmlElement th = null;
                            HtmlElement td = null;

                            table = document.CreateElement("TABLE");
                            thead = document.CreateElement("THEAD");
                            table.SetAttribute("style", " width: 800px; ");
                            document.Body.AppendChild(table);
                            table.AppendChild(thead);
                            List<HtmlElement> rows = new List<HtmlElement>();
                            int indexCurrentRow = 0;
                            foreach (HtmlElement div in sfAllDivs)
                            {
                                switch (div.GetAttribute("className").ToString())
                                {
                                    case "slds-theme--default": //timeline
                                        div.FirstChild.OuterHtml = "";
                                        break;
                                    case "slds-grid slds-m-top--x-small": //cient info
                                        sfClientDataDiv = div;
                                        break;
                                    case "slds-col slds-text-heading--label": //section heading
                                        indexCurrentColumn++;
                                        indexCurrentRow = 0;

                                        row = rows.ElementAtOrDefault(indexCurrentRow);
                                        if (row == null)
                                        {
                                            row = document.CreateElement("TR");
                                            rows.Insert(indexCurrentRow, row);
                                            thead.AppendChild(row);
                                        }

                                        indexCurrentRow++;
                                        th = document.CreateElement("TH");
                                        th.InnerHtml = div.InnerHtml;
                                        th.SetAttribute("colspan", "2");
                                        row.AppendChild(th);
                                        break;
                                    case "slds-col": //name/value pairs
                                        countsColumns[indexCurrentColumn]++;
                                        row = rows.ElementAtOrDefault(indexCurrentRow);

                                        if (row == null)
                                        {
                                            row = document.CreateElement("TR");
                                            rows.Insert(indexCurrentRow, row);
                                            thead.AppendChild(row);
                                        }

                                        indexCurrentRow++;
                                        //ridiculously overcomplicated work around to different column counts needing blank cells inserted, refactor
                                        for (int i = 0; i < indexCurrentColumn; i++)
                                        {
                                            if (countsColumns[i] < countsColumns[indexCurrentColumn])
                                            {
                                                td = document.CreateElement("TD");
                                                td.InnerHtml = "&nbsp;";
                                                row.AppendChild(td);
                                                td = document.CreateElement("TD");
                                                td.InnerHtml = "&nbsp;";
                                                row.AppendChild(td);
                                            }
                                        }

                                        innerHtml = div.Children[0].InnerHtml;
                                        td = document.CreateElement("TD");
                                        td.InnerHtml = (!string.IsNullOrEmpty(innerHtml)) ? innerHtml : "&nbsp;";
                                        row.AppendChild(td);
                                        innerHtml = div.Children[1].InnerHtml;
                                        if (string.IsNullOrEmpty(innerHtml))
                                        {
                                            innerHtml = "&nbsp;";
                                        }
                                        else if (innerHtml.Contains("Not Checked"))
                                        {
                                            innerHtml = "<span style='color:red; '>X</span>";
                                        }
                                        else if (innerHtml.Contains("Checked"))
                                        {
                                            innerHtml = "<span style='color:green; '>&#10004;</span>";
                                        }

                                        td = document.CreateElement("TD");
                                        td.InnerHtml = innerHtml;
                                        row.AppendChild(td);
                                        break;
                                }
                            }

                            sfClientDataDiv.OuterHtml = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardVerificationSecurityQuestion.webBrowser_CheckForSfLogin\nException Occurred in Method webBrowser_CheckForSfLogin:\nVerified Security Questions billboard error trying to log into Salesforce:]\n\tEx: {}", ex);
                    Topic.Exception(newEx);
                    this.TopMost = false;
                }
            }
        }

        private void WebBrowser_HijackLinkClicks(object sender, WebBrowserNavigatingEventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.webBrowser_HijackLinkClicks(..)"))

            {
                /*
                                string url = e.Url.ToString();
                                string urlHost = e.Url.GetComponents(UriComponents.Host, UriFormat.Unescaped);
                
                                if (url != "about:blank" && !urlHost.EndsWith("salesforce.com"))
                                {
                                    e.Cancel = true;
                                    Process.Start(e.Url.ToString());
                                }*/

              
            }
        }

        private void VerificationSecurityQuestion_Load(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load(..)"))
            {
                Topic.Note("Entering and exiting method BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load:\nParameters\n\tsender: {}\n\te: " + e.ToString(), sender);
            }

        }

        private void BillboardDismissButton_Click(object sender, EventArgs e)
        {
            using (Topic.Scope("BillboardVerificationSecurityQuestion.VerificationSecurityQuestion_Load(..)"))
            {
                this.billboardDismissButton.Enabled = false;
                this.Close();
            }
        }

        private void Billboard_Exit(object sender, System.ComponentModel.CancelEventArgs e)
        {

            using (Topic.Scope("BillboardVerificationSecurityQuestion.billboard_Exit(..)"))
            {
                try
                {
                    // Commenting out for now.  This method was set as the FormClosingEventHandler.  The this.close() seems to do nothing more than call this event.  
                    //this.Close();
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("In BillboardVerificationSecurityQuestion.billboard_Exit\nException occurred in Billboard_exit\nException: " + ex.Message);
                    Topic.Exception(newEx);
                }
            }
        }

        private void BillboardWebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}