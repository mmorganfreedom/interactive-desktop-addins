﻿using System;
using System.Windows.Forms;
using FFN.TransferToPartnerFp.AccountIdEntryForm;

namespace FFN.TransferToPartnerFdr.AccountIdEntryForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ApplicationIdEntryForm(AccountIdEntryFormType.FreedomPlus));
        }
    }
}
