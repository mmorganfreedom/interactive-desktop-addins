﻿namespace FFN.TransferToPartnerFdr.AccountIdEntryForm
{
    partial class ApplicationIdEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.aniLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dnisLabel = new System.Windows.Forms.Label();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.applicationIdTextbox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(12, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(214, 13);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "No FDR Lead ID # associated with this call.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Outside Phone #:";
            // 
            // aniLabel
            // 
            this.aniLabel.AutoSize = true;
            this.aniLabel.Location = new System.Drawing.Point(108, 43);
            this.aniLabel.Name = "aniLabel";
            this.aniLabel.Size = new System.Drawing.Size(72, 13);
            this.aniLabel.TabIndex = 2;
            this.aniLabel.Text = "No ANI found";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Inbound Line:";
            // 
            // dnisLabel
            // 
            this.dnisLabel.AutoSize = true;
            this.dnisLabel.Location = new System.Drawing.Point(108, 65);
            this.dnisLabel.Name = "dnisLabel";
            this.dnisLabel.Size = new System.Drawing.Size(111, 13);
            this.dnisLabel.TabIndex = 4;
            this.dnisLabel.Text = "No inbound line found";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(12, 99);
            this.descriptionLabel.MaximumSize = new System.Drawing.Size(270, 100);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(252, 26);
            this.descriptionLabel.TabIndex = 5;
            this.descriptionLabel.Text = "If this call was to/from a lead, please enter the FDR Lead ID # then click OK.";
            // 
            // applicationIdTextbox
            // 
            this.applicationIdTextbox.Location = new System.Drawing.Point(13, 131);
            this.applicationIdTextbox.Name = "applicationIdTextbox";
            this.applicationIdTextbox.Size = new System.Drawing.Size(256, 20);
            this.applicationIdTextbox.TabIndex = 1;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(194, 157);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 2;
            this.submitButton.Text = "OK";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(113, 157);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ApplicationIdEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 189);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.applicationIdTextbox);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.dnisLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.aniLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.titleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ApplicationIdEntryForm";
            this.Text = "Input FDR Lead ID #";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label aniLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label dnisLabel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox applicationIdTextbox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
    }
}

