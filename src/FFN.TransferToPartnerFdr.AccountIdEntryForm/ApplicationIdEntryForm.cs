﻿using System;
using System.Windows.Forms;
using ININ.Diagnostics;
using ININ.IceLib.Interactions;

namespace FFN.TransferToPartnerFdr.AccountIdEntryForm
{
    public partial class ApplicationIdEntryForm : Form
    {
        public Interaction Interaction { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }

        public readonly static ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer("PSO.TransferToPartnerFdr.AccountIdEntryForm");

        public ApplicationIdEntryForm(AccountIdEntryFormType type)
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        public void LoadFreedomPlusForm()
        {
            try
            {
                Text = "Input FreedomPlus Application ID";

                aniLabel.Text = !string.IsNullOrEmpty(Interaction.GetStringAttribute("Eic_RemoteTn"))
                    ? Interaction.GetStringAttribute("Eic_RemoteTn")
                    : "No ANI found!";

                dnisLabel.Text = !string.IsNullOrEmpty(Interaction.GetStringAttribute("Eic_LocalTn"))
                    ? Interaction.GetStringAttribute("Eic_RemoteTn")
                    : "No DNIS found!";

                descriptionLabel.Text = "PLease enter the Application Name (digits only) or Salesforce Record Id then click OK.";
                titleLabel.Text = "No FreedomPlus Application associated with this call.";
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }

        public void LoadFdrForm()
        {
            Text = "Input FDR Lead ID #";

            aniLabel.Text = !string.IsNullOrEmpty(Interaction.GetStringAttribute("Eic_RemoteTn"))
                ? Interaction.GetStringAttribute("Eic_RemoteTn")
                : "No ANI found!";

            dnisLabel.Text = !string.IsNullOrEmpty(Interaction.GetStringAttribute("Eic_LocalTn"))
                ? Interaction.GetStringAttribute("Eic_RemoteTn")
                : "No DNIS found!";

            descriptionLabel.Text = "If this call was to/from a lead, please enter the FDR Lead ID # then click OK.";
            titleLabel.Text = "No FDR Lead ID # associated with this call";
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            AttributeValue = applicationIdTextbox.Text;
            Interaction.SetStringAttribute(AttributeName, applicationIdTextbox.Text);
            Dispose();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Interaction.SetStringAttribute(AttributeName, "Cancel");
            Dispose();
        }
    }
}
