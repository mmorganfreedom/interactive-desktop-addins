﻿namespace FFN.TransferToPartnerFp.AccountIdEntryForm
{
    partial class HoursOfOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HoursOfOperationForm));
            this.hoursOfOperationDataGridView = new System.Windows.Forms.DataGridView();
            this.dayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.closeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.hoursOfOperationDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // hoursOfOperationDataGridView
            // 
            this.hoursOfOperationDataGridView.AllowUserToAddRows = false;
            this.hoursOfOperationDataGridView.AllowUserToDeleteRows = false;
            this.hoursOfOperationDataGridView.AllowUserToResizeColumns = false;
            this.hoursOfOperationDataGridView.AllowUserToResizeRows = false;
            this.hoursOfOperationDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.hoursOfOperationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hoursOfOperationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dayColumn,
            this.openColumn,
            this.closeColumn});
            this.hoursOfOperationDataGridView.Location = new System.Drawing.Point(12, 25);
            this.hoursOfOperationDataGridView.Name = "hoursOfOperationDataGridView";
            this.hoursOfOperationDataGridView.ReadOnly = true;
            this.hoursOfOperationDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.hoursOfOperationDataGridView.Size = new System.Drawing.Size(354, 234);
            this.hoursOfOperationDataGridView.TabIndex = 0;
            // 
            // dayColumn
            // 
            this.dayColumn.HeaderText = "Day";
            this.dayColumn.Name = "dayColumn";
            this.dayColumn.ReadOnly = true;
            // 
            // openColumn
            // 
            this.openColumn.HeaderText = "Open";
            this.openColumn.Name = "openColumn";
            this.openColumn.ReadOnly = true;
            // 
            // closeColumn
            // 
            this.closeColumn.HeaderText = "Close";
            this.closeColumn.Name = "closeColumn";
            this.closeColumn.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hours of Operation";
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(291, 265);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 2;
            this.submitButton.Text = "OK";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // HoursOfOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 300);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hoursOfOperationDataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HoursOfOperationForm";
            this.Text = "Hours of Operation";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.hoursOfOperationDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView hoursOfOperationDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dayColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn openColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn closeColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button submitButton;
    }
}