﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FFN.TransferToPartnerFp.AccountIdEntryForm
{
    public partial class HoursOfOperationForm : Form
    {
        public HoursOfOperationForm(Dictionary<string, string[]> hoursOfOperationDictionary)
        {
            InitializeComponent();
            PopulateDataGridView(hoursOfOperationDictionary);
        }

        private void PopulateDataGridView(Dictionary<string, string[]> hoursOfOperationDictionary)
        {
            foreach (var day in hoursOfOperationDictionary)
            {
                var row = new DataGridViewRow();
                row.Cells["Day"].Value = day.Key;
                row.Cells["Open"].Value = day.Value[0];
                row.Cells["Close"].Value = day.Value[1];
                hoursOfOperationDataGridView.Rows.Add(row);
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
